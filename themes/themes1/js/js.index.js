
$(document).ready(function() {
    $(".view_sektoral").on("click", function(){
        var id=$(this).attr("id");
// console.log("<?php echo $this->request->baseUrl;?>");
        $.ajax({
            url: "http://localhost/basic/web/index.php?r=site/viewmeeting&id="+id,
            type: "GET",
            contentType: false,
            cache: false,
            processData:false,
            beforeSend : function(event){
                // $("#arcupload").css("opacity",".5");
                // $("#loading").css("display", "block");
                $("#s_title").html('');
                $("#s_body").html('');
            },
            success: function(res) {
                // $("#loading").css("display", "none");
                if(res.status=='1'){
                    var ketua="";
                    if( !!res.data.ketua1 && !!res.data.ketua2 ){
                        ketua="Ketua Rapat : "+res.data.ketua1+" dan "+res.data.ketua2;
                    } else if(!!res.data.ketua1){
                        ketua="Ketua Rapat : "+res.data.ketua1;
                    } else {
                        ketua="Ketua Rapat : "+res.data.ketua2;
                    }

                    var lokasi=res.data.lokasi+', '+res.data.tanggal+'<br>'+ketua;
                    var tl="-";
                    if(!!res.data.tindaklanjut){
                        tl=res.data.tindaklanjut;
                    }
                    $("#s_title").html(res.data.judul);
                    $("#s_body").html(lokasi+"<br>Tindak Lanjut : <br>"+tl);
                    if(res.data.foto!=NULL){
                        $("#s_img").html('<img src="'+res.data.foto+'" class="imgberita img-responsive">');
                    }
                } else {
                    alert(res.message);
                }
            }
        });
        $($(this).data("target")).modal("show");
    });
});