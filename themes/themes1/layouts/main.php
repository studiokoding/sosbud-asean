<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
$homeUrl=Url::home(true).'..';
// AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo $this->theme->baseUrl; ?>/images/favicon.ico" type="image/vnd.microsoft.icon" />
    <!-- Core css -->
    <link href="<?php echo $this->theme->baseUrl; ?>/css/app.min.css" rel="stylesheet">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        .bodygray{
            background-image: url('<?php echo $this->theme->baseUrl; ?>/images/bg-body.png');
        }

        .navbar-light .navbar-nav .nav-link {
            color:white;
        }
    </style>
</head>
<body class="bodygray">
<?php $this->beginBody() ?>
<div class="app">
    
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <img src="<?php echo $this->theme->baseUrl; ?>/images/header.png" width="100%" class="img-fluid" alt="Kemenko-PMK" />
            </div>
            
                <div class="col-lg-12">
                    <?php $modelKl = \app\models\Kl::find()->all() ?>
                    <div class="row">  
                        <?php foreach($modelKl as $m): ?>
                            <div style="padding:20px">
                                <img src="<?= Url::to('@web/logo/'.$m->logo, true); ?>" style="width:40px"/>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            

            <div class="col-lg-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-dark justify-content-between">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo Yii::$app->homeUrl;?>">Beranda</a>
                        </li>
                        
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="<?php echo Url::to(['site/about']);?>"></a>
                        </li> -->
                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Sosbud ASEAN</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="<?php echo Url::to(['site/latarbelakang']);?>"> Latar Belakang </a></li>
                                <li><a class="dropdown-item" href="<?php echo Url::to(['site/badansektoral']);?>"> Badan Sektoral </a></li>
                                <li><a class="dropdown-item" href="<?php echo Url::to(['site/entitas']);?>"> Entitas Regional </a>
                            </ul>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::home() ?>./../../backendpmk">Login</a>
                        </li>
                    </ul>
                </div>
                
                <!-- <form class="form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Masukkan kata kunci" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
                </form> -->
            </nav>
            </div>
        </div>
        
        <div class="main-content">
            <?= $content ?>
        </div>
    </div>
</div>


<footer class="footer">
    <div class="footer-content">
        <img src="<?php echo $this->theme->baseUrl; ?>/images/logo/logo-kecil.png" height="40px" class="rounded float-left" alt="Logo-KemenkoPMK-kecil" />
        <span>Kementerian Koordinator Bidang Pembangunan Manusia dan Kebudayaan. </span>        
    </div>
</footer>
<script src="<?php echo $homeUrl;?>/assets/4d90268e/jquery.js"></script>
<script src="<?php echo $this->theme->baseUrl; ?>/js/vendors.min.js"></script>

<?php
    $this->endBody() 
?>

</body>
</html>
<?php $this->endPage() ?>