<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\MeetingAll;
use app\models\Berita;
use app\models\ContactForm;
use app\models\Config;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','viewmeeting','viewberita'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    // 'viewmeeting' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $querydata = Yii::$app->db->createCommand('SELECT * FROM berita ORDER BY berita_id DESC LIMIT 5');
		$databerita = $querydata->queryAll();
        
        $querysektoral = Yii::$app->db->createCommand('SELECT * FROM meeting_all_name ORDER BY all_id DESC LIMIT 5');
		$datasektoral = $querysektoral->queryAll();
		
		$querylinkk = Yii::$app->db->createCommand('SELECT * FROM link where link_show=1 ORDER BY link_nama ASC');
        $datalinkk = $querylinkk->queryAll();

        return $this->render('index',[
            'databerita' => $databerita,
            'datasektoral' => $datasektoral,
            'datalinks' => $datalinkk,
        ]);
    }

    public function actionLatarbelakang(){
        $sql="SELECT * FROM config WHERE alias='about'";
        $data=Yii::$app->db->createCommand($sql)->queryOne();

        return $this->render('latarbelakang',[
            'data'=>$data
        ]);
    }

    public function actionBadansektoral(){
        $sql="SELECT k.kl_id,k.kl_nama,s.sektoral_id,s.sektoral_nama,s.sektoral_singkatan,ss.sektoralsub_id,ss.sektoralsub_nama ,ss.sektoralsub_singkatan
        FROM sektoralsub ss
        LEFT JOIN sektoral s ON s.sektoral_id=ss.sektoralsub_sektoral_id
        LEFT JOIN kl k ON k.kl_id=s.sektoral_kl_id
        ORDER BY k.kl_id,s.sektoral_id,ss.sektoralsub_id";
        $data=Yii::$app->db->createCommand($sql)->queryAll();

        return $this->render('badansektoral',[
            'data'=>$data
        ]);
    }

    public function actionEntitas(){
        $data="entitas";
        return $this->render('entitas',[
            'data'=>$data
        ]);
    }

    public function actionViewmeeting($id){
        $return=array(
            'status'=>0,
            'message'=>'data tidak ditemukan',
            'data'=>''
        );
        $model = MeetingAll::find(1)
                ->where('all_id=:id', [':id' => $id])
                ->one();
        if($model){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $return=array(
                'status'=>1,
                'message'=>'data tidak ditemukan',
                'data'=>array(
                    'id'=>$model->all_id,
                    'foto'=>$model->foto,
                    'judul'=>$model->title,
                    'lokasi'=>$model->lokasi,
                    'tanggal'=>$model->tanggal,
                    'ketua1'=>$model->ketua1,
                    'ketua2'=>$model->ketua2,
                    'tindaklanjut'=>$model->tindaklanjut,
                )
            );
        }
        
        return $return;
    }

    public function actionViewberita($id){
        $homeUrl=Url::home(true).'..';
        $return=array(
            'status'=>0,
            'message'=>'data tidak ditemukan',
            'data'=>''
        );
        $model = Berita::find(1)
                ->where('berita_id=:id', [':id' => $id])
                ->one();
        if($model){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $return=array(
                'status'=>1,
                'message'=>'data ditemukan',
                'data'=>array(
                    'id'=>$model->berita_id,
                    'foto'=>$homeUrl.'/assets/berita/'.$model->berita_foto,
                    'judul'=>$model->berita_judul,
                    'berita'=>$model->berita_overview,
                    'tanggal'=>Yii::$app->myHelper->indonesian_date($model->berita_adddate),
                )
            );
        }
        
        return $return;
    }

    public function actionResultsearch($keyword){
        $cleankeyword=HtmlPurifier::process($keyword);
        echo $cleankeyword;
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $a=Config::find(1)
        ->where('alias=:alias', [':alias' =>'about'])
        ->one();
        return $this->render('about',[
            'about'=>$a
        ]);
    }
}
