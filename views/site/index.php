<?php

use yii\helpers\Url;
use yii\web\View;
/* @var $this yii\web\View */
$homeUrl=Url::home(true).'..';
    function readmore($teks, $jumlahkata){
        $string=explode(" ", $teks);
        $hasil="";
        for($i=0;$i<$jumlahkata;$i++){ 
            $hasil .= $string[$i]." "; 
        }
        return $hasil;
    }
$this->title = 'Forkom ASEAN';
    $kementrian="";
    $lembaga="";
    foreach($datalinks as $links){
	    if($links['link_opt']==1){
            $kementrian.='<li class="list-group-item"><a target="_blank" href="'.$links['link_href'].'" class="nav-link">'.$links['link_nama'].'</a></li>';
		} else {
            $lembaga.='<li class="list-group-item"><a target="_blank" href="'.$links['link_href'].'" class="nav-link">'.$links['link_nama'].'</a></li>';
        }
    }

    $listsektoral="";
    foreach($datasektoral as $sektoral){
        $ketua="Ketua Rapat : ";
        if(!empty($sektoral['ketua1']) && !empty($sektoral['ketua2'])) $ketua.=$sektoral['ketua1']." dan ".$sektoral['ketua2'];
		if(empty($sektoral['ketua1'])) $ketua.=$sektoral['ketua2'];
        if(empty($sektoral['ketua2'])) $ketua.=$sektoral['ketua1'];
        
        $listsektoral.='<li data-target=".bd-example-modal-xl" id="'.$sektoral['all_id'].'" class="list-group-item cursor-pointer view_sektoral">
            <a class="sektoral nav-link"><strong>'.$sektoral['title'].'</strong><br>
            '.$sektoral['lokasi'].', '.Yii::$app->myHelper->indonesian_date($sektoral['tanggal']).'
            </a>
        </li>';
    }
?>
<style>
.list-group-item{
    padding:0px 0px 0px 0px;
}

.cursor-pointer{
  cursor: pointer;
}
</style>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-body">
                        <?php
                            $i=0;
                            $li=array();
                            $items=array();
                            $linkJudul=array();
                            $active="";
                            foreach($databerita as $berita){
                                array_push($linkJudul,'<li class="list-group-item"><img src="'.$berita['berita_foto'].'" style="width:150px" ><a target="_blank" href="#" class="nav-link">'.$berita['berita_judul'].'</a></li>');
                                $image=$this->theme->baseUrl."/images/slideshow/img-2.jpg";
                                if($i==0){
                                   $active='active'; 
                                } else {
                                    $active="";
                                }

                                if($berita['berita_foto']!=''){
                                    $image=$homeUrl.'/assets/berita/'.$berita['berita_foto'];
                                }
                                $item='<div class="carousel-item '.$active.'">
                                    <img src="'.$image.'" alt="berita_'.$i.'" class="d-block img-fluid">
                                    <div class="carousel-caption d-none d-md-block">
                                    <h5>'.$berita['berita_judul'].'</h5>
                                    
                                    </div>
                                </div>';
                                array_push($items,$item);

                                $i++;        
                            }
                        ?>

                        <div id="carouselExampleCaption" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <?php echo implode(" ",$items);?>
                            </div>
                            
                            <a class="carousel-control-prev" href="#carouselExampleCaption" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            
                            <a class="carousel-control-next" href="#carouselExampleCaption" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 "><a href="http://asean.org/"><img src="<?php echo $this->theme->baseUrl; ?>/images/logo/asean.png" class="img-responsive rounded mx-auto d-block"></a></div>
                    <div class="col-md-4"><a href="http://www.aseanstats.org/"><img src="<?php echo $this->theme->baseUrl; ?>/images/logo/acss.png" class="img-responsive rounded mx-auto d-block"></a></div>
                    <div class="col-md-4"><a href="http://www.kemenkopmk.go.id/"><img src="<?php echo $this->theme->baseUrl; ?>/images/logo/pmk.png" class="img-responsive rounded mx-auto d-block"></a></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">Berita</div>
                            </div>
                            <div class="card-body">
                                <?php foreach($databerita as $berita): ?>
                                    <?php $image=$homeUrl.'/assets/berita/'.$berita['berita_foto']; ?>
                                    <div class="row mb-3">
                                        <div class="col-md-3">
                                            <img src="<?= $image ?>" width="100%" />
                                        </div>
                                        <div class="col-md-9">
                                            <strong><?= $berita['berita_judul'] ?></strong><br>
                                            <small><?= Yii::$app->myHelper->indonesian_date($berita['berita_adddate'])?> </small>
                                            <small><?php
                                            $b=strip_tags($berita['berita_overview']);
                                            $output="";
                                            if(str_word_count($b) > 200){
                                               $output= readmore($b,200);
                                               echo '<p>'.$output.' ... <a  data-target=".bd-example-modal-xl" id="brt_'.$berita['berita_id'].'" class="view_berita" href="#">Selengkapnya</a></p>';
                                            } else {
                                               echo $berita['berita_overview'];
                                            }
                                            ?></small>
                                        </div>
                                    </div>
                                    <hr>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Kementrian Terkait</h4>
                    </div>
                    <div>
                        <ul class="list-group">
                            <?php echo $kementrian;?>
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Lembaga Terkait</h4>
                    </div>
                    <div>
                        <ul class="list-group">
                            <?php echo $lembaga;?>
                        </ul>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Rapat</h4>
                    </div>
                    <div>
                        <ul class="list-group">
                            <?php  echo $listsektoral; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade bd-example-modal-xl">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="s_title" class="modal-title h4">...</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="anticon anticon-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center" id="s_img"></div>
                <div id="s_body"></div>
            </div>
        </div>
    </div>
</div>


<?php

$url=Url::to(['site/viewmeeting']);
$script ='
    $(".view_sektoral").on("click", function(){
        var id=$(this).attr("id");
        var url="'.Url::to(['site/viewmeeting']).'";
        $.ajax({
            url: url,
            type: "GET",
            data:{id:id},
            beforeSend : function(event){
                // $("#arcupload").css("opacity",".5");
                // $("#loading").css("display", "block");
                $("#s_title").html("");
                $("#s_body").html("");
            },
            success: function(res) {
                // $("#loading").css("display", "none");
                if(res.status=="1"){
                    var ketua="";
                    if( !!res.data.ketua1 && !!res.data.ketua2 ){
                        ketua="Ketua Rapat : "+res.data.ketua1+" dan "+res.data.ketua2;
                    } else if(!!res.data.ketua1){
                        ketua="Ketua Rapat : "+res.data.ketua1;
                    } else {
                        ketua="Ketua Rapat : "+res.data.ketua2;
                    }

                    var lokasi=res.data.lokasi+", "+res.data.tanggal+"<br>"+ketua;
                    var tl="-";
                    if(!!res.data.tindaklanjut){
                        tl=res.data.tindaklanjut;
                    }
                    
                    $("#s_title").html(res.data.judul);
                    $("#s_body").html(lokasi+"<br>Tindak Lanjut : <br>"+unescape(tl));
                    if(!!res.data.foto){
                        $("#s_img").html("<img src=\'+res.data.foto+\' class=\'imgberita img-responsive\'>");
                    }
                } else {
                    alert(res.message);
                }
            }
        });
        $($(this).data("target")).modal("show");
    });

    $(".view_berita").on("click", function(){
        var id=$(this).attr("id");
        var tmp=id.split("_");
        
        var url="'.Url::to(['site/viewberita']).'";
        $.ajax({
            url: url,
            type: "GET",
            data:{id:tmp[1]},
            beforeSend : function(event){
                $("#s_title").html("");
                $("#s_body").html("");
                $("#s_img").html("");
            },
            success: function(res) {
                if(res.status=="1"){
                    $("#s_title").html(res.data.judul);
                    $("#s_body").html(res.data.tanggal+"<br>"+res.data.berita);
                    if(!!res.data.foto){
                        $("#s_img").html("<img width=\'100%\' src=\'"+res.data.foto+"\' class=\'imgberita img-responsive\'>");
                    }
                } else {
                    alert(res.message);
                }
            }
        });
        $($(this).data("target")).modal("show");
    });

   
';
$this->registerJs($script,View::POS_READY,'popup-handler');
$this->registerCss("
a.nav-link:hover { 
    background-color: #007bff;
    color: white;
}
.sektoral {
    color : #007bff !important;
}
.sektoral:hover {
    background-color: #007bff !important;
    color: white !important;
}
");
?>