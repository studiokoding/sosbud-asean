<?php
use yii\helpers\Url;
use yii\web\View;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
    
?>
<style>
.list-group-item{
    padding:0px 0px 0px 0px;
}

.cursor-pointer{
  cursor: pointer;
}
</style>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-header">Daftar Badan Sektoral</div>
                    <div class="card-body">
                        <div style="width:100%;overflow-x:scroll;">
                        <table 
                            id="listsektoral"
                            class="table"
                            style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 10%;">#</th>
                                    <th scope="col" style="width: 30%;">Kementrian / Lembaga</th>
                                    <th scope="col" style="width: 30%;">Sektoral</th>
                                    <th scope="col" style="width: 30%;">Sektoral Sub</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $tr="";
                                    $i=1;
                                    foreach($data as $r){
                                        $tr.="<tr>
                                        <td>".$i."</td>
                                        <td>".$r['kl_nama']."</td>
                                        <td>".($r['sektoral_singkatan'] ? "[".$r['sektoral_singkatan']."] ".$r['sektoral_nama'] : "")."</td>
                                        <td>".($r['sektoralsub_singkatan'] ? "[".$r['sektoralsub_singkatan']."] ".$r['sektoralsub_nama'] : "")."</td>
                                        </tr>";

                                        $i++;
                                    }
                                    echo $tr;
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>