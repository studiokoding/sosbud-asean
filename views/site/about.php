<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-9">
                <div class="card m-t-10">
                    <div class="card-header"><h1><?php echo $about->judul;?></h1></div>
                    <div class="card-body">
                        <?php echo $about->isi; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                
            </div>
        </div>

    </div>
</div>