<?php
use yii\helpers\Url;
use yii\web\View;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
    
?>
<style>
.list-group-item{
    padding:0px 0px 0px 0px;
}

.cursor-pointer{
  cursor: pointer;
}
</style>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-header"><?php echo $data;?></div>
                    <div class="card-body">
                    <?php echo $data;?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>