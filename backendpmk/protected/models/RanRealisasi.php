<?php

/**
 * This is the model class for table "ran_realisasi".
 *
 * The followings are the available columns in table 'ran_realisasi':
 * @property integer $id
 * @property integer $tahun
 * @property integer $triwulan
 * @property integer $kegiatan
 * @property integer $target
 * @property integer $realisasi
 *
 * The followings are the available model relations:
 * @property RanDatadukung[] $ranDatadukungs
 * @property MKegiatan $kegiatan0
 * @property RanTarget $target0
 */
class RanRealisasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ran_realisasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tahun, triwulan, kegiatan, target, realisasi', 'required'),
			array('tahun, triwulan, kegiatan, target, realisasi', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tahun, triwulan, kegiatan, target, realisasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ranDatadukungs' => array(self::HAS_MANY, 'RanDatadukung', 'id_realisasi'),
			'kegiatan0' => array(self::BELONGS_TO, 'MKegiatan', 'kegiatan'),
			'target0' => array(self::BELONGS_TO, 'RanTarget', 'target'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tahun' => 'Tahun',
			'triwulan' => 'Triwulan',
			'kegiatan' => 'Kegiatan',
			'target' => 'Target',
			'realisasi' => 'Realisasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('triwulan',$this->triwulan);
		$criteria->compare('kegiatan',$this->kegiatan);
		$criteria->compare('target',$this->target);
		$criteria->compare('realisasi',$this->realisasi);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RanRealisasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
