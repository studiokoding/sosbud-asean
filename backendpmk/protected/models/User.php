<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_sektoralsub_id
 * @property integer $level
 *
 * The followings are the available model relations:
 * @property Berita[] $beritas
 * @property Brafaks[] $brafaks
 * @property Deklarasi[] $deklarasis
 * @property Implementasi[] $implementasis
 * @property Kegiatan[] $kegiatans
 * @property Kongres[] $kongres
 * @property Meeting[] $meetings
 * @property Sektoral[] $sektorals
 * @property Sektoralsub[] $sektoralsubs
 * @property Summit[] $summits
 * @property Tindak[] $tindaks
 * @property Sektoralsub $userSektoralsub
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, auth_key, password_hash, email, created_at, updated_at', 'required'),
			array('status, created_at, updated_at, user_sektoralsub_id, level', 'numerical', 'integerOnly'=>true),
			array('username, password_hash, password_reset_token, email', 'length', 'max'=>255),
			array('auth_key', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, auth_key, password_hash, password_reset_token, email, status, created_at, updated_at, user_sektoralsub_id, level', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'beritas' => array(self::HAS_MANY, 'Berita', 'berita_user_id'),
			'brafaks' => array(self::HAS_MANY, 'Brafaks', 'brafaks_user_id'),
			'deklarasis' => array(self::HAS_MANY, 'Deklarasi', 'deklarasi_user_id'),
			'implementasis' => array(self::HAS_MANY, 'Implementasi', 'implementasi_user_id'),
			'kegiatans' => array(self::HAS_MANY, 'Kegiatan', 'kegiatan_user_id'),
			'kongres' => array(self::HAS_MANY, 'Kongres', 'kongres_user_id'),
			'meetings' => array(self::HAS_MANY, 'Meeting', 'meeting_user_id'),
			'sektorals' => array(self::HAS_MANY, 'Sektoral', 'sektoral_user_id'),
			'sektoralsubs' => array(self::HAS_MANY, 'Sektoralsub', 'sektoralsub_user_id'),
			'summits' => array(self::HAS_MANY, 'Summit', 'summit_user_id'),
			'tindaks' => array(self::HAS_MANY, 'Tindak', 'tindak_user_id'),
			'userSektoralsub' => array(self::BELONGS_TO, 'Sektoralsub', 'user_sektoralsub_id'),
			'level0' => array(self::BELONGS_TO, 'Userlevel', 'level'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'auth_key' => 'Auth Key',
			'password_hash' => 'Password Hash',
			'password_reset_token' => 'Password Reset Token',
			'email' => 'Email',
			'status' => 'Status',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'user_sektoralsub_id' => 'User Sektoralsub',
			'level' => 'Level',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('auth_key',$this->auth_key,true);
		$criteria->compare('password_hash',$this->password_hash,true);
		$criteria->compare('password_reset_token',$this->password_reset_token,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);
		$criteria->compare('user_sektoralsub_id',$this->user_sektoralsub_id);
		$criteria->compare('level',$this->level);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function validatePassword($pass){
		if( $this->auth_key === $this->hashPassword($pass,$this->password_hash)){
			return TRUE;
		}
		return FALSE;
	}

	public function hashPassword($password,$salt){
		return md5("pmk2020-".$password.$salt);
	}

	public function generateHash(){
		return uniqid("pmk2020-",TRUE);
	}
}
