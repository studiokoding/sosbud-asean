<?php

/**
 * This is the model class for table "berita".
 *
 * The followings are the available columns in table 'berita':
 * @property integer $berita_id
 * @property string $berita_judul
 * @property string $berita_foto
 * @property string $berita_overview
 * @property string $berita_isi
 * @property integer $berita_user_id
 * @property string $berita_adddate
 *
 * The followings are the available model relations:
 * @property User $beritaUser
 */
class Berita extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'berita';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('berita_user_id', 'numerical', 'integerOnly'=>true),
			array('berita_foto', 'length', 'max'=>255),
			array('berita_judul, berita_overview, berita_isi, berita_adddate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('berita_id, berita_judul, berita_foto, berita_overview, berita_isi, berita_user_id, berita_adddate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'beritaUser' => array(self::BELONGS_TO, 'User', 'berita_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'berita_id' => 'Berita',
			'berita_judul' => 'Berita Judul',
			'berita_foto' => 'Berita Foto',
			'berita_overview' => 'Berita Overview',
			'berita_isi' => 'Berita Isi',
			'berita_user_id' => 'Berita User',
			'berita_adddate' => 'Berita Adddate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('berita_id',$this->berita_id);
		$criteria->compare('berita_judul',$this->berita_judul,true);
		$criteria->compare('berita_foto',$this->berita_foto,true);
		$criteria->compare('berita_overview',$this->berita_overview,true);
		$criteria->compare('berita_isi',$this->berita_isi,true);
		$criteria->compare('berita_user_id',$this->berita_user_id);
		$criteria->compare('berita_adddate',$this->berita_adddate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Berita the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
