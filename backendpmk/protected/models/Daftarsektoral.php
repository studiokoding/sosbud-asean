<?php

/**
 * This is the model class for table "daftarsektoral".
 *
 * The followings are the available columns in table 'daftarsektoral':
 * @property integer $sektoral_id
 * @property string $sektoral_nama
 * @property string $sektoral_singkatan
 * @property integer $sektoral_kl_id
 * @property string $sektoral_overview
 * @property integer $sektoral_user_id
 * @property string $sektoral_adddate
 * @property integer $sektoralsub_id
 * @property string $sektoralsub_nama
 * @property string $sektoralsub_singkatan
 * @property integer $sektoralsub_user_id
 * @property string $sektoralsub_adddate
 */
class Daftarsektoral extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'daftarsektoral';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sektoral_id, sektoral_kl_id, sektoral_user_id, sektoralsub_id, sektoralsub_user_id', 'numerical', 'integerOnly'=>true),
			array('sektoral_nama, sektoral_singkatan, sektoralsub_nama, sektoralsub_singkatan', 'length', 'max'=>255),
			array('sektoral_overview, sektoral_adddate, sektoralsub_adddate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sektoral_id, sektoral_nama, sektoral_singkatan, sektoral_kl_id, sektoral_overview, sektoral_user_id, sektoral_adddate, sektoralsub_id, sektoralsub_nama, sektoralsub_singkatan, sektoralsub_user_id, sektoralsub_adddate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sektoral_id' => 'Sektoral',
			'sektoral_nama' => 'Sektoral Nama',
			'sektoral_singkatan' => 'Sektoral Singkatan',
			'sektoral_kl_id' => 'Sektoral Kl',
			'sektoral_overview' => 'Sektoral Overview',
			'sektoral_user_id' => 'Sektoral User',
			'sektoral_adddate' => 'Sektoral Adddate',
			'sektoralsub_id' => 'Sektoralsub',
			'sektoralsub_nama' => 'Sektoralsub Nama',
			'sektoralsub_singkatan' => 'Sektoralsub Singkatan',
			'sektoralsub_user_id' => 'Sektoralsub User',
			'sektoralsub_adddate' => 'Sektoralsub Adddate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sektoral_id',$this->sektoral_id);
		$criteria->compare('sektoral_nama',$this->sektoral_nama,true);
		$criteria->compare('sektoral_singkatan',$this->sektoral_singkatan,true);
		$criteria->compare('sektoral_kl_id',$this->sektoral_kl_id);
		$criteria->compare('sektoral_overview',$this->sektoral_overview,true);
		$criteria->compare('sektoral_user_id',$this->sektoral_user_id);
		$criteria->compare('sektoral_adddate',$this->sektoral_adddate,true);
		$criteria->compare('sektoralsub_id',$this->sektoralsub_id);
		$criteria->compare('sektoralsub_nama',$this->sektoralsub_nama,true);
		$criteria->compare('sektoralsub_singkatan',$this->sektoralsub_singkatan,true);
		$criteria->compare('sektoralsub_user_id',$this->sektoralsub_user_id);
		$criteria->compare('sektoralsub_adddate',$this->sektoralsub_adddate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Daftarsektoral the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
