<?php

/**
 * This is the model class for table "kl".
 *
 * The followings are the available columns in table 'kl':
 * @property integer $kl_id
 * @property string $kl_nama
 * @property string $logo
 *
 * The followings are the available model relations:
 * @property MKegiatan[] $mKegiatans
 * @property Sektoral[] $sektorals
 */
class Kl extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kl';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kl_nama, logo', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kl_id, kl_nama, logo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mKegiatans' => array(self::HAS_MANY, 'MKegiatan', 'pj_kl'),
			'sektorals' => array(self::HAS_MANY, 'Sektoral', 'sektoral_kl_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kl_id' => 'Kl',
			'kl_nama' => 'Kl Nama',
			'logo' => 'Logo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kl_id',$this->kl_id);
		$criteria->compare('kl_nama',$this->kl_nama,true);
		$criteria->compare('logo',$this->logo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kl the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
