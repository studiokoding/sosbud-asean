<?php

/**
 * This is the model class for table "sektoralsub".
 *
 * The followings are the available columns in table 'sektoralsub':
 * @property integer $sektoralsub_id
 * @property string $sektoralsub_nama
 * @property string $sektoralsub_singkatan
 * @property integer $sektoralsub_sektoral_id
 * @property integer $sektoralsub_user_id
 * @property string $sektoralsub_adddate
 *
 * The followings are the available model relations:
 * @property Meeting[] $meetings
 * @property Sektoral $sektoralsubSektoral
 * @property User $sektoralsubUser
 * @property User[] $users
 */
class Sektoralsub extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sektoralsub';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sektoralsub_sektoral_id, sektoralsub_user_id', 'numerical', 'integerOnly'=>true),
			array('sektoralsub_nama, sektoralsub_singkatan', 'length', 'max'=>255),
			array('sektoralsub_adddate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('sektoralsub_id, sektoralsub_nama, sektoralsub_singkatan, sektoralsub_sektoral_id, sektoralsub_user_id, sektoralsub_adddate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'meetings' => array(self::HAS_MANY, 'Meeting', 'meeting_sektoralsub_id'),
			'sektoralsubSektoral' => array(self::BELONGS_TO, 'Sektoral', 'sektoralsub_sektoral_id'),
			'sektoralsubUser' => array(self::BELONGS_TO, 'User', 'sektoralsub_user_id'),
			'users' => array(self::HAS_MANY, 'User', 'user_sektoralsub_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'sektoralsub_id' => 'Sektoralsub',
			'sektoralsub_nama' => 'Sektoralsub Nama',
			'sektoralsub_singkatan' => 'Sektoralsub Singkatan',
			'sektoralsub_sektoral_id' => 'Sektoralsub Sektoral',
			'sektoralsub_user_id' => 'Sektoralsub User',
			'sektoralsub_adddate' => 'Sektoralsub Adddate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sektoralsub_id',$this->sektoralsub_id);
		$criteria->compare('sektoralsub_nama',$this->sektoralsub_nama,true);
		$criteria->compare('sektoralsub_singkatan',$this->sektoralsub_singkatan,true);
		$criteria->compare('sektoralsub_sektoral_id',$this->sektoralsub_sektoral_id);
		$criteria->compare('sektoralsub_user_id',$this->sektoralsub_user_id);
		$criteria->compare('sektoralsub_adddate',$this->sektoralsub_adddate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sektoralsub the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
