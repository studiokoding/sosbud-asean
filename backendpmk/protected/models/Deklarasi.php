<?php

/**
 * This is the model class for table "deklarasi".
 *
 * The followings are the available columns in table 'deklarasi':
 * @property integer $deklarasi_id
 * @property string $deklarasi_judul
 * @property string $deklarasi_focal
 * @property integer $deklarasi_user_id
 * @property string $deklarasi_adddate
 * @property integer $deklarasi_summit_id
 *
 * The followings are the available model relations:
 * @property User $deklarasiUser
 * @property Implementasi[] $implementasis
 * @property Tindak[] $tindaks
 */
class Deklarasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'deklarasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deklarasi_user_id, deklarasi_summit_id', 'numerical', 'integerOnly'=>true),
			array('deklarasi_judul', 'length', 'max'=>100),
			array('deklarasi_focal', 'length', 'max'=>255),
			array('deklarasi_adddate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('deklarasi_id, deklarasi_judul, deklarasi_focal, deklarasi_user_id, deklarasi_adddate, deklarasi_summit_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'deklarasiUser' => array(self::BELONGS_TO, 'User', 'deklarasi_user_id'),
			'implementasis' => array(self::HAS_MANY, 'Implementasi', 'implementasi_deklarasi_id'),
			'tindaks' => array(self::HAS_MANY, 'Tindak', 'tindak_deklarasi_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'deklarasi_id' => 'Deklarasi',
			'deklarasi_judul' => 'Deklarasi Judul',
			'deklarasi_focal' => 'Deklarasi Focal',
			'deklarasi_user_id' => 'Deklarasi User',
			'deklarasi_adddate' => 'Deklarasi Adddate',
			'deklarasi_summit_id' => 'Deklarasi Summit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('deklarasi_id',$this->deklarasi_id);
		$criteria->compare('deklarasi_judul',$this->deklarasi_judul,true);
		$criteria->compare('deklarasi_focal',$this->deklarasi_focal,true);
		$criteria->compare('deklarasi_user_id',$this->deklarasi_user_id);
		$criteria->compare('deklarasi_adddate',$this->deklarasi_adddate,true);
		$criteria->compare('deklarasi_summit_id',$this->deklarasi_summit_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Deklarasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
