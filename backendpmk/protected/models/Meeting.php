<?php

/**
 * This is the model class for table "meeting".
 *
 * The followings are the available columns in table 'meeting':
 * @property integer $meeting_id
 * @property string $meeting_title
 * @property integer $meeting_sektoral_id
 * @property integer $meeting_urutan
 * @property string $meeting_ketua
 * @property integer $meeting_sektoralsub_id
 * @property integer $meeting_urutansub
 * @property string $meeting_ketuasub
 * @property string $meeting_lokasi
 * @property string $meeting_negara
 * @property string $meeting_bahasan
 * @property string $meeting_hasil
 * @property string $meeting_bperihal
 * @property string $meeting_bpendahuluan
 * @property string $meeting_bisi
 * @property string $meeting_btindaklanjut
 * @property string $meeting_foto
 * @property string $meeting_fhasil
 * @property string $meeting_fbrafaks
 * @property string $meeting_flaint1
 * @property string $meeting_flain1
 * @property string $meeting_flaint2
 * @property string $meeting_flain2
 * @property string $meeting_flaint3
 * @property string $meeting_flain3
 * @property string $meeting_flaint4
 * @property string $meeting_flain4
 * @property string $meeting_flaint5
 * @property string $meeting_flain5
 * @property integer $meeting_user_id
 * @property string $meeting_tgl
 * @property string $meeting_tglb
 * @property string $meeting_adddate
 * @property string $meeting_entri
 *
 * The followings are the available model relations:
 * @property Sektoral $meetingSektoral
 * @property Sektoralsub $meetingSektoralsub
 * @property User $meetingUser
 */
class Meeting extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'meeting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('meeting_entri', 'required'),
			array('meeting_sektoral_id, meeting_sektoralsub_id, meeting_user_id', 'numerical', 'integerOnly'=>true),
			array('meeting_urutan, meeting_urutansub','length', 'max'=>5),
			array('meeting_title, meeting_ketua, meeting_ketuasub, meeting_lokasi, meeting_negara, meeting_foto, meeting_fhasil, meeting_fbrafaks, meeting_flaint1, meeting_flain1, meeting_flaint2, meeting_flain2, meeting_flaint3, meeting_flain3, meeting_flaint4, meeting_flain4, meeting_flaint5, meeting_flain5, meeting_entri', 'length', 'max'=>255),
			array('meeting_bahasan, meeting_hasil, meeting_bperihal, meeting_bpendahuluan, meeting_bisi, meeting_btindaklanjut, meeting_tgl, meeting_tglb, meeting_adddate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('meeting_id, meeting_title, meeting_sektoral_id, meeting_urutan, meeting_ketua, meeting_sektoralsub_id, meeting_urutansub, meeting_ketuasub, meeting_lokasi, meeting_negara, meeting_bahasan, meeting_hasil, meeting_bperihal, meeting_bpendahuluan, meeting_bisi, meeting_btindaklanjut, meeting_foto, meeting_fhasil, meeting_fbrafaks, meeting_flaint1, meeting_flain1, meeting_flaint2, meeting_flain2, meeting_flaint3, meeting_flain3, meeting_flaint4, meeting_flain4, meeting_flaint5, meeting_flain5, meeting_user_id, meeting_tgl, meeting_tglb, meeting_adddate, meeting_entri', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'meetingSektoral' => array(self::BELONGS_TO, 'Sektoral', 'meeting_sektoral_id'),
			'meetingSektoralsub' => array(self::BELONGS_TO, 'Sektoralsub', 'meeting_sektoralsub_id'),
			'meetingUser' => array(self::BELONGS_TO, 'User', 'meeting_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'meeting_id' => 'Meeting',
			'meeting_title' => 'Meeting Title',
			'meeting_sektoral_id' => 'Meeting Sektoral',
			'meeting_urutan' => 'Meeting Urutan',
			'meeting_ketua' => 'Meeting Ketua',
			'meeting_sektoralsub_id' => 'Meeting Sektoralsub',
			'meeting_urutansub' => 'Meeting Urutansub',
			'meeting_ketuasub' => 'Meeting Ketuasub',
			'meeting_lokasi' => 'Meeting Lokasi',
			'meeting_negara' => 'Meeting Negara',
			'meeting_bahasan' => 'Meeting Bahasan',
			'meeting_hasil' => 'Meeting Hasil',
			'meeting_bperihal' => 'Meeting Bperihal',
			'meeting_bpendahuluan' => 'Meeting Bpendahuluan',
			'meeting_bisi' => 'Meeting Bisi',
			'meeting_btindaklanjut' => 'Meeting Btindaklanjut',
			'meeting_foto' => 'Meeting Foto',
			'meeting_fhasil' => 'Meeting Fhasil',
			'meeting_fbrafaks' => 'Meeting Fbrafaks',
			'meeting_flaint1' => 'Meeting Flaint1',
			'meeting_flain1' => 'Meeting Flain1',
			'meeting_flaint2' => 'Meeting Flaint2',
			'meeting_flain2' => 'Meeting Flain2',
			'meeting_flaint3' => 'Meeting Flaint3',
			'meeting_flain3' => 'Meeting Flain3',
			'meeting_flaint4' => 'Meeting Flaint4',
			'meeting_flain4' => 'Meeting Flain4',
			'meeting_flaint5' => 'Meeting Flaint5',
			'meeting_flain5' => 'Meeting Flain5',
			'meeting_user_id' => 'Meeting User',
			'meeting_tgl' => 'Meeting Tgl',
			'meeting_tglb' => 'Meeting Tglb',
			'meeting_adddate' => 'Meeting Adddate',
			'meeting_entri' => 'Meeting Entri',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('meeting_id',$this->meeting_id);
		$criteria->compare('meeting_title',$this->meeting_title,true);
		$criteria->compare('meeting_sektoral_id',$this->meeting_sektoral_id);
		$criteria->compare('meeting_urutan',$this->meeting_urutan);
		$criteria->compare('meeting_ketua',$this->meeting_ketua,true);
		$criteria->compare('meeting_sektoralsub_id',$this->meeting_sektoralsub_id);
		$criteria->compare('meeting_urutansub',$this->meeting_urutansub);
		$criteria->compare('meeting_ketuasub',$this->meeting_ketuasub,true);
		$criteria->compare('meeting_lokasi',$this->meeting_lokasi,true);
		$criteria->compare('meeting_negara',$this->meeting_negara,true);
		$criteria->compare('meeting_bahasan',$this->meeting_bahasan,true);
		$criteria->compare('meeting_hasil',$this->meeting_hasil,true);
		$criteria->compare('meeting_bperihal',$this->meeting_bperihal,true);
		$criteria->compare('meeting_bpendahuluan',$this->meeting_bpendahuluan,true);
		$criteria->compare('meeting_bisi',$this->meeting_bisi,true);
		$criteria->compare('meeting_btindaklanjut',$this->meeting_btindaklanjut,true);
		$criteria->compare('meeting_foto',$this->meeting_foto,true);
		$criteria->compare('meeting_fhasil',$this->meeting_fhasil,true);
		$criteria->compare('meeting_fbrafaks',$this->meeting_fbrafaks,true);
		$criteria->compare('meeting_flaint1',$this->meeting_flaint1,true);
		$criteria->compare('meeting_flain1',$this->meeting_flain1,true);
		$criteria->compare('meeting_flaint2',$this->meeting_flaint2,true);
		$criteria->compare('meeting_flain2',$this->meeting_flain2,true);
		$criteria->compare('meeting_flaint3',$this->meeting_flaint3,true);
		$criteria->compare('meeting_flain3',$this->meeting_flain3,true);
		$criteria->compare('meeting_flaint4',$this->meeting_flaint4,true);
		$criteria->compare('meeting_flain4',$this->meeting_flain4,true);
		$criteria->compare('meeting_flaint5',$this->meeting_flaint5,true);
		$criteria->compare('meeting_flain5',$this->meeting_flain5,true);
		$criteria->compare('meeting_user_id',$this->meeting_user_id);
		$criteria->compare('meeting_tgl',$this->meeting_tgl,true);
		$criteria->compare('meeting_tglb',$this->meeting_tglb,true);
		$criteria->compare('meeting_adddate',$this->meeting_adddate,true);
		$criteria->compare('meeting_entri',$this->meeting_entri,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Meeting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
