<?php

/**
 * This is the model class for table "implementasi".
 *
 * The followings are the available columns in table 'implementasi':
 * @property integer $implementasi_id
 * @property string $implementasi_keterangan
 * @property integer $implementasi_sektoral_id
 * @property integer $implementasi_user_id
 * @property string $implementasi_adddate
 * @property integer $implementasi_deklarasi_id
 * @property string $implementasi_entri
 *
 * The followings are the available model relations:
 * @property Deklarasi $implementasiDeklarasi
 * @property Sektoral $implementasiSektoral
 * @property User $implementasiUser
 */
class Implementasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'implementasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('implementasi_entri', 'required'),
			array('implementasi_sektoral_id, implementasi_user_id, implementasi_deklarasi_id', 'numerical', 'integerOnly'=>true),
			array('implementasi_entri', 'length', 'max'=>255),
			array('implementasi_keterangan, implementasi_adddate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('implementasi_id, implementasi_keterangan, implementasi_sektoral_id, implementasi_user_id, implementasi_adddate, implementasi_deklarasi_id, implementasi_entri', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'implementasiDeklarasi' => array(self::BELONGS_TO, 'Deklarasi', 'implementasi_deklarasi_id'),
			'implementasiSektoral' => array(self::BELONGS_TO, 'Sektoral', 'implementasi_sektoral_id'),
			'implementasiUser' => array(self::BELONGS_TO, 'User', 'implementasi_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'implementasi_id' => 'Implementasi',
			'implementasi_keterangan' => 'Implementasi Keterangan',
			'implementasi_sektoral_id' => 'Implementasi Sektoral',
			'implementasi_user_id' => 'Implementasi User',
			'implementasi_adddate' => 'Implementasi Adddate',
			'implementasi_deklarasi_id' => 'Implementasi Deklarasi',
			'implementasi_entri' => 'Implementasi Entri',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('implementasi_id',$this->implementasi_id);
		$criteria->compare('implementasi_keterangan',$this->implementasi_keterangan,true);
		$criteria->compare('implementasi_sektoral_id',$this->implementasi_sektoral_id);
		$criteria->compare('implementasi_user_id',$this->implementasi_user_id);
		$criteria->compare('implementasi_adddate',$this->implementasi_adddate,true);
		$criteria->compare('implementasi_deklarasi_id',$this->implementasi_deklarasi_id);
		$criteria->compare('implementasi_entri',$this->implementasi_entri,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Implementasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
