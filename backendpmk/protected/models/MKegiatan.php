<?php

/**
 * This is the model class for table "m_kegiatan".
 *
 * The followings are the available columns in table 'm_kegiatan':
 * @property integer $id
 * @property string $iku
 * @property string $kegiatan
 * @property integer $pj_sektoralsub
 * @property integer $pj_sektoral
 * @property integer $pj_kl
 *
 * The followings are the available model relations:
 * @property Sektoralsub $pjSektoralsub
 * @property Sektoral $pjSektoral
 * @property Kl $pjKl
 * @property RanRealisasi[] $ranRealisasis
 * @property RanTarget[] $ranTargets
 */
class MKegiatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'm_kegiatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('iku, kegiatan, pj_sektoralsub, pj_sektoral, pj_kl', 'required'),
			array('pj_sektoralsub, pj_sektoral, pj_kl', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, iku, kegiatan, pj_sektoralsub, pj_sektoral, pj_kl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pjSektoralsub' => array(self::BELONGS_TO, 'Sektoralsub', 'pj_sektoralsub'),
			'pjSektoral' => array(self::BELONGS_TO, 'Sektoral', 'pj_sektoral'),
			'pjKl' => array(self::BELONGS_TO, 'Kl', 'pj_kl'),
			'ranRealisasis' => array(self::HAS_MANY, 'RanRealisasi', 'kegiatan'),
			'ranTargets' => array(self::HAS_MANY, 'RanTarget', 'kegiatan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'iku' => 'Iku',
			'kegiatan' => 'Kegiatan',
			'pj_sektoralsub' => 'Pj Sektoralsub',
			'pj_sektoral' => 'Pj Sektoral',
			'pj_kl' => 'Pj Kl',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('iku',$this->iku,true);
		$criteria->compare('kegiatan',$this->kegiatan,true);
		$criteria->compare('pj_sektoralsub',$this->pj_sektoralsub);
		$criteria->compare('pj_sektoral',$this->pj_sektoral);
		$criteria->compare('pj_kl',$this->pj_kl);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MKegiatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
