<?php
/* @var $this meetingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'meetings',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');    
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-md-datetimepicker/css/bootstrap-material-datetimepicker.css');
?>
<style>
.hide_{
    display:none;
}

.show_{
    display:block;
}
</style>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- <h4 class="page-title">Pertemuan</h4> -->
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Daftar Pertemuan Pilar Sosial Budaya</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <div class="row">
                            <div class="col-md-2">
                                <button style="float:left;" id="btn-a" type="button" class="btn btn-success waves-effect waves-light">Tambah Pertemuan</button>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                        <br>
                        <table
                            id="listmeeting"
                            class="table table-striped table-bordered"
                            style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" style="width: 20%;">Pertemuan</th>
                                    <th scope="col" style="width: 20%;">Ketua</th>
                                    <th scope="col" style="width: 15%;">Ketua Pertemuan Tingkat Eselon I / Deputi</th>
                                    <th scope="col" style="width: 20%;">Lokasi Pertemuan</th>
                                    <th scope="col" style="width: 15%;">Tanggal Pertemuan</th>
                                    <th scope="col" style="width: 8%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
										$tr="";
										$user="";
										
										foreach($dataProvider as $k=>$row){  
											$button='<div class="button-items">
												<button type="button"  id="v_'.$row['meeting_id'].'" class="btn-v btn btn-primary btn-sm btn-block waves-effect waves-light"><i class="far fa-eye"></i> Lihat</button>
												<button type="button"  id="u_'.$row['meeting_id'].'" class="btn-u btn btn-warning btn-sm btn-block waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Update</button>
												<button type="button"  id="d_'.$row['meeting_id'].'" class="btn-d btn btn-danger btn-sm btn-block waves-effect waves-light"><i class="far fa-trash-alt"></i> Hapus</button>
											</div>';

											$tr.='<tr id="'.$row['meeting_id'].'">
												<td style="width: 2%;">'.($k+1).'</td>
												<td id="judul_'.$row['meeting_id'].'" style="width: 20%;">'.$row['meeting_title'].'</td>
												<td style="width: 20%;">'.$row['meeting_ketua'].'</td>
												<td style="width: 15%;">'.$row['meeting_ketuasub'].'</td>
												<td style="width: 20%;">'.$row['meeting_lokasi'].'<br>'.$row['meeting_negara'].'</td>
												<td style="width: 15%;">'.Yii::app()->dateFormatter->format("dd MMM yyyy",$row['meeting_tgl']).'</td>
												<td  style="width: 8%;">'.$button.'</td>
											</tr>';
										}
										echo $tr;
									?>
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade view-meeting" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="meeting_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="col-md-12">
						<h4 id="meeting_judul" class="mt-0 header-title"></h4>
                        <hr>
						<table class="table table-striped mb-0">
                            <tbody>
                                <tr>
                                	<th scope="row">Tanggal Pertemuan</th>
                                    <td> : </td><td><span id="meeting_tgl"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">Ketua Pertemuan</th>
                                    <td> : </td><td><span id="meeting_ketua"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">Ketua Pertemuan Tingkat Esselon I/ Deputi</th>
                                    <td> : </td><td><span id="meeting_ketuasub"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">Lokasi</th>
                                    <td> : </td><td><span id="meeting_lokasi"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">Bahasan Pertemuan</th>
                                    <td> : </td><td><span id="meeting_bahasan"></span></td>
                                </tr>
							</tbody>
                        </table>
						<h5>ISI LAPORAN</h5>
						<table class="table table-striped mb-0">
                            <tbody>
                                <tr>
                                	<th scope="row">Perihal</th>
                                    <td> : </td><td><span id="meeting_perihal"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">Pendahuluan</th>
                                    <td> : </td><td><span id="meeting_pendahuluan"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">Isi</th>
                                    <td> : </td><td><span id="meeting_isi"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">Tindak Lanjut</th>
                                    <td> : </td><td><span id="meeting_tindaklanjut"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">File Hasil</th>
                                    <td> : </td><td><span id="meeting_hasil"></span></td>
                                </tr>
								<tr>
                                	<th scope="row">File Laporan</th>
                                    <td> : </td><td><span id="meeting_laporan"></span></td>
                                </tr>
                                <tr>
                                	<th scope="row">File Tambahan Lainnya</th>
                                    <td> : </td><td><span id="meeting_tambahan"></span></td>
                                </tr>
							</tbody>
                        </table>
					</div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade form-meeting" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-meeting" method="post" enctype="multipart/form-data">                        
                <div class="modal-body">
                        <input type="hidden" id="f_meeting_id" name="input_meeting_id" value="">
                        <input type="hidden" id="f_sektoral_id" name="input_sektoral_id" value="">
                        <input type="hidden" id="f_subsektoral_id" name="input_subsektoral_id" value="">
                        <input type="hidden" id="f_sektoral_nama" name="input_sektoral_nama" value="">
                        <input type="hidden" id="f_subsektoral_nama" name="input_subsektoral_nama" value="">
                        
                        <div class="row">
							<div class="col-md-12">
                                    <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Badan Sektoral : <strong class="text-danger">*</strong></label>  
									<div class="col-sm-8">		
                                            <select name="input_sektoral_id" id="f_sektoral" class="form-control select2">
												<option>:: Pilih Badan Sektoral ::</option>
                                                <option value="0">KTT ASEAN</option>
												<?php
                                                    $opt="";
                                                    foreach($daftarSektoral as $r){
                                                        $opt.="<option value='".$r->sektoralsub_id."'>".$r->sektoralsub_nama."</option>";
                                                    }
                                                    echo $opt;
                                                ?>
											</select>
                                    </div>
                                </div>
                            </div>

                            <div id="pertemuan" class="hide_ col-md-12 cek">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pertemuan : <strong class="text-danger">*</strong></label>  
                                    <div class="col-sm-8">    
                                        <select name="input_pertemuan" id="f_pertemuan" class="form-control select2"></select>
                                    </div>
                                </div>
                            </div>
                            
                                
                            <div style="display:none;" id="sektoral" class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
                                <div class="col-md-12">
                                    <h3 id="sektoral_pertemuan"></h3>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Pertemuan Ke <strong class="text-danger">*</strong></label>
                                        <div class="col-sm-8">
                                            <input name="input_s_pertemuanke" id="f_s_pertemuanke" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Ketua Pertemuan <strong class="text-danger">*</strong></label>
                                        <div class="col-sm-8">
                                            <input name="input_s_ketua" id="f_s_ketua" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="display:none;" id="subsektoral" class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
                                <div class="col-md-12">
                                    <h3 id="subsektoral_pertemuan"></h3>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Pertemuan Ke <strong class="text-danger">*</strong></label>
                                        <div class="col-sm-8">
                                            <input name="input_sub_pertemuanke" id="f_sub_pertemuanke" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Ketua Pertemuan <strong class="text-danger">*</strong></label>
                                        <div class="col-sm-8">
                                            <input name="input_sub_ketua" id="f_sub_ketua" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <div style="display:none;" id="isianrinci">        
                            <div id="lokasi" class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
                                
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Tanggal Pertemuan <strong class="text-danger">*</strong></label>
                                        <div class="col-sm-4">
                                            <input name="input_tanggal1" id="f_tanggal1" class="form-control" type="text"/>
                                        </div>
                                        s/d
                                        <div class="col-sm-4">
                                            <input name="input_tanggal2" id="f_tanggal2" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Kota <strong class="text-danger">*</strong></label>
                                        <div class="col-sm-8">
                                            <input name="input_kota" id="f_kota" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Negara <strong class="text-danger">*</strong></label>
                                        <div class="col-sm-8">
                                            <input name="input_negara" id="f_negara" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="form-group">
                                        <label>Upload Foto Pertemuan</label>
                                        <input id="f_foto" name="input_foto" type="file" class="filestyle" data-buttonname="btn-secondary">
                                    </div>
                                </div>
                            </div>

							
                            <div id="brafaks" class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
							<div class="col-md-12">
								<h3>LAPORAN</h3>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Perihal</label><br>
                                    <textarea class="text_wysiwyg" id="f_perihal" name="input_perihal" class="form-control" rows="4" placeholder="Overview"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Pendahuluan</label>
                                    <textarea class="text_wysiwyg" id="f_pendahuluan" name="input_pendahuluan" class="form-control" rows="4" placeholder="Isi meeting"></textarea>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label>Isi</label>
                                    <textarea class="text_wysiwyg" id="f_isi" name="input_isi" class="form-control" rows="4" placeholder="Isi meeting"></textarea>
                                </div>
                            </div>
							<div class="col-md-12">
                                <div class="form-group">
                                    <label>Tindak Lanjut</label>
                                    <textarea class="text_wysiwyg" id="f_tindaklanjut" name="input_tindaklanjut" class="form-control" rows="4" placeholder="Isi meeting"></textarea>
                                </div>
                            </div>
                            </div>

                            <div class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
							<div class="col-md-12" style="margin-top:10px;">
                                <div class="form-group">
                                    <label>Upload File Laporan</label>
                                    <input id="f_flaporan" name="input_flaporan" type="file" class="filestyle" data-buttonname="btn-secondary">
                                </div>
                            </div>
							<div class="col-md-12" style="margin-top:10px;">
                                <div class="form-group">
                                    <label>Upload File Hasil</label>
                                    <input id="f_fhasil" name="input_fhasil" type="file" class="filestyle" data-buttonname="btn-secondary">
                                </div>
                            </div>
                            </div>

                            <div class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Nama File Tambahan ke 1</label>
                                        <div class="col-sm-7">
                                            <input name="input_nft1" id="f_nft1" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="form-group">
                                        <label>Upload File Tambahan ke 1</label>
                                        <input id="f_ft1" name="input_ft1" type="file" class="filestyle" data-buttonname="btn-secondary">
                                    </div>
                                </div>
                            </div>

                            <div class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                    <label class="col-sm-5 col-form-label">Nama File Tambahan ke 2</label>
                                        <div class="col-sm-7">
                                            <input name="input_nft2" id="f_nft2" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="form-group">
                                        <label>Upload File Tambahan ke 2</label>
                                        <input id="f_ft2" name="input_ft2" type="file" class="filestyle" data-buttonname="btn-secondary">
                                    </div>
                                </div>
                            </div>

                            <div class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                    <label class="col-sm-5 col-form-label">Nama File Tambahan ke 3</label>
                                        <div class="col-sm-7">
                                            <input name="input_nft3" id="f_nft3" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="form-group">
                                        <label>Upload File Tambahan ke 3</label>
                                        <input id="f_ft3" name="input_ft3" type="file" class="filestyle" data-buttonname="btn-secondary">
                                    </div>
                                </div>
                            </div>

                            <div class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                    <label class="col-sm-5 col-form-label">Nama File Tambahan ke 4</label>
                                        <div class="col-sm-7">
                                            <input name="input_nft4" id="f_nft4" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="form-group">
                                        <label>Upload File Tambahan ke 4</label>
                                        <input id="f_ft4" name="input_ft4" type="file" class="filestyle" data-buttonname="btn-secondary">
                                    </div>
                                </div>
                            </div>

                            <div class="row m-r-10 m-l-10 shadow p-3 mb-5 bg-white rounded">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-5 col-form-label">Nama File Tambahan ke 5</label>
                                        <div class="col-sm-7">
                                            <input name="input_nft5" id="f_nft5" class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;">
                                    <div class="form-group">
                                        <label>Upload File Tambahan ke 5</label>
                                        <input id="f_ft5" name="input_ft5" type="file" class="filestyle" data-buttonname="btn-secondary">
                                    </div>
                                </div>
                            </div>

							<div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Nama Pengentri <strong class="text-danger">*</strong></label>
                                    <div class="col-sm-8">
                                        <input name="input_namaentry" id="f_namaentry" class="form-control" type="text"/>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_saving" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-gradient-primary text-white">
                <h5 class="modal-title">Informasi</h5>
            </div>
            <div class="modal-body .text-gray-100 text-center">
                <p>Proses Upload Sedang Dilakukan</p>
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
                <p class="text-primary">File Upload Sedang Dalam Proses...</p>
            </div>
            <div class="modal-footer bg-gradient-primary text-white text-center">
                <strong>Mohon Untuk Tidak Keluar/Close Dari Halaman Ini Sampai Proses Upload Selesai</strong>
            </div>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/bootstrap-md-datetimepicker/js/moment-with-locales.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/bootstrap-md-datetimepicker/js/bootstrap-material-datetimepicker.js', CClientScript::POS_END);
	
	Yii::app()->clientScript->registerScript('initdatatable', '

        function isHTML(str) {
            var a = document.createElement("div");
            a.innerHTML = str;
        
            for (var c = a.childNodes, i = c.length; i--; ) {
            if (c[i].nodeType == 1) return true; 
            }
        
            return false;
        }
        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }

		$(".btn-v").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            
            $.ajax({
				url: "'.Yii::app()->createUrl("meeting/view").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
					$("#loading").css("display", "block");
					
					$("#meeting_title").html("-");
					$("#meeting_judul").html("-");

					$("#meeting_tgl").html("-");
					$("#meeting_ketua").html("-");
					$("#meeting_ketuasub").html("-");
					$("#meeting_lokasi").html("-");
					$("#meeting_bahasan").html("-");
					$("#meeting_perihal").html("-");
					$("#meeting_pendahuluan").html("-");
					$("#meeting_isi").html("-");
					$("#meeting_tindaklanjut").html("-");
					$("#meeting_hasil").html("-");
					$("#meeting_laporan").html("-");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);

					$("#meeting_title").html("Meeting #"+res.data.id);
					$("#meeting_judul").html(res.data.pertemuan);

					$("#meeting_tgl").html(res.data.tgl);
					$("#meeting_ketua").html(res.data.ketua);
					$("#meeting_ketuasub").html(res.data.ketuasub);
					$("#meeting_lokasi").html(res.data.lokasi);
					$("#meeting_bahasan").html(res.data.bahasan);
					$("#meeting_perihal").html(res.data.perihal);
					$("#meeting_pendahuluan").html(res.data.pendahuluan);
					$("#meeting_isi").html(res.data.isi);
					$("#meeting_tindaklanjut").html(res.data.tindaklanjut);
					$("#meeting_hasil").html(res.data.filehasil);
					$("#meeting_laporan").html(res.data.filelaporan);
					$("#meeting_tambahan").html(res.data.filelainnya);
					$(".view-meeting").modal("show");
				}
            });
		});

		$(".btn-u").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
			console.log(res[1]);
			$.ajax({
				url: "'.Yii::app()->createUrl("meeting/view").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#loading").css("display", "block");
                    $("#f_perihal").summernote("code","");
                    $("#f_isi").summernote("code","");
                    $("#f_pendahuluan").summernote("code","");
                    $("#f_tindaklanjut").summernote("code","");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
                    var selectedpertemuan="";
                
                    if(res.data.sektoralsubid==null){
                        selectedpertemuan+="0_";
                    } else {
                        selectedpertemuan+=res.data.sektoralsubid+"_";
                    }

                    if(res.data.sektoralid==null){
                        selectedpertemuan+="0";
                    } else {
                        selectedpertemuan+=res.data.sektoralid;
                    }
                    
                    console.log(res.data.opt);

                    $("#subsektoral_pertemuan").html(res.data.sektoralsubnama);
                    $("#sektoral_pertemuan").html(res.data.sektoralnama);
                    $("#f_meeting_id").val(res.data.id);
                    $("#f_sektoral_id").val(res.data.sektoralid);
                    $("#f_sektoral").val(res.data.sektoralid).trigger("change");
                    $("#f_subsektoral_id").val(res.data.sektoralsubid);
                    $("#f_pertemuan").html(res.data.opt);    
                    $("#f_pertemuan").val(selectedpertemuan).trigger("change");
                    $("#f_s_pertemuanke").val(res.data.pertemuanke);
                    $("#f_s_ketua").val(res.data.ketua);
                    $("#f_sub_pertemuanke").val(res.data.pertemuansubke);
                    $("#f_sub_ketua").val(res.data.ketuasub);
                    $("#f_kota").val(res.data.lokasikota);
                    $("#f_negara").val(res.data.lokasinegara);
                    $("#f_tanggal1").val(res.data.start);
                    $("#f_tanggal2").val(res.data.end);

                    /*
                    $("#f_perihal").summernote("editor.pasteHTML", res.data.perihal);
                    $("#f_isi").summernote("editor.pasteHTML", res.data.isi);
                    $("#f_pendahuluan").summernote("editor.pasteHTML", res.data.pendahuluan);
                    $("#f_tindaklanjut").summernote("editor.pasteHTML", res.data.tindaklanjut); 
                    */

                    if( isHTML(res.data.perihal) ){
                        $("#f_perihal").summernote("code", res.data.perihal);
                        // $("#f_perihal").summernote("pasteHTML", res.data.perihal);
                        console.log("ISHTML");
                    } else {
                        $("#f_perihal").summernote("insertText", res.data.perihal);
                        console.log("NOTHTML");
                    }
                    
                    if( isHTML(res.data.isi) ){
                        $("#f_isi").summernote("code", res.data.isi);
                        console.log("ISHTML2");
                    } else {
                        $("#f_isi").summernote("insertText", res.data.isi);
                        console.log("NOTHTML2");
                    }

                    if( isHTML(res.data.pendahuluan) ){
                        $("#f_pendahuluan").summernote("code", res.data.pendahuluan);
                    } else {
                        $("#f_pendahuluan").summernote("insertText", res.data.pendahuluan);
                    }

                    if( isHTML(res.data.tindaklanjut) ){
                        $("#f_tindaklanjut").summernote("code", res.data.tindaklanjut);
                    } else {
                        $("#f_tindaklanjut").summernote("insertText", res.data.tindaklanjut);
                    }

                    $("#f_nft1").val(res.data.file1);
                    $("#f_nft2").val(res.data.file2);
                    $("#f_nft3").val(res.data.file3);
                    $("#f_nft4").val(res.data.file4);
                    $("#f_nft5").val(res.data.file5);
                    $("#f_nft5").val(res.data.file5);
                    $("#f_namaentry").val(res.data.entry);

					if(res.data.sektoralid==0){
                        show("sektoral");
                        hide("subsektoral");
                    } else if(res.data.pertemuanke!=null && res.data.pertemuansubke==null){
                        show("sektoral");
                        $("#sektoral_pertemuan").html(res.data.sektoralnama);
                        $("#f_sektoral_nama").val(res.data.sektoralnama);
                        hide("subsektoral");
                    } else if(res.data.pertemuanke==null && res.data.pertemuansubke!=null){
                        hide("sektoral");
                        show("subsektoral");
                        $("#subsektoral_pertemuan").html(res.data.sektoralsubnama);
                        $("#f_subsektoral_nama").html(res.data.sektoralsubnama);
                    } else if(res.data.pertemuanke!=null && res.data.pertemuansubke!=null){
                        show("sektoral");
                        $("#sektoral_pertemuan").html(res.data.sektoralnama);
                        $("#f_sektoral_nama").val(res.data.sektoralnama);
                        show("subsektoral");
                        $("#subsektoral_pertemuan").html(res.data.sektoralsubnama);
                        $("#f_subsektoral_nama").html(res.data.sektoralsubnama);
                    }

                    if(res.data.sektoralid>0){
                        show("pertemuan");
                    }
                    show("isianrinci");
                    $(".form-meeting").modal("show");
				}
			});
			
        });
        
        $("#form-meeting").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var tgl1=$("#f_tanggal1").val();
            var tgl2=$("#f_tanggal2").val();
            var entry=$("#f_namaentry").val();

            var ok=true;
            if(!tgl1 || !tgl2){
                alert("tanggal harus terisi");
                $("#btn-form").attr("disabled",false);
            
                ok=false;
            }

            if(!entry){
                alert("Nama Pengentry harus terisi");
                $("#btn-form").attr("disabled",false);
            
                ok=false;
            }

            if(ok){
                var formdata=new FormData(this);   
                console.log(formdata);
                $.ajax({
                    url: "'.Yii::app()->createUrl("meeting/create").'",
                    type: "POST",
                    data:  formdata,
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend : function(event){
                        $(".form-meeting").modal("hide");
                        $("#modal_saving").modal("show");
                        $("#loading").css("display", "block");
                        $("#btn-form").attr("disabled",false);
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                       
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                            showCancelButton: false,
                            confirmButtonClass: "btn btn-success",
                        }).then(function () {
                           $("#btn-form").attr("disabled",false);
                           $(".form-meeting").modal("hide");
                           $("#modal_saving").modal("hide");
                           location.reload();
                        }, function (dismiss) {
                        });                    
                    },
                    error: function(data){
                        alert("Upload Gagal, File Terlalu Besar");
                        $("#btn-form").attr("disabled",false);
                        $("#modal_saving").modal("hide");
                        $("#loading").css("display", "none");
                        $(".form-meeting").modal("show");
                    }
                });  
            }
        });

        $(".btn-d").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            
            swal({
                title: "Yakin menghapus meeting ini?",
                text: "meeting yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("meeting/delete").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })

			
        });

        $("#f_sektoral").select2({
            placeholder : " :: Pilih Badan Sektoral :: "
        });

        $("#f_pertemuan").select2({
            placeholder : " :: Pilih Pertemuan :: "
        });

        $("#f_pertemuan").on("select2:select", function (e) { 
            $("#sektoral_pertemuan").html("");
            $("#subsektoral_pertemuan").html("");
            $("#f_sub_pertemuanke").val("");
            $("#f_sub_ketua").val("");
            $("#f_s_pertemuanke").val("");
            $("#f_s_ketua").val("");
            hide("isianrinci");

            var v = $("#f_pertemuan option:selected").val();
            var f = $("#f_pertemuan option:selected").text();

            var res=v.split("_");
            
            // var sektoral=res[1];
            // var subsektoral=res[0];

            console.log(res[0]+" >> "+res[1]);
            if(res[0]>0){
                $("#f_subsektoral_id").val(res[0]);
                $("#f_subsektoral_nama").val(f);
                $("#subsektoral_pertemuan").html(f);
                
                $("#f_sektoral_id").val("");
                $("#f_sektoral_nama").val("");
                $("#sektoral_pertemuan").html("");

                show("subsektoral");
                hide("sektoral");
            }

            if(res[1]>0){
                $("#f_sektoral_id").val(res[1]);
                $("#f_sektoral_nama").val(f);
                $("#sektoral_pertemuan").html(f);

                $("#f_subsektoral_id").val("");
                $("#f_subsektoral_nama").val("");
                $("#subsektoral_pertemuan").html("");
                
                show("sektoral");
                hide("subsektoral");
            }

            if(res[0]>0 && res[1]>0){
                $("#f_sektoral_id").val(res[1]);
                $("#f_subsektoral_id").val(res[0]);

                var res2=f.split("dan");
                $("#subsektoral_pertemuan").html(res2[0]);
                $("#f_subsektoral_nama").val(res2[0]);
                $("#sektoral_pertemuan").html(res2[1]);
                $("#f_sektoral_nama").val(res2[1]);

                show("sektoral");
                show("subsektoral");
            }

            show("isianrinci");
        });

        $("#f_sektoral").on("select2:select", function (e) { 
            var f = $("#f_sektoral").val();
            $("#sektoral_pertemuan").html("");
            hide("isianrinci");
            hide("sektoral");
            hide("subsektoral");
            $("#f_tanggal1").val("");
            $("#f_tanggal2").val("");
            if(f!=""){
                if(f==0){
                    $("#f_sektoral_id").val("0");
                    $("#sektoral_pertemuan").html("KTT ASEAN");
                    hide("pertemuan");
                    show("sektoral");
                    show("isianrinci");
                }
                else{
                    $("#f_sektoral_id").val(f);
                    $.ajax({
                        url: "'.Yii::app()->createUrl("meeting/getsektoral").'",
                        type: "POST",
                        data: { id:f },
                        beforeSend : function(event){
                            $("#loading").css("display", "block");
                            $("#pertemuan").removeClass("show_").addClass("hide_");
                        },
                        success: function(result) {
                            $("#loading").css("display", "none");
                            var res = JSON.parse(result);
                            if(res.status == 1){
                                $("#pertemuan").removeClass("hide_").addClass("show_");
                                show("pertemuan");
                                $("#f_pertemuan").html(res.data);                
                            } else {
                                alert(res.message);
                            }
                        }
                    });
                }
            }
        });

        $("#btn-a").on("click", function(){  
            $("#sektoral").hide();
            $("#subsektoral").hide();
            $("#pertemuan").removeClass("show_").addClass("hide_");
            $("#isianrinci").hide();
            
            $("#loading").css("display", "block");
            $("#f_perihal").summernote("code","");
            $("#f_isi").summernote("code","");
            $("#f_pendahuluan").summernote("code","");
            $("#f_tindaklanjut").summernote("code","");
            $(".form-meeting").modal("show");
            $("#f_sektoral").val(null).trigger("change");
            hide("isianrinci");
            hide("sektoral");
            hide("pertemuan");
            $("#f_meeting_id").val(null);
            $("#f_namaentry").val(null);
            $("#loading").css("display", "none");
		});

		$(document).ready(function() {

            $("#f_tanggal1").bootstrapMaterialDatePicker({ 
                format : "YYYY-MM-DD", 
                weekStart : 0, 
                time: false ,
            }).on("change", function(e, date) {
                $("#f_tanggal2").bootstrapMaterialDatePicker("setMinDate", date);
            });
            
            $("#f_tanggal2").bootstrapMaterialDatePicker({ 
                format : "YYYY-MM-DD", 
                weekStart : 0, 
                time: false ,
            });

			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listmeeting\').DataTable({
                "columnDefs": [{targets: [5], "type":"date"}]
            });

            $(".form-meeting").on("hidden.bs.modal", function () {
                $("#loading").css("display", "none");
            })
			
		} );
	');
?>