<?php
/* @var $this MeetingController */
/* @var $data Meeting */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->meeting_id), array('view', 'id'=>$data->meeting_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_title')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_sektoral_id')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_sektoral_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_urutan')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_urutan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_ketua')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_ketua); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_sektoralsub_id')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_sektoralsub_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_urutansub')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_urutansub); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_ketuasub')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_ketuasub); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_lokasi')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_lokasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_negara')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_negara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_bahasan')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_bahasan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_hasil')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_hasil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_bperihal')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_bperihal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_bpendahuluan')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_bpendahuluan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_bisi')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_bisi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_btindaklanjut')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_btindaklanjut); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_foto')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_foto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_fhasil')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_fhasil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_fbrafaks')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_fbrafaks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flaint1')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flaint1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flain1')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flain1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flaint2')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flaint2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flain2')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flain2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flaint3')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flaint3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flain3')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flain3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flaint4')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flaint4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flain4')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flain4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flaint5')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flaint5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_flain5')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_flain5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_tgl')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_tgl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_tglb')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_tglb); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_adddate')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_adddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meeting_entri')); ?>:</b>
	<?php echo CHtml::encode($data->meeting_entri); ?>
	<br />

	*/ ?>

</div>