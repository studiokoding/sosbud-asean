<?php
/* @var $this MeetingController */
/* @var $model Meeting */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'meeting-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_title'); ?>
		<?php echo $form->textField($model,'meeting_title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_sektoral_id'); ?>
		<?php echo $form->textField($model,'meeting_sektoral_id'); ?>
		<?php echo $form->error($model,'meeting_sektoral_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_urutan'); ?>
		<?php echo $form->textField($model,'meeting_urutan'); ?>
		<?php echo $form->error($model,'meeting_urutan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_ketua'); ?>
		<?php echo $form->textField($model,'meeting_ketua',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_ketua'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_sektoralsub_id'); ?>
		<?php echo $form->textField($model,'meeting_sektoralsub_id'); ?>
		<?php echo $form->error($model,'meeting_sektoralsub_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_urutansub'); ?>
		<?php echo $form->textField($model,'meeting_urutansub'); ?>
		<?php echo $form->error($model,'meeting_urutansub'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_ketuasub'); ?>
		<?php echo $form->textField($model,'meeting_ketuasub',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_ketuasub'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_lokasi'); ?>
		<?php echo $form->textField($model,'meeting_lokasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_lokasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_negara'); ?>
		<?php echo $form->textField($model,'meeting_negara',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_negara'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_bahasan'); ?>
		<?php echo $form->textArea($model,'meeting_bahasan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meeting_bahasan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_hasil'); ?>
		<?php echo $form->textArea($model,'meeting_hasil',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meeting_hasil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_bperihal'); ?>
		<?php echo $form->textArea($model,'meeting_bperihal',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meeting_bperihal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_bpendahuluan'); ?>
		<?php echo $form->textArea($model,'meeting_bpendahuluan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meeting_bpendahuluan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_bisi'); ?>
		<?php echo $form->textArea($model,'meeting_bisi',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meeting_bisi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_btindaklanjut'); ?>
		<?php echo $form->textArea($model,'meeting_btindaklanjut',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meeting_btindaklanjut'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_foto'); ?>
		<?php echo $form->textField($model,'meeting_foto',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_foto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_fhasil'); ?>
		<?php echo $form->textField($model,'meeting_fhasil',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_fhasil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_fbrafaks'); ?>
		<?php echo $form->textField($model,'meeting_fbrafaks',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_fbrafaks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flaint1'); ?>
		<?php echo $form->textField($model,'meeting_flaint1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flaint1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flain1'); ?>
		<?php echo $form->textField($model,'meeting_flain1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flain1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flaint2'); ?>
		<?php echo $form->textField($model,'meeting_flaint2',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flaint2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flain2'); ?>
		<?php echo $form->textField($model,'meeting_flain2',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flain2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flaint3'); ?>
		<?php echo $form->textField($model,'meeting_flaint3',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flaint3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flain3'); ?>
		<?php echo $form->textField($model,'meeting_flain3',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flain3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flaint4'); ?>
		<?php echo $form->textField($model,'meeting_flaint4',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flaint4'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flain4'); ?>
		<?php echo $form->textField($model,'meeting_flain4',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flain4'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flaint5'); ?>
		<?php echo $form->textField($model,'meeting_flaint5',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flaint5'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_flain5'); ?>
		<?php echo $form->textField($model,'meeting_flain5',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_flain5'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_user_id'); ?>
		<?php echo $form->textField($model,'meeting_user_id'); ?>
		<?php echo $form->error($model,'meeting_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_tgl'); ?>
		<?php echo $form->textField($model,'meeting_tgl'); ?>
		<?php echo $form->error($model,'meeting_tgl'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_tglb'); ?>
		<?php echo $form->textField($model,'meeting_tglb'); ?>
		<?php echo $form->error($model,'meeting_tglb'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_adddate'); ?>
		<?php echo $form->textField($model,'meeting_adddate'); ?>
		<?php echo $form->error($model,'meeting_adddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meeting_entri'); ?>
		<?php echo $form->textField($model,'meeting_entri',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meeting_entri'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->