<?php
/* @var $this MeetingController */
/* @var $model Meeting */

$this->breadcrumbs=array(
	'Meetings'=>array('index'),
	$model->meeting_id,
);

$this->menu=array(
	array('label'=>'List Meeting', 'url'=>array('index')),
	array('label'=>'Create Meeting', 'url'=>array('create')),
	array('label'=>'Update Meeting', 'url'=>array('update', 'id'=>$model->meeting_id)),
	array('label'=>'Delete Meeting', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->meeting_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Meeting', 'url'=>array('admin')),
);
?>

<h1>View Meeting #<?php echo $model->meeting_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'meeting_id',
		'meeting_title',
		'meeting_sektoral_id',
		'meeting_urutan',
		'meeting_ketua',
		'meeting_sektoralsub_id',
		'meeting_urutansub',
		'meeting_ketuasub',
		'meeting_lokasi',
		'meeting_negara',
		'meeting_bahasan',
		'meeting_hasil',
		'meeting_bperihal',
		'meeting_bpendahuluan',
		'meeting_bisi',
		'meeting_btindaklanjut',
		'meeting_foto',
		'meeting_fhasil',
		'meeting_fbrafaks',
		'meeting_flaint1',
		'meeting_flain1',
		'meeting_flaint2',
		'meeting_flain2',
		'meeting_flaint3',
		'meeting_flain3',
		'meeting_flaint4',
		'meeting_flain4',
		'meeting_flaint5',
		'meeting_flain5',
		'meeting_user_id',
		'meeting_tgl',
		'meeting_tglb',
		'meeting_adddate',
		'meeting_entri',
	),
)); ?>
