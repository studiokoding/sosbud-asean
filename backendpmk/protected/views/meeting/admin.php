<?php
/* @var $this MeetingController */
/* @var $model Meeting */

$this->breadcrumbs=array(
	'Meetings'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Meeting', 'url'=>array('index')),
	array('label'=>'Create Meeting', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#meeting-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Meetings</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'meeting-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'meeting_id',
		'meeting_title',
		'meeting_sektoral_id',
		'meeting_urutan',
		'meeting_ketua',
		'meeting_sektoralsub_id',
		/*
		'meeting_urutansub',
		'meeting_ketuasub',
		'meeting_lokasi',
		'meeting_negara',
		'meeting_bahasan',
		'meeting_hasil',
		'meeting_bperihal',
		'meeting_bpendahuluan',
		'meeting_bisi',
		'meeting_btindaklanjut',
		'meeting_foto',
		'meeting_fhasil',
		'meeting_fbrafaks',
		'meeting_flaint1',
		'meeting_flain1',
		'meeting_flaint2',
		'meeting_flain2',
		'meeting_flaint3',
		'meeting_flain3',
		'meeting_flaint4',
		'meeting_flain4',
		'meeting_flaint5',
		'meeting_flain5',
		'meeting_user_id',
		'meeting_tgl',
		'meeting_tglb',
		'meeting_adddate',
		'meeting_entri',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
