<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Backend Forkom ASEAN</title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl;?>/images/favicon.ico">

        <link href="<?php echo Yii::app()->getBaseUrl(true);?>/public/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getBaseUrl(true);?>/public/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Yii::app()->getBaseUrl(true);?>/public/css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <!-- Background -->
        <div class="account-pages"></div>
        <!-- Begin page -->
        <div class="wrapper-page">
            <?php echo $content; ?>
        <div class="m-t-40 text-center">
            <p class="text-muted">© <?= date('Y') ?>. Kementerian Koordinator Bidang Pembangunan Manusia dan Kebudayaan</i> </p>
        </div>

        </div>
       
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="<?php echo Yii::app()->getBaseUrl(true);?>/public/js/jquery.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true);?>/public/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true);?>/public/js/jquery.slimscroll.js"></script>
        <script src="<?php echo Yii::app()->getBaseUrl(true);?>/public/js/waves.min.js"></script>

        <script src="<?php echo Yii::app()->getBaseUrl(true);?>/public/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

        <!-- App js -->
        <script src="<?php echo Yii::app()->getBaseUrl(true);?>/public/js/app.js"></script>

    </body>

</html>