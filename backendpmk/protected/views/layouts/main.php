<?php
	//dhonieko@2020
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <script src="<?php echo Yii::app()->baseUrl; ?>/public/js/jquery.min.js"></script>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Backend Sosbud PMK</title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl;?>/images/favicon.ico">

        <?php
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/morris/morris.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/css/bootstrap.min.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/css/icons.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/css/style.css');

        $user = User::model()->findByPk(Yii::app()->user->id);
        ?>
    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <a href="<?php echo Yii::app()->getBaseUrl(true);?>"><h2 style="color:white;"><?= $user->userSektoralsub->sektoralsubSektoral->sektoral_nama ?></h2></a>
                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">
                        <?php //$belumdibaca=MsgLog::model()->count("(msg_to=:unit) AND is_read=:read", array('unit' =>Yii::app()->user->unit_es3,'read'=>FALSE)); ?>
                                               
                        <ul class="navbar-right d-flex list-inline float-right mb-0">
                            <li class="dropdown notification-list">
                                <a href="<?= Yii::app()->createUrl('site/logout')?>" style="color:white;" class="nav-link arrow-none waves-effect nav-user waves-light" role="button" >Log Out</a>                                                            
                            </li>

                            <li class="dropdown notification-list">
                                <div class="dropdown notification-list">
                                    <a href="<?= Yii::app()->createUrl('user/profile',array('id'=>Yii::app()->user->id))?>" style="color:white;" class="nav-link arrow-none waves-effect nav-user waves-light" role="button" >Profile <?php echo Yii::app()->user->name;?>&nbsp;<img src="<?php echo Yii::app()->baseUrl;?>/images/user.jpg" alt="Profile Pengguna" class="rounded-circle"></a>                                                     
                                </div>
                            </li>

                        </ul>
    
    
    
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu">
                                <a id="dashboard" href="<?php echo Yii::app()->getBaseUrl(true);?>"><i class="mdi mdi-home"></i>Dashboard</a>
                            </li>

                            <li class="has-submenu">
                                <a id="input_berita" href="<?php echo Yii::app()->createUrl('berita/utama');?>"><i class="mdi mdi-newspaper" ></i>Input Berita Utama</a>
                            </li>

                            <!--<li class="has-submenu">
                                <a id="input_sekilas_info" href="<?php echo Yii::app()->createUrl('berita/info');?>"><i class="mdi mdi-newspaper" ></i>Input Sekilas Info</a>
                            </li>-->

                            <li class="has-submenu">
                                <a id="input_pertemuan" href="<?php echo Yii::app()->createUrl('meeting');?>"><i class="fas fa-calendar-alt" ></i>Input Pertemuan</a>
                            </li>

                            <li class="has-submenu">
                                <a id="input_deklarasi" href="<?php echo Yii::app()->createUrl('deklarasi');?>"><i class="fas fa-chalkboard-teacher" ></i>Input Deklarasi</a>
                            </li>
                            
                            <li class="has-submenu">
                                <a id="input_target" href="<?= Yii::app()->createUrl('ran/inputtarget') ?>"><i class="far fa-calendar-check"></i>Input Target</a>
                            </li>

                            <li class="has-submenu">
                                <a id="input_realisasi" href="<?= Yii::app()->createUrl('ran/inputrealisasi') ?>"><i class="far fa-calendar-check"></i>Input Realisasi</a>
                            </li>

                            <?php if($user->level==10): ?>
                                <li id="statistik" class="has-submenu">
                                    <a href="<?php echo Yii::app()->createUrl('laporan/statistik');?>"><i class="fas fa-chart-bar"></i>Statistik Laporan</a>
                                </li>
                            

                            <li class="has-submenu">
                                <a href="#"><i class="mdi mdi-settings" id="data_master"></i>Data Master</a>
                                <ul class="submenu">
                                    <li><a href="<?php echo Yii::app()->createUrl('master/kl');?>">K/L</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('master/sektoral');?>">Badan Sektoral</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('master/sektoralsub');?>">Badan Sektoral I</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('master/ran');?>">Rencana Aksi Nasional</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('master/link');?>">Link</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('master/config');?>">Pengaturan</a></li>
                                    <li><a href="<?php echo Yii::app()->createUrl('master/user');?>">Pengguna</a></li>
                                </ul>
                            </li>
                            <?php endif ?>
                            <?php //} ?>
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <!-- page wrapper start -->
        <div class="wrapper">
			<?php echo $content; ?>
        </div>
        <!-- page wrapper end -->

        <!-- Footer -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    <p class="text-muted">© <?= date('Y') ?>. Kementerian Koordinator Bidang Pembangunan Manusia dan Kebudayaan</i> </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->
        <?php
            //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/jquery.min.js', CClientScript::POS_HEAD); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/bootstrap.bundle.min.js', CClientScript::POS_HEAD); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/jquery.slimscroll.js', CClientScript::POS_HEAD); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/waves.min.js', CClientScript::POS_END); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/jquery-sparkline/jquery.sparkline.min.js', CClientScript::POS_END); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/peity/jquery.peity.min.js', CClientScript::POS_END); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/morris/morris.min.js', CClientScript::POS_END); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/raphael/raphael-min.js', CClientScript::POS_END); 
            // Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/pages/dashboard.js', CClientScript::POS_END); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/app.js', CClientScript::POS_END); 
            Yii::app()->clientScript->registerScript('', '
                $("document").ready(function(){
                    var controller = "'.Yii::app()->controller->id.'/'.Yii::app()->controller->action->id.'";
                    console.log(controller);
                    if(controller=="site/index")
                    {
                        $("#dashboard").css("color", "#1b82ec");
                    }
                    else if(controller=="berita/utama")
                    {
                        $("#input_berita").css("color", "#1b82ec");
                    }
                    else if(controller=="berita/info")
                    {
                        $("#input_sekilas_info").css("color", "#1b82ec");
                    }
                    else if(controller=="meeting/index")
                    {
                        $("#input_pertemuan").css("color", "#1b82ec");
                    }
                    else if(controller=="deklarasi/index")
                    {
                        $("#input_deklarasi").css("color", "#1b82ec");
                    }
                    else if(controller=="ran/inputtarget")
                    {
                        $("#input_target").css("color", "#1b82ec");
                    }
                    else if(controller=="ran/inputrealisasi")
                    {
                        $("#input_realisasi").css("color", "#1b82ec");
                    }
                    else if(controller=="laporan/index")
                    {
                        $("#statistik").css("color", "#1b82ec");
                    }
                    else if("'.Yii::app()->controller->id.'"=="master")
                    {
                        $("#data_master").css("color", "#1b82ec");
                    }
                });
            ');
        ?>
    </body>

</html>