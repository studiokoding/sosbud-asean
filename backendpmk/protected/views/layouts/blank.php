<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<title>MTSM</title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl;?>/public/images/bps.ico">

        <?php
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/morris/morris.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/css/bootstrap.min.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/css/icons.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/css/style.css');
        ?>
    </head>

    <body>

        <!-- Background -->
        <div class="account-pages"></div>

		<!-- page wrapper start -->
        <div class="wrapper-page">
			<?php echo $content; ?>
        </div>
        <!-- page wrapper end -->

        <!-- Footer -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    <p class="text-muted">© 2020. Crafted with <i class="mdi mdi-heart text-danger"></i> by Pengemasan Informasi Statistik</p>
                    </div>
                </div>
            </div>
        </footer>
        <?php
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/jquery.min.js', CClientScript::POS_HEAD); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/bootstrap.bundle.min.js', CClientScript::POS_HEAD); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/jquery.slimscroll.js', CClientScript::POS_HEAD); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/waves.min.js', CClientScript::POS_END); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/jquery-sparkline/jquery.sparkline.min.js', CClientScript::POS_END); 
            Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/js/app.js', CClientScript::POS_END); 
        ?>
    </body>
</html>