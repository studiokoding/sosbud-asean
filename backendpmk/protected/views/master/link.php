<?php
/* @var $this BeritaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Link',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/public/plugins/handsontable/dist/handsontable.full.min.css");
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');
?>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Data Link</h4>
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Data Master Link</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <div class="row">
                            <div class="col-md-2">
                                <button style="float:left;" id="btn-a" type="button" class="btn btn-success waves-effect waves-light">Tambah Master Link</button>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                        <br>
                        <table
                            id = "listmaster"
                            class = "table table-striped table-bordered"
                            style = "border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" >Nama</th>
                                    <th scope="col" >Link</th>
                                    <th scope="col" >Kementrian/Lembaga</th>
                                    <th scope="col" >Tampilkan di Homepage</th>
                                    <th scope="col" >Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($model as $key=>$m): ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td><?= $m->link_nama ?></td>
                                        <td><?= $m->link_href ?></td>
                                        <td>
                                            <?php
                                                $txt_opt = "Kementrian"; 
                                                if($m->link_opt==2)
                                                {
                                                    $txt_opt = "Lembaga";
                                                }
                                                echo $txt_opt;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $txt_show = "Ya";
                                                if($m->link_show==2)
                                                {
                                                    $txt_show = "Tidak";
                                                }
                                                echo $txt_show;
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="button-items">
												<button type="button"  id="v_<?= $m->link_id ?>" class="btn-v btn btn-primary btn-sm waves-effect waves-light"><i class="far fa-eye"></i> Lihat</button>
												<button type="button"  id="u_<?= $m->link_id ?>" class="btn-u btn btn-warning btn-sm waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Update</button>
												<button type="button"  id="d_<?= $m->link_id ?>" class="btn-d btn btn-danger btn-sm waves-effect waves-light"><i class="far fa-trash-alt"></i> Hapus</button>
											</div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade view-master-link" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="link_title" class="modal-title mt-0">Master Link</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Nama Link</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_nama"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Link</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_href"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Kementrian/Lembaga</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_opt"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Tampilkan di Homepage</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_show"></div>
                            </div>
                        </div>

					</div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade form-master-link" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-master-link" method="post" enctype="multipart/form-data">        
                <input type="hidden" id="f_id" name="input_id" value="" />      
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Link</label><br>
                                    <input type="text" id="f_nama" name="input_nama" class="form-control" placeholder="Nama Link"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Href</label><br>
                                    <input type="text" id="f_href" name="input_href" class="form-control" placeholder="Href"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kementrian/Lembaga</label><br>
                                    <div class="p-2">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input input_opt" type="radio" name="input_opt" id="input_opt1" value=1>
                                            <label class="form-check-label" for="input_opt">Kementrian</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input input_opt" type="radio" name="input_opt" id="input_opt2" value=2>
                                            <label class="form-check-label" for="input_opt">Lembaga</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tampilkan Pada Homepage</label><br>
                                    <div class="p-2">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input input_show" type="radio" name="input_show" id="input_show1" value=1>
                                            <label class="form-check-label" for="input_show">Ya</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input input_show" type="radio" name="input_show" id="input_show2" value=2>
                                            <label class="form-check-label" for="input_show">Tidak</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/handsontable/dist/handsontable.full.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
	
    Yii::app()->clientScript->registerScript('initdatatable', '

        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }


        $("#btn-a").on("click", function(){    
            $("#f_id").val("");
            $("#f_title").html("Tambah Master Link");
            $("#f_nama").val(null);
            $("#f_href").val(null);
            $("#f_opt").val(null);
            $("#f_show").val(null);
            $(".form-master-link").modal("show");
		});

        $(".btn-v").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			
            $.ajax({
				url: "'.Yii::app()->createUrl("master/viewlink").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    $("#v_m_nama").html(res.data.link_nama);
                    $("#v_m_href").html(res.data.link_href);
                    
                    var opt_text = "Kementrian";
                    var show_text = "Ya";

                    if(res.data.link_opt==2)
                    {
                        opt_text = "Lembaga";
                    }

                    if(res.data.link_show==2)
                    {
                        show_text = "Tidak";
                    }

                    $("#v_m_opt").html(opt_text);
                    $("#v_m_show").html(show_text);
					$(".view-master-link").modal("show");
				}
            });
        });

        $(".btn-u").on("click", function(){    
            $("#f_id_link").val("");

			var id=$(this).attr("id");
            var res=id.split("_");
			$("#f_title").html("Update Master Link")
			$.ajax({
				url: "'.Yii::app()->createUrl("master/viewlink").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#f_nama").val(null);
                    $("#f_id").val(null);
                    $("#f_href").val(null);
                    $("#f_opt").val(null);
                    $("#f_show").val(null);
                    
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					
					$("#f_id").val(res.data.link_id);
                    $("#f_nama").val(res.data.link_nama);
                    $("#f_href").val(res.data.link_href);
                    
                    if(res.data.link_opt==1)
                    {
                        $("#input_opt1").prop("checked", true);
                    }
                    else
                    {
                        $("#input_opt2").prop("checked", true);
                    }
                    
                    if(res.data.link_show==1)
                    {
                        $("#input_show1").prop("checked", true);
                    }
                    else
                    {
                        $("#input_show2").prop("checked", true);
                    }
					
                    $(".form-master-link").modal("show");
				}
			});
			
        });

        $("#form-master-link").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
            console.log(formdata);
    
            $.ajax({
                url: "'.Yii::app()->createUrl("master/createlink").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type,
                        showCancelButton: false,
                        confirmButtonClass: "btn btn-success",
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                        $(".form-link").modal("hide");
                        location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

        $(".btn-d").on("click", function(){
            var id=$(this).attr("id");
            var res=id.split("_");

            swal({
                title: "Yakin menghapus Master Link ini?",
                text: "Master Link yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("master/deletelink").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })
        });

		$(document).ready(function() {
			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listberita\').DataTable();
			
		} );
	');
?>