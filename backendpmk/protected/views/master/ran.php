<?php
/* @var $this BeritaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Rencana Aksi Nasional',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/public/plugins/handsontable/dist/handsontable.full.min.css");
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');
?>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Data Master Rencana Aksi Nasional</h4>
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Data Master Rencana Aksi Nasional</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <div class="row">
                            <div class="col-md-2">
                                <button style="float:left;" id="btn-a" type="button" class="btn btn-success waves-effect waves-light">Tambah Master Rencana Aksi</button>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                        <br>
                        <table
                            id = "listmaster"
                            class = "table table-striped table-bordered"
                            style = "border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" style="width: 30%;">Rencana Aksi</th>
                                    <th scope="col" style="width: 30%;">Indikator</th>
                                    <th scope="col">Badan Sektoral I</th>
                                    <th scope="col">Badan Sektoral</th>
                                    <th scope="col">KL</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($model as $key=>$m): ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td><?= $m->iku ?></td>
                                        <td><?= $m->kegiatan ?></td>
                                        <td><?= $m->pjSektoralsub->sektoralsub_nama ?> (<b><?= $m->pjSektoralsub->sektoralsub_singkatan ?></b>)</td>
                                        <td><?= $m->pjSektoral->sektoral_nama ?> (<b><?= $m->pjSektoral->sektoral_singkatan ?></b>)</td>
                                        <td><?php
                                         if($m->pjKl){
                                             echo $m->pjKl->kl_nama;
                                         } else {
                                             echo "-";
                                         }
                                        ?></td>
                                        <td>
                                            <div class="button-items">
												<button type="button"  id="v_<?= $m->id ?>" class="btn-v btn btn-primary btn-sm btn-block waves-effect waves-light"><i class="far fa-eye"></i> Lihat</button>
												<button type="button"  id="u_<?= $m->id ?>" class="btn-u btn btn-warning btn-sm btn-block waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Update</button>
												<button type="button"  id="d_<?= $m->id ?>" class="btn-d btn btn-danger btn-sm btn-block waves-effect waves-light"><i class="far fa-trash-alt"></i> Hapus</button>
											</div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade view-master-ran" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="deklarasi_title" class="modal-title mt-0">Master RAN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Rencana Aksi</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_iku"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Indikator</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_kegiatan"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>PJ Badan Sektoral I</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_sektoralsub"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>PJ Badan Sektoral</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_sektoral"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>PJ KL</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_kl"></div>
                            </div>
                        </div>
					</div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade form-master-ran" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-master-ran" method="post" enctype="multipart/form-data">        
                <input type="hidden" id="f_id" name="f_id" value="" />      
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Rencana Aksi</label><br>
                                    <textarea class="text_wysiwyg" id="f_iku" name="input_iku" class="form-control" rows="4" placeholder="IKU"></textarea>
                                </div>
                            </div>
														
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Indikator</label><br>
                                    <textarea class="text_wysiwyg" id="f_kegiatan" name="input_kegiatan" class="form-control" rows="4" placeholder="Kegiatan"></textarea>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Badan Sektoral I</label>  
                                    <div class="col-sm-9">    
                                        <select name="input_sektoralsub" id="f_sektoralsub" class="form-control select2">`
                                        <option>:: Pilih Badan Sektoral I ::</option>
                                        <?php
                                            $opt="";
                                            foreach($sektoralSub as $r){
                                                $opt.="<option value='".$r->sektoralsub_id."'>".$r->sektoralsub_singkatan."(".$r->sektoralsub_nama.")</option>";
                                            }
                                            echo $opt;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/handsontable/dist/handsontable.full.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
	
    Yii::app()->clientScript->registerScript('initdatatable', '

        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }


        $("#btn-a").on("click", function(){    
            $("#f_id").val("");
            $("#f_title").html("Tambah Master Rencana Aksi");
            $("#f_iku").summernote("code", "");
			$("#f_kegiatan").summernote("code", "");
            $(".form-master-ran").modal("show");
            $("#f_sektoralsub").val(null);
		});

        $(".btn-v").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			
            $.ajax({
				url: "'.Yii::app()->createUrl("master/viewran").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    $("#v_m_iku").html(res["data"]["iku"]);
                    $("#v_m_kegiatan").html(res["data"]["kegiatan"]);
                    $("#v_m_sektoralsub").html(res["data"]["pj_sektoralsub"]);
                    $("#v_m_sektoral").html(res["data"]["pj_sektoral"]);
                    $("#v_m_kl").html(res["data"]["pj_kl"]);
					$(".view-master-ran").modal("show");
				}
            });
        });

        $(".btn-u").on("click", function(){    
            $("#f_id").val("");

			var id=$(this).attr("id");
            var res=id.split("_");
			$("#f_title").html("Update Master Rencana Aksi")
			$.ajax({
				url: "'.Yii::app()->createUrl("master/viewran").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#f_iku").summernote("code", "");
					$("#f_kegiatan").summernote("code", "");
                    
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					
					$("#f_id").val(res.data.id);

					$("#f_iku").summernote("pasteHTML", res["data"]["iku"]);

                    $("#f_kegiatan").summernote("code", res["data"]["kegiatan"]);

                    $("#f_sektoralsub").val(res["data"]["id_sektoralsub"]);
					
                    $(".form-master-ran").modal("show");
				}
			});
			
        });

        $("#form-master-ran").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
            console.log(formdata);
    
            $.ajax({
                url: "'.Yii::app()->createUrl("master/createran").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type,
                        showCancelButton: false,
                        confirmButtonClass: "btn btn-success",
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                        $(".form-deklarasi").modal("hide");
                        location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

        $(".btn-d").on("click", function(){
            var id=$(this).attr("id");
            var res=id.split("_");

            swal({
                title: "Yakin menghapus Master Rencana Aksi Nasional ini?",
                text: "Master Rencana Aksi Nasional yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("master/deleteran").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.msg;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })
        });

		$(document).ready(function() {
			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listberita\').DataTable();
			
		} );
	');
?>