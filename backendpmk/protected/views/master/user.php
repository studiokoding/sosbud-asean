 <?php
/* @var $this BeritaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master User',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/public/plugins/handsontable/dist/handsontable.full.min.css");
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');
?>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Data User</h4>
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Data Master User</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <div class="row">
                            <div class="col-md-2">
                                <button style="float:left;" id="btn-a" type="button" class="btn btn-success waves-effect waves-light">Tambah User</button>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                        <br>
                        <table
                            id = "listmaster"
                            class = "table table-striped table-bordered"
                            style = "border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" >Username</th>
                                    <th scope="col" >Email</th>
                                    <th scope="col" >Badan Sektoral I</th>
                                    <th scope="col" >Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($model as $key=>$m): ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td><?= $m->username ?></td>
                                        <td><?= $m->email ?></td>
                                        <td><?= $m->userSektoralsub->sektoralsub_nama." (".$m->userSektoralsub->sektoralsub_singkatan.")" ?></td>
                                        <td class="text-center">
                                            <div class="button-items">
												<button type="button"  id="v_<?= $m->id ?>" class="btn-v btn btn-primary btn-sm waves-effect waves-light"><i class="far fa-eye"></i> Lihat</button>
												<button type="button"  id="u_<?= $m->id ?>" class="btn-u btn btn-warning btn-sm waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Update</button>
                                                <button type="button"  id="p_<?= $m->id ?>" class="btn-p btn btn-info btn-sm waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Ubah Password</button>
												<button type="button"  id="d_<?= $m->id ?>" class="btn-d btn btn-danger btn-sm waves-effect waves-light"><i class="far fa-trash-alt"></i> Hapus</button>
											</div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade view-master-user" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="user_title" class="modal-title mt-0">Master User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Username</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_username"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Email</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_email"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Status</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_status"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Badan Sektoral I</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_sektoralsub"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Level</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_level"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Created Date</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_created"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Updated Date</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_updated"></div>
                            </div>
                        </div>

					</div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade form-master-user" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-master-user" method="post" enctype="multipart/form-data">        
                <input type="hidden" id="f_id" name="input_id" value="" />      
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Username</label><br>
                                    <input type="text" id="f_username" name="input_username" class="form-control" placeholder="Username"/>
                                </div>
                            </div>
                        </div>
                        <div class="row pass">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password</label><br>
                                    <input type="password" id="f_password" name="input_password" class="form-control" placeholder="Password"/>
                                </div>
                            </div>
                        </div>
                        <div class="row pass">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Repeat Password</label><br>
                                    <input type="password" id="f_repeat_password" name="input_repeat_password" class="form-control" placeholder="Ulangi Password"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label><br>
                                    <input type="email" id="f_email" name="input_email" class="form-control" placeholder="Email"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Badan Sektoral I</label><br>
                                    <select name="input_sektoralsub" id="f_sektoralsub" class="form-control select2">`
                                    <option>:: Pilih Badan Sektoral I ::</option>
                                    <?php
                                        $opt="";
                                        foreach($modelSektoralsub as $r){
                                            $opt.="<option value='".$r->sektoralsub_id."'>".$r->sektoralsub_nama."</option>";
                                        }
                                        echo $opt;
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>User Level</label><br>
                                    <select name="input_level" id="f_level" class="form-control select2">`
                                    <option>:: Pilih User Level ::</option>
                                    <?php
                                        $opt="";
                                        foreach($modelUserLevel as $r){
                                            $opt.="<option value='".$r->id."'>".$r->userlevel."</option>";
                                        }
                                        echo $opt;
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>

<div class="modal fade form-master-password" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-master-password" method="post" enctype="multipart/form-data">        
                <input type="hidden" id="f_id_pass" name="input_id" value="" />      
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password Lama</label><br>
                                    <input type="password" id="f_old_pass" name="input_old_pass" class="form-control" placeholder="Password Lama"/>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password Baru</label><br>
                                    <input type="password" id="f_new_pass" name="input_new_pass" class="form-control" placeholder="Password Baru"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Ulangi Password Baru</label><br>
                                    <input type="password" id="f_repeat_new_pass" name="input_repeat_new_pass" class="form-control" placeholder="Ulangi Password Baru"/>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/handsontable/dist/handsontable.full.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
	
    Yii::app()->clientScript->registerScript('initdatatable', '

        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }


        $("#btn-a").on("click", function(){    
            $("#f_id").val("");
            $("#f_title").html("Tambah Master User");
            $("#f_username").val(null);
            $("#f_password").val(null);
            $("#f_repeat_password").val(null);
            $("#f_email").val(null);
            $("#f_sektoralsub").val(null);
            $("#f_level").val(null);
            $(".pass").show();
            $(".form-master-user").modal("show");
		});

        $(".btn-v").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			
            $.ajax({
				url: "'.Yii::app()->createUrl("master/viewuser").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    $("#v_m_username").html(res.data.username);
                    $("#v_m_email").html(res.data.email);
                    $("#v_m_status").html(res.data.status);
                    $("#v_m_sektoralsub").html(res.data.sektoralsub_text);
                    $("#v_m_level").html(res.data.level_text);
                    $("#v_m_created").html(res.data.created_at_text);
                    $("#v_m_updated").html(res.data.updated_at_text);
					$(".view-master-user").modal("show");
				}
            });
        });

        $(".btn-u").on("click", function(){    
            $("#f_id_link").val("");
            $(".pass").hide();
			var id=$(this).attr("id");
            var res=id.split("_");
			$("#f_title").html("Update Master User")
			$.ajax({
				url: "'.Yii::app()->createUrl("master/viewuser").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#f_username").val(null);
                    $("#f_password").val(null);
                    $("#f_repeat_password").val(null);
                    $("#f_email").val(null);
                    $("#f_sektoralsub").val(null);
                    $("#f_level").val(null);
                    
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					$("#f_id").val(res.data.id);
					$("#f_username").val(res.data.username);
                    $("#f_email").val(res.data.email);
                    $("#f_sektoralsub").val(res.data.user_sektoralsub_id);
                    $("#f_level").val(res.data.level);
                    
                    $(".form-master-user").modal("show");
				}
			});
			
        });

        $(".btn-p").on("click", function(){    
            $("#f_id_link").val("");
            $(".pass").hide();
			var id=$(this).attr("id");
            var res=id.split("_");
			$("#f_title").html("Update Master User")
			$.ajax({
				url: "'.Yii::app()->createUrl("master/viewuser").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					$("#f_id_pass").val(res.data.id);
                    $("#f_old_pass").val(null);
                    
                    $(".form-master-password").modal("show");
				}
			});
			
        });

        $("#form-master-password").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
            console.log(formdata);
    
            $.ajax({
                url: "'.Yii::app()->createUrl("master/changepassword").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type,
                        showCancelButton: false,
                        confirmButtonClass: "btn btn-success",
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                        $(".form-link").modal("hide");
                        location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

        $("#form-master-user").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
            console.log(formdata);
    
            $.ajax({
                url: "'.Yii::app()->createUrl("master/createuser").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type,
                        showCancelButton: false,
                        confirmButtonClass: "btn btn-success",
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                        $(".form-link").modal("hide");
                        location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

        $(".btn-d").on("click", function(){
            var id=$(this).attr("id");
            var res=id.split("_");

            swal({
                title: "Yakin menghapus Master User ini?",
                text: "Master User yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("master/deleteuser").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })
        });

		$(document).ready(function() {
			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listberita\').DataTable();
			
		} );
	');
?>