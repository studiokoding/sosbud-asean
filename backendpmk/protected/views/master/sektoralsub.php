<?php
/* @var $this BeritaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Badan Sektoral I',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . "/public/plugins/handsontable/dist/handsontable.full.min.css");
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');
?>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Data Badan Sektoral I</h4>
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Data Master Badan Sektoral I</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <div class="row">
                            <div class="col-md-2">
                                <button style="float:left;" id="btn-a" type="button" class="btn btn-success waves-effect waves-light">Tambah Master Badan Sektoral I</button>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                        <br>
                        <table
                            id = "listmaster"
                            class = "table table-striped table-bordered"
                            style = "border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" >Nama</th>
                                    <th scope="col" >Singkatan</th>
                                    <th scope="col" >Badan Sektoral</th>
                                    <th scope="col" >Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($model as $key=>$m): ?>                                    
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td><?= $m->sektoralsub_nama ?></td>
                                        <td><?= $m->sektoralsub_singkatan ?></td>
                                        <td><?= $m->sektoralsubSektoral["sektoral_nama"] ?></td>
                                        <td class="text-center">
                                            <div class="button-items">
												<button type="button"  id="v_<?= $m->sektoralsub_id ?>" class="btn-v btn btn-primary btn-sm waves-effect waves-light"><i class="far fa-eye"></i> Lihat</button>
												<button type="button"  id="u_<?= $m->sektoralsub_id ?>" class="btn-u btn btn-warning btn-sm waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Update</button>
												<button type="button"  id="d_<?= $m->sektoralsub_id ?>" class="btn-d btn btn-danger btn-sm waves-effect waves-light"><i class="far fa-trash-alt"></i> Hapus</button>
											</div>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade view-master-sektoralsub" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="badan_sektoralsub_title" class="modal-title mt-0">Master Badan Sektoral I</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="col-md-12">

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Nama Badan Sektoral I</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_nama"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Singkatan</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_singkatan"></div>
                            </div>
                        </div>

                        <div class="row" style="padding:5px">
						    <div class="col-md-3">
                                <b>Badan Sektoral</b>
                            </div>
                            <div class="col-md-9">
                                <div id="v_m_sektoral"></div>
                            </div>
                        </div>

					</div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade form-master-sektoralsub" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-master-sektoralsub" method="post" enctype="multipart/form-data">        
                <input type="hidden" id="f_id_sektoralsub" name="input_id_sektoralsub" value="" />      
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama Badan Sektoral I</label><br>
                                    <input type="text" id="f_nama" name="input_nama" class="form-control" placeholder="Nama Badan Sektoral"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Singkatan Badan Sektoral</label><br>
                                    <input type="text" id="f_singkatan" name="input_singkatan" class="form-control" placeholder="Singkatan Badan Sektoral"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Badan Sektoral</label><br>   
                                    <select name="input_sektoral" id="f_sektoral" class="form-control select2">`
                                    <option>:: Pilih Badan Sektoral ::</option>
                                    <?php
                                        $opt="";
                                        foreach($modelSektoral as $r){
                                            $opt.="<option value='".$r->sektoral_id."'>".$r->sektoral_nama."</option>";
                                        }
                                        echo $opt;
                                    ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/handsontable/dist/handsontable.full.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
	
    Yii::app()->clientScript->registerScript('initdatatable', '

        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }


        $("#btn-a").on("click", function(){    
            $("#f_id").val("");
            $("#f_title").html("Tambah Master Badan Sektoral I");
            $("#f_nama").val(null);
            $("#f_id_sektoralsub").val(null);
            $("#f_singkatan").val(null);
            $("#f_sektoral").val(null);
            $(".form-master-sektoralsub").modal("show");
		});

        $(".btn-v").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			
            $.ajax({
				url: "'.Yii::app()->createUrl("master/viewsektoralsub").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    $("#badan_sektoralsub_title").html(res.data.sektoralsub_singkatan);
                    $("#v_m_nama").html(res.data.sektoralsub_nama);
                    $("#v_m_singkatan").html(res.data.sektoralsub_singkatan);
                    $("#v_m_sektoral").html(res.data.sektoral_nama);
					$(".view-master-sektoralsub").modal("show");
				}
            });
        });

        $(".btn-u").on("click", function(){    
			var id=$(this).attr("id");
            var res=id.split("_");

			$("#f_title").html("Update Master Badan Sektoral")
			$.ajax({
				url: "'.Yii::app()->createUrl("master/viewsektoralsub").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){                    
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					
					$("#f_id_sektoralsub").val(res.data.sektoralsub_id);

                    $("#f_nama").val(res.data.sektoralsub_nama);

					$("#f_singkatan").val(res.data.sektoralsub_nama);

                    $("#f_sektoral").val(res.data.sektoralsub_sektoral_id);
					
                    $(".form-master-sektoralsub").modal("show");
				}
			});
			
        });

        $("#form-master-sektoralsub").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
            console.log(formdata);
    
            $.ajax({
                url: "'.Yii::app()->createUrl("master/createsektoralsub").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type,
                        showCancelButton: false,
                        confirmButtonClass: "btn btn-success",
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                        $(".form-sektoralsub").modal("hide");
                        location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

        $(".btn-d").on("click", function(){
            var id=$(this).attr("id");
            var res=id.split("_");

            swal({
                title: "Yakin menghapus Master Badan Sektoral I ini?",
                text: "Master Badan Sektoral I yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("master/deletesektoralsub").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })
        });

		$(document).ready(function() {
			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listberita\').DataTable();
			
		} );
	');
?>