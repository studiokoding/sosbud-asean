<?php
/* @var $this BeritaController */
/* @var $model Berita */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'berita_id'); ?>
		<?php echo $form->textField($model,'berita_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'berita_judul'); ?>
		<?php echo $form->textArea($model,'berita_judul',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'berita_foto'); ?>
		<?php echo $form->textField($model,'berita_foto',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'berita_overview'); ?>
		<?php echo $form->textArea($model,'berita_overview',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'berita_isi'); ?>
		<?php echo $form->textArea($model,'berita_isi',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'berita_user_id'); ?>
		<?php echo $form->textField($model,'berita_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'berita_adddate'); ?>
		<?php echo $form->textField($model,'berita_adddate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->