<?php
/* @var $this Sekilas InfoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sekilas Infos',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/public/plugins/handsontable/dist/handsontable.full.min.css");
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');
?>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- <h4 class="page-title">Sekilas Info</h4> -->
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Daftar Sekilas Info</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <div class="row">
                            <div class="col-md-2">
                                <button style="float:left;" id="btn-a" type="button" class="btn btn-success waves-effect waves-light">Tambah Sekilas Info</button>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                        <br>
                        <table
                            id="listinfo"
                            class="table table-striped table-bordered"
                            style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" style="width: 70%;">Info</th>
                                    <th scope="col" style="width: 10%;">Status</th>
                                    <th scope="col" style="width: 10%;">Metadata</th>
                                    <th scope="col" style="width: 8%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
										$tr="";
										$user="";
										
										foreach($dataProvider->data as $k=>$row){  
											$u=User::model()->findByPk($row['info_user_id']);
											if($u){
												$user=$u->username;
                                            }
                                            
                                            $infoshow="Tidak Tampil";
                                            if($row['info_show']==1){
                                                $infoshow="Tampil";
                                            }

											$button='<div class="button-items">
												<button type="button"  id="v_'.$row['info_id'].'" class="btn-v btn btn-primary btn-sm btn-block waves-effect waves-light"><i class="far fa-eye"></i> Lihat</button>
												<button type="button"  id="u_'.$row['info_id'].'" class="btn-u btn btn-warning btn-sm btn-block waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Update</button>
												<button type="button"  id="d_'.$row['info_id'].'" class="btn-d btn btn-danger btn-sm btn-block waves-effect waves-light"><i class="far fa-trash-alt"></i> Hapus</button>
											</div>';

											$tr.='<tr id="'.$row['info_id'].'">
												<td style="width: 2%;">'.($k+1).'</td>
												<td id="judul_'.$row['info_id'].'" class="judulinfo" style="width: 70%;word-break: break-all;">'.strip_tags($row['info_text']).'</td>
												<td style="width: 10%;">'.$infoshow.'</td>
												<td  style="width: 10%;"><span class="badge badge-light">'.$user.'</span><br><span class="badge badge-light">'.$row['info_adddate'].'</span></td>
												<td  style="width: 8%;">'.$button.'</td>
											</tr>';
										}
										echo $tr;
									?>
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade view-info" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="v_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="col-md-12">
						<div id="v_info"></div>
                        <hr>
						<div>Status : <span id="v_status"></span></div>
					</div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade form-info" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-info" method="post" enctype="multipart/form-data">                        
                <div class="modal-body">
                        <input type="hidden" id="f_id" name="input_id" value="">
                        <input type="hidden" id="f_user" name="input_user" value="">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Info : </label><br>
                                    <textarea class="text_wysiwyg" id="f_text" name="input_text" class="form-control" rows="4" placeholder="Info"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Status Info : </label>
                                        <div class="col-sm-10">
                                            <select name="input_status" id="f_status" class="form-control select2">
                                                <option>:: Pilih Status Info ::</option>
                                                <option value="0">Tidak Tampil</option>
                                                <option value="1">Tampil</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/handsontable/dist/handsontable.full.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
	
	Yii::app()->clientScript->registerScript('initdatatable', '

		$(".btn-v").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            
            $.ajax({
				url: "'.Yii::app()->createUrl("berita/vinfo").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#loading").css("display", "block");
                    $("#v_info").html("");
                    $("#v_title").html("");
					$("#v_status").html("");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    $("#v_title").html("Info #"+res.data.id);
                    $("#v_info").html(res.data.text);
                    var status = "Tampil";
                    if(res.data.status==0)
                    {
                        status = "Tidak Tampil";
                    }
					$("#v_status").html(status);
					$(".view-info").modal("show");
				}
            });
            
			$("#v_judul").html(t);
		});

		$(".btn-u").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
			
			$.ajax({
				url: "'.Yii::app()->createUrl("berita/vinfo").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#f_id").val("");
					$("#f_status").val("");
					$("#f_text").summernote("code", "");
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					//console.log(res);
					$("#f_id").val(res.data.id);
					$("#f_text").summernote("pasteHTML", res.data.text);
					$("#f_status").val(res.data.status);
                    $(".form-info").modal("show");
				}
			});
        });
        
        $("#form-info").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
    
            $.ajax({
                url: "'.Yii::app()->createUrl("berita/createinfo").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                       $(".form-info").modal("hide");
                       location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

        $(".btn-d").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            
            swal({
                title: "Yakin menghapus info ini?",
                text: "info yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("berita/dinfo").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })

			
        });

        $("#btn-a").on("click", function(){    
            $("#f_id").val("");
            $("#f_status").val("");
            $("#f_text").summernote("code", "");
            $(".form-info").modal("show");
		});

		$(document).ready(function() {
			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listinfo\').DataTable();
			
		} );
	');
?>