<?php
/* @var $this BeritaController */
/* @var $model Berita */

$this->breadcrumbs=array(
	'Beritas'=>array('index'),
	$model->berita_id,
);

$this->menu=array(
	array('label'=>'List Berita', 'url'=>array('index')),
	array('label'=>'Create Berita', 'url'=>array('create')),
	array('label'=>'Update Berita', 'url'=>array('update', 'id'=>$model->berita_id)),
	array('label'=>'Delete Berita', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->berita_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Berita', 'url'=>array('admin')),
);
?>

<h1>View Berita #<?php echo $model->berita_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'berita_id',
		'berita_judul',
		'berita_foto',
		'berita_overview',
		'berita_isi',
		'berita_user_id',
		'berita_adddate',
	),
)); ?>
