<?php
/* @var $this BeritaController */
/* @var $data Berita */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('berita_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->berita_id), array('view', 'id'=>$data->berita_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berita_judul')); ?>:</b>
	<?php echo CHtml::encode($data->berita_judul); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berita_foto')); ?>:</b>
	<?php echo CHtml::encode($data->berita_foto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berita_overview')); ?>:</b>
	<?php echo CHtml::encode($data->berita_overview); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berita_isi')); ?>:</b>
	<?php echo CHtml::encode($data->berita_isi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berita_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->berita_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('berita_adddate')); ?>:</b>
	<?php echo CHtml::encode($data->berita_adddate); ?>
	<br />


</div>