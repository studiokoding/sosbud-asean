<?php
/* @var $this BeritaController */
/* @var $model Berita */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'berita-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'berita_judul'); ?>
		<?php echo $form->textArea($model,'berita_judul',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'berita_judul'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'berita_foto'); ?>
		<?php echo $form->textField($model,'berita_foto',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'berita_foto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'berita_overview'); ?>
		<?php echo $form->textArea($model,'berita_overview',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'berita_overview'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'berita_isi'); ?>
		<?php echo $form->textArea($model,'berita_isi',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'berita_isi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'berita_user_id'); ?>
		<?php echo $form->textField($model,'berita_user_id'); ?>
		<?php echo $form->error($model,'berita_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'berita_adddate'); ?>
		<?php echo $form->textField($model,'berita_adddate'); ?>
		<?php echo $form->error($model,'berita_adddate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->