<?php
/* @var $this RanController */
/* @var $model MKegiatan */

$this->breadcrumbs=array(
	'Mkegiatans'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MKegiatan', 'url'=>array('index')),
	array('label'=>'Create MKegiatan', 'url'=>array('create')),
	array('label'=>'Update MKegiatan', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MKegiatan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MKegiatan', 'url'=>array('admin')),
);
?>

<h1>View MKegiatan #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'iku',
		'kegiatan',
		'pj_sektoralsub',
		'pj_sektoral',
		'pj_kl',
	),
)); ?>
