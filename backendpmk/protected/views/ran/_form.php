<?php
/* @var $this RanController */
/* @var $model MKegiatan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mkegiatan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'iku'); ?>
		<?php echo $form->textArea($model,'iku',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'iku'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kegiatan'); ?>
		<?php echo $form->textArea($model,'kegiatan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'kegiatan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pj_sektoralsub'); ?>
		<?php echo $form->textField($model,'pj_sektoralsub'); ?>
		<?php echo $form->error($model,'pj_sektoralsub'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pj_sektoral'); ?>
		<?php echo $form->textField($model,'pj_sektoral'); ?>
		<?php echo $form->error($model,'pj_sektoral'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pj_kl'); ?>
		<?php echo $form->textField($model,'pj_kl'); ?>
		<?php echo $form->error($model,'pj_kl'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->