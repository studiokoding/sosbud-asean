<?php
/* @var $this RanController */
/* @var $data MKegiatan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iku')); ?>:</b>
	<?php echo CHtml::encode($data->iku); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kegiatan')); ?>:</b>
	<?php echo CHtml::encode($data->kegiatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pj_sektoralsub')); ?>:</b>
	<?php echo CHtml::encode($data->pj_sektoralsub); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pj_sektoral')); ?>:</b>
	<?php echo CHtml::encode($data->pj_sektoral); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pj_kl')); ?>:</b>
	<?php echo CHtml::encode($data->pj_kl); ?>
	<br />


</div>