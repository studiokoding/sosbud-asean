<?php
/* @var $this RanController */
/* @var $model MKegiatan */

$this->breadcrumbs=array(
	'Mkegiatans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MKegiatan', 'url'=>array('index')),
	array('label'=>'Manage MKegiatan', 'url'=>array('admin')),
);
?>

<h1>Create MKegiatan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>