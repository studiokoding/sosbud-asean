<?php
/* @var $this BeritaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Beritas',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/public/plugins/handsontable/dist/handsontable.full.min.css");
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');
?>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Rencana Aksi Nasional</h4>
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Daftar Rencana Aksi Nasional</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <div class="row">
                            <div class="col-md-2">
                                <button style="float:left;" id="btn-a" type="button" class="btn btn-success waves-effect waves-light">Tambah Rencana Aksi</button>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                        <br>
                        <table
                            id="listberita"
                            class="table table-striped table-bordered"
                            style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" style="width: 40%;">IKU</th>
                                    <th scope="col" style="width: 40%;">Kegiatan</th>
                                    <th scope="col" style="width: 10%;">Penanggung Jawab</th>
                                    <th scope="col" style="width: 8%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
										$tr="";
										$sektoralsub="";
										$kl="";
										
										foreach($dataProvider->data as $k=>$row){  
											$ss=Sektoralsub::model()->findByPk($row['pj_sektoralsub']);
											if($ss){
												$sektoralsub=$ss->sektoralsub_singkatan;
												$kl=Kl::model()->findByPk(Sektoral::model()->findByPk($ss->sektoralsub_sektoral_id)->sektoral_kl_id)->kl_nama;
											}

											$button='<div class="button-items">
												<button type="button"  id="s_'.$row['id'].'" class="btn-v btn btn-secondary btn-sm btn-block waves-effect waves-light"><i class="fas fa-briefcase"></i> Input Realisasi</button>
												<button type="button"  id="v_'.$row['id'].'" class="btn-v btn btn-primary btn-sm btn-block waves-effect waves-light"><i class="far fa-eye"></i> Lihat</button>
												<button type="button"  id="u_'.$row['id'].'" class="btn-u btn btn-warning btn-sm btn-block waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Update</button>
												<button type="button"  id="d_'.$row['id'].'" class="btn-d btn btn-danger btn-sm btn-block waves-effect waves-light"><i class="far fa-trash-alt"></i> Hapus</button>
											</div>';

											$tr.='<tr id="'.$row['id'].'">
												<td style="width: 2%;">'.($k+1).'</td>
												<td id="iku_'.$row['id'].'" style="width: 40%;word-break: break-all;">'.$row['iku'].'</td>
												<td id="kg_'.$row['id'].'" style="width: 40%;word-break: break-all;">'.$row['kegiatan'].'</td>
												<td id="pj_'.$row['id'].'" style="width: 10%;word-break: break-all;">'.$sektoralsub.'<br>'.$kl.'</td>
												<td  style="width: 8%;">'.$button.'</td>
											</tr>';
										}
										echo $tr;
									?>
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade view-ran" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="v_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="col-md-12">
						<h4 id="v_judul" class="mt-0 header-title"></h4>
                        <hr>
						<div id="v_isi"></div>
                        <hr>
						<div id="v_overview"></div>
					</div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade form-ran" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-ran" method="post" enctype="multipart/form-data">                        
                <div class="modal-body">
                        <input type="hidden" id="f_id" name="input_id" value="">
                        <input type="hidden" id="f_user" name="input_user" value="">
                        
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>IKU</label><br>
                                    <textarea class="text_wysiwyg" id="f_iku" name="input_iku" class="form-control" rows="4" placeholder="IKU"></textarea>
                                </div>
                            </div>
														
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Kegiatan</label><br>
                                    <textarea class="text_wysiwyg" id="f_kegiatan" name="input_kegiatan" class="form-control" rows="4" placeholder="Kegiatan"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SektoralSub</label>  
                                    <div class="col-sm-8">    
                                        <select name="input_summit" id="f_summit" class="form-control select2">`
                                        <option>:: Pilih SektoralSub ::</option>
                                        <?php
                                            $opt="";
                                            foreach($sektoralSub as $r){
                                                $opt.="<option value='".$r->sektoralsub_id."'>".$r->sektoralsub_nama."</option>";
                                            }
                                            echo $opt;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div<
                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/handsontable/dist/handsontable.full.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
	
	Yii::app()->clientScript->registerScript('initdatatable', '

		$(".btn-v").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            
            $.ajax({
				url: "'.Yii::app()->createUrl("berita/vutama").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#loading").css("display", "block");
                    $("#v_judul").html("");
					$("#v_isi").html("");
					$("#v_overview").html("");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    $("#v_title").html("Berita #"+res.data.id);
					$("#v_judul").html(res.data.judul);
					$("#v_isi").html(res.data.isi);
					$("#v_overview").html(res.data.overview);
					$(".view-ran").modal("show");
				}
            });
            
			$("#v_judul").html(t);
		});

		$(".btn-u").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
			
			$.ajax({
				url: "'.Yii::app()->createUrl("berita/vutama").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#f_id").val("");
					$("#f_user").val("");
					$("#f_judul").val("");
					$("#f_isi").summernote("code", "");
					$("#f_overview").summernote("code", "");
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					//console.log(res);
					$("#f_id").val(res.data.id);
					$("#f_user").val("'.Yii::app()->user->id.'");
					$("#f_judul").val(res.data.judul);
					$("#f_isi").summernote("pasteHTML", res.data.isi);
					$("#f_overview").summernote("pasteHTML", "res.data.overview");
                    $(".form-ran").modal("show");
                    
                    
				}
			});
        });
        
        $("#form-ran").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
            console.log(formdata);
    
            $.ajax({
                url: "'.Yii::app()->createUrl("berita/create").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type,
                        showCancelButton: false,
                        confirmButtonClass: "btn btn-success",
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                       $(".form-ran").modal("hide");
                       location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

        $(".btn-d").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            
            swal({
                title: "Yakin menghapus berita ini?",
                text: "Berita yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("berita/dutama").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })

			
        });

        $("#btn-a").on("click", function(){    
            $(".form-ran").modal("show");
		});

		$(document).ready(function() {
			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listberita\').DataTable();
			
		} );
	');
?>