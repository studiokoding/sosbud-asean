<?php
/* @var $this BeritaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Rencana Aksi Nasional',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/public/plugins/handsontable/dist/handsontable.full.min.css");
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');
?>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
               <!--  <h4 class="page-title">Data Master Rencana Aksi Nasional</h4> -->
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Data Master Rencana Aksi Nasional</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <br>
                        <table
                            id = "listmaster"
                            class = "table table-stripde table-bordered"
                            style = "border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Rencana Aksi</th>
                                    <th scope="col">Indikator</th>
                                    <th scope="col"  style="width: 30%;">Target</th>
                                    <th scope="col">KL</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($modelKegiatan as $key=>$m): ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td><?= $m->iku ?></td>
                                        <td id="row_kegiatan_<?= $m->id ?>"><?= $m->kegiatan ?></td>
                                        <td class="text-center">
                                            <?php if(count($m->ranTargets)==0): ?>
                                                <span class="badge badge-warning">Belum Input Target</span>
                                            <?php endif ?>
                                            <table class="table table-borderless">
                                                <?php foreach($m->ranTargets as $t): ?>
                                                    <?php if(count($m->ranTargets)!=1): ?>
                                                        <tr style="border-bottom: 1px solid #dee2e6;">
                                                    <?php else: ?>
                                                        <tr>
                                                    <?php endif ?>
                                                        <td><b>Tahun <?= $t->tahun ?></b></td>
                                                        <td>:</td>
                                                        <td><?= $t->target ?> <?= $t->satuan ?></td>
                                                        <td>
                                                            <?php if(($sektoralsub==$m->pj_sektoralsub && $sektoral==$m->pj_sektoral) || Yii::app()->user->level==10): ?> 
                                                                <button type="button" id="u_<?= $t->id ?>" class="btn-u btn btn-primary btn-sm btn-block waves-effect waves-light">Update</button>
                                                                <button type="button" id="d_<?= $t->id ?>" class="btn-d btn btn-danger btn-sm btn-block waves-effect waves-light">Hapus</button>
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </table>
                                        </td>
                                        <td><?php
                                         if($m->pjKl){
                                             echo $m->pjKl->kl_nama;
                                         } else {
                                             echo "-";
                                         }
                                        ?></td>
                                        <td>
                                            <?php if(($sektoralsub==$m->pj_sektoralsub && $sektoral==$m->pj_sektoral) || Yii::app()->user->level==10): ?> 
                                                <div class="button-items">
    												<button type="button"  id="<?= $m->id ?>" class="btn-input-target btn btn-success btn-sm btn-block waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Input Target</button>
    											</div>
                                            <?php else: ?>
                                                <span class="badge badge-info">Hanya Bisa Diinput Badan Sektoral Terkait</span>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade form-input-target" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form id="form-input-target" method="post" enctype="multipart/form-data">
                <input type="hidden" id="f_id_kegiatan" name="input_id_kegiatan" />
                <input type="hidden" id="f_id_target" name="input_id_target" />
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <b>Indikator</b>
                        </div>
                        <div class="col-md-9">
                            <div id="m_kegiatan"></div>
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-3">
                            <b>Tahun</b>
                        </div>
                        <div class="col-md-9">
                            <select name="input_tahun" id="f_tahun" class="form-control select2">`
                                <option>:: Pilih Tahun ::</option>
                                <?php
                                    $opt="";
                                    $year = date("Y");
                                    for($i=$year;$i<($year+5);$i++){
                                        $opt.="<option value='".$i."'>".$i."</option>";
                                    }
                                    echo $opt;
                                    ?>
                            </select>
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-3">
                            <b>Target</b>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" type="number" id="f_target" name="input_target" placeholder="Masukkan Target" />
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-3">
                            <b>Satuan</b>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" type="text" id="f_satuan" name="input_satuan" placeholder="Masukkan Satuan dari Target" />
                        </div>
                    </div>
			    </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/handsontable/dist/handsontable.full.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
	
    Yii::app()->clientScript->registerScript('initdatatable', '

        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }


        $(".btn-input-target").on("click", function(){
            var id = $(this).attr("id");
            var kegiatan = $("#row_kegiatan_"+id).html();

            $("#f_id_kegiatan").val(id);
            $("#f_id_target").val("");

            $("#f_title").html("Input Target");
            $("#m_kegiatan").html(kegiatan);

            $(".form-input-target").modal("show");

        });

        $(".btn-u").on("click", function(){    
            $("#f_id_kegiatan").val("");
            $("#f_id_target").val("");

			var id=$(this).attr("id");
            var res=id.split("_");

			$("#f_title").html("Update Target")
			$.ajax({
				url: "'.Yii::app()->createUrl("ran/viewtarget").'",
				type: "POST",
				data: { 
                    id:res[1]
                },
				beforeSend : function(event){
                    $("#f_target").val("");
					$("#f_satuan").val("");
                    
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					
					$("#f_id_target").val(res.data.id);
                    $("#f_id_kegiatan").val(res.data.id_kegiatan);
                    $("#f_tahun").val(res.data.tahun);
                    $("#f_target").val(res.data.target);
                    $("#f_satuan").val(res.data.satuan);
                    $("#m_kegiatan").html(res.data.kegiatan);
					
                    $(".form-input-target").modal("show");
				}
			});
			
        });

        $("#form-input-target").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
            console.log(formdata);
    
            $.ajax({
                url: "'.Yii::app()->createUrl("ran/createtarget").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type,
                        showCancelButton: false,
                        confirmButtonClass: "btn btn-success",
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                        $(".form-input-target").modal("hide");
                        location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

         $(".btn-d").on("click", function(){
            var id=$(this).attr("id");
            var res=id.split("_");

            swal({
                title: "Yakin menghapus Target Rencana Aksi Nasional ini?",
                text: "Target Rencana Aksi Nasional yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("ran/deletetarget").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })
        });

		$(document).ready(function() {
			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listberita\').DataTable();
			
		} );
	');
?>