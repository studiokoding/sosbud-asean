<?php
/* @var $this RanController */
/* @var $model MKegiatan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'iku'); ?>
		<?php echo $form->textArea($model,'iku',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kegiatan'); ?>
		<?php echo $form->textArea($model,'kegiatan',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pj_sektoralsub'); ?>
		<?php echo $form->textField($model,'pj_sektoralsub'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pj_sektoral'); ?>
		<?php echo $form->textField($model,'pj_sektoral'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pj_kl'); ?>
		<?php echo $form->textField($model,'pj_kl'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->