<?php
/* @var $this RanController */
/* @var $model MKegiatan */

$this->breadcrumbs=array(
	'Mkegiatans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MKegiatan', 'url'=>array('index')),
	array('label'=>'Create MKegiatan', 'url'=>array('create')),
	array('label'=>'View MKegiatan', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MKegiatan', 'url'=>array('admin')),
);
?>

<h1>Update MKegiatan <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>