<?php
/* @var $this BeritaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Master Rencana Aksi Nasional',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/public/plugins/handsontable/dist/handsontable.full.min.css");
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');
?>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- <h4 class="page-title">Realisasi Rencana Aksi Nasional</h4> -->
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Realisasi Rencana Aksi Nasional</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <br>
                        <table
                            id = "listmaster"
                            class = "table table-stripde table-bordered"
                            style = "border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Rencana Aksi</th>
                                    <th scope="col">Indikator</th>
                                    <th scope="col">Tahun</th>
                                    <th scope="col">Target</th>
                                    <th scope="col" style="width:40%">Realisasi</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($modelTarget as $key=>$m): ?>
                                    <tr>
                                        <td><?= $key+1 ?></td>
                                        <td><?= $m->kegiatan0->iku ?></td>
                                        <td id="row_kegiatan_<?= $m->id ?>"><?= $m->kegiatan0->kegiatan ?></td>
                                        <td id="row_tahun_<?= $m->id ?>"><?= $m->tahun ?></td>
                                        <td id="row_target_<?= $m->id ?>"><?= $m->target ?> <?= $m->satuan ?></td>
                                        <td class="text-center">
                                            <?php if(count($m->ranRealisasis)==0): ?>
                                                <span class="badge badge-warning">Belum Input Realisasi</span>
                                            <?php endif ?>
                                            <table class="table table-borderless">
                                            <?php foreach($m->ranRealisasis as $index=>$real): ?>
                                                 <?php if(count($m->ranRealisasis)!=1 && ($index+1)!=count($m->ranRealisasis)): ?>
                                                    <tr style="border-bottom: 1px solid #dee2e6;">
                                                <?php else: ?>
                                                    <tr>
                                                <?php endif ?>
                                                    <td><strong>Triwulan <?= $real->triwulan ?></strong></td> 
                                                    <td>:</td>
                                                    <td><?= $real->realisasi ?> <?= $m->satuan ?></td>
                                                    <td class="text-left">
                                                        <ol>
                                                        <?php foreach($real->ranDatadukungs as $dukungs): ?>
                                                            <li id="row-data-dukung-<?= $dukungs->id ?>"><a style="color:blue; " target="blank" href="<?php echo Yii::app()->baseUrl . '/../assets/datadukung/' . $dukungs->file; ?>"> Download File <?= $dukungs->filename ?> </a></li>
                                                        <?php endforeach ?>
                                                        </ol>
                                                    </td>
                                                    <td>
                                                        <?php if(($sektoralsub==$m->kegiatan0->pj_sektoralsub && $sektoral==$m->kegiatan0->pj_sektoral) || Yii::app()->user->level==10): ?> 
                                                            <button id-realisasi="<?= $real->id ?>" class="btn-edit-realisasi btn btn-primary btn-sm mt-2">Edit</button>
                                                            <button id-realisasi="<?= $real->id ?>" class="btn-delete-realisasi btn btn-danger btn-sm mt-2">Delete</button>
                                                        <?php endif ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?> 
                                            </table>
                                        </td>
                                        <td>
                                            <?php if(($sektoralsub==$m->kegiatan0->pj_sektoralsub && $sektoral==$m->kegiatan0->pj_sektoral) || Yii::app()->user->level==10): ?> 
                                                <div class="button-items">
    												<button type="button"  id="<?= $m->id ?>" id_kegiatan="<?= $m->kegiatan ?>" class="btn-input-realisasi btn btn-success btn-sm btn-block waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Input Realisasi</button>
    											</div>
                                            <?php else: ?>
                                                <span class="badge badge-info">Hanya Bisa Diinput Badan Sektoral Terkait</span>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade form-input-realisasi tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="<?php echo Yii::app()->createUrl('ran/createrealisasi');?>" method="post" enctype="multipart/form-data">
                <input type="hidden" id="f_id_target" name="input_id_target" />
                <input type="hidden" id="f_id_kegiatan" name="input_kegiatan" />
                <input type="hidden" id="f_tahun" name="input_tahun" />
                <input type="hidden" id="f_id_realisasi" name="input_id_realisasi" />
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <b>Indikator</b>
                        </div>
                        <div class="col-md-9">
                            <div id="m_kegiatan"></div>
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-3">
                            <b>Tahun</b>
                        </div>
                        <div class="col-md-9">
                            <div id="m_tahun"></div>
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-3">
                            <b>Target</b>
                        </div>
                        <div class="col-md-9">
                            <div id="m_target"></div>
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-3">
                            <b>Triwulan</b>
                        </div>
                        <div class="col-md-9">
                            <select name="input_triwulan" id="f_triwulan" class="form-control select2">`
                                <option value=0>:: Pilih Triwulan ::</option>
                                <?php
                                    $opt="";
                                    $year = date("Y");
                                    for($i=1;$i<=4;$i++){
                                        $opt.="<option value='".$i."'>Triwulan ".$i."</option>";
                                    }
                                    echo $opt;
                                    ?>
                            </select>
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-3">
                            <b>Realisasi</b>
                        </div>
                        <div class="col-md-9">
                            <input class="form-control" type="number" id="f_realisasi" name="input_realisasi" placeholder="Masukkan Realisasi Sesuai Triwulan yang Dipilih" />
                        </div>
                    </div>

                    <div class="row pt-4">
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-9">
                            <?php
                                $memberFormConfig = array(
                                    'elements' => array(
                                        'file' => array(
                                            'type' => 'file',
                                            'prompt' => ' :: Pilih File ::',
                                            'visible' => true,
                                            'label' => 'File Data Dukung',
                                            'accept'=>'application/pdf,image/jpg,image/jpeg'
                                        ),
                                        'jenis' => array(
                                            'type' => 'hidden',
                                            'visible' => true,
                                            'label' => 'jenis',
                                            'value' => 1,
                                        ),
                                    )
                                );
                                $this->widget('ext.multimodelform.MultiModelForm', array(
                                    'id' => 'fileid',
                                    'formConfig' => $memberFormConfig,
                                    'model' => $modelFileDukung,
                                    'bootstrapLayout' => true,
                                    'validatedItems' => $validatedFiles,
                                    'tableHtmlOptions' => array('class' => 'table'),
                                    //'data' => empty($validatedDocs) ? $modelDoc->findAll(array('condition'=>'id_jenis=:id_pkb','params'=>array(':id_pkb'=>$model1->id),'order'=>'id_jenis')) : null,
                                    'sortAttribute' => 'id_kontrak',
                                    // 'removeOnClick' => 'alert("Yakin delete item ini?")',
                                    'hideCopyTemplate' => false,
                                    'clearInputs' => true,
                                    'tableView' => true,
                                    // 'jsAfterCloneCallback'=>'alertIds',
                                    'showAddItemOnError' => true,
                                    'fieldsetWrapper' => array(
                                        'tag' => 'div',
                                        'htmlOptions' => array('class' => 'view', 'style' => 'position:relative;background:#EFEFEF;')
                                    ),
                                    'removeLinkWrapper' => array(
                                        'tag' => 'div',
                                        'htmlOptions' => array('style' => 'position:absolute; top:1em; right:1em;')
                                    ),
                                    'addItemText'=>'Tambah Data Dukung',
                                ));

                            ?>
                        </div>
                    </div>  

                    <div id="m-data-dukung" style="display:none" class="row pt-4">
                        <div class="col-md-3">
                            <strong>File Data Dukung yang Sudah Diupload</strong>
                        </div>
                        <div class="col-md-9">
                            <table class="table table-striped" id="table-data-dukung">
                            </table>                    
                        </div>
                    </div>                  
			    </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/handsontable/dist/handsontable.full.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
	
    Yii::app()->clientScript->registerScript('initdatatable', '
        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }

        $(".btn-edit-realisasi").on("click", function(){
            $("#m-data-dukung").show();
            $("#f_triwulan").val(0);
            $("#f_realisasi").val("");
            $("#f_title").html("Edit Realisasi");
            var idRealisasi = $(this).attr("id-realisasi");
            
            $.ajax({
				url: "'.Yii::app()->createUrl("ran/viewrealisasi").'",
				type: "POST",
				data: { id:idRealisasi },
				beforeSend : function(event){
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    console.log(res);

                    var id_target = res.data.target;
                    var id_kegiatan = res.data.kegiatan;
                    var tahun = res.data.tahun;
                    var triwulan = res.data.triwulan;
                    var realisasi = res.data.realisasi;
                    var id_realisasi = res.data.id;

                    $("#f_id_kegiatan").val(id_kegiatan);
                    $("#f_id_target").val(id_target);
                    $("#f_tahun").val(tahun);
                    $("#f_triwulan").val(triwulan);
                    $("#f_realisasi").val(realisasi);
                    $("#f_id_realisasi").val(id_realisasi);

                    var txt_kegiatan = $("#row_kegiatan_"+id_target).html();
                    var txt_target = $("#row_target_"+id_target).html();

                    $("#m_kegiatan").html(txt_kegiatan);
                    $("#m_tahun").html(tahun);
                    $("#m_target").html(txt_target);

                    var datadukung = res.data.datadukung;
                    
                    var content_tbl = "";
                    for(var i=0; i<datadukung.length; i++)
                    {
                        content_tbl += "<tr>";
                        var filename = datadukung[i].filename;
                        var file = datadukung[i].file;
                        var temp_td = "<td>";
                        var linkfile = "<a style=\"color:blue;\" target=\"blank\" href=\"'. Yii::app()->baseUrl . '/../assets/datadukung/" +file+" \"> Download File "+filename+" </a>";
                        temp_td += linkfile;
                        temp_td +="</td>"; 
                        temp_td +="<td>";
                        var linkhapus = "<a id-realisasi="+realisasi+" id-dukung="+datadukung[i].id+" class=\"delete-data-dukung btn btn-sm btn-danger\" href=\"#\"> Delete</a>";
                        temp_td +=linkhapus;
                        temp_td +="</td>"; 
                        content_tbl +=temp_td+"</tr>";    
                    }

                    $("#table-data-dukung").empty();
                    $("#table-data-dukung").append(content_tbl);

					$(".form-input-realisasi").modal("show");
				}
            });
        });


        $(".btn-input-realisasi").on("click", function(){
            $("#m-data-dukung").hide();
            $("#f_triwulan").val(0);
            $("#f_realisasi").val("");

            var id = $(this).attr("id");
            var kegiatan = $("#row_kegiatan_"+id).html();
            var id_kegiatan = $(this).attr("id_kegiatan");
            var tahun = $("#row_tahun_"+id).html();
            var target = $("#row_target_"+id).html();

            $("#f_id_kegiatan").val(id_kegiatan);
            $("#f_id_target").val(id);

            $("#f_title").html("Input Realisasi");

            $("#m_kegiatan").html(kegiatan);
            $("#m_tahun").html(tahun);
            $("#f_tahun").val(tahun);
            $("#m_target").html(target);

            $(".form-input-realisasi").modal("show");

        });

        $(document).on("click", ".delete-data-dukung", function(){
            var tempThis = $(this);
            var idDukung = $(this).attr("id-dukung");
            var idRealisasi = $(this).attr("id-realisasi");
            console.log(idDukung);
            console.log(idRealisasi);
            swal({
                title: "Yakin menghapus Data Dukung ini?",
                text: "Data Dukung yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("ran/deletedatadukung").'",
                    type: "POST",
                    data: { 
                        idDukung:idDukung,
                        idRealisasi : idRealisasi
                     },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        tempThis.parent().parent().hide();
                        $("#row-data-dukung-"+idDukung).hide();
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            
                        }, function (dismiss) {
                        });  
                    }
                });
            });

        });

        $(".btn-delete-realisasi").on("click", function(){
            var id=$(this).attr("id-realisasi");
            console.log(id);
            swal({
                title: "Yakin menghapus Target Rencana Aksi Nasional ini?",
                text: "Target Rencana Aksi Nasional yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("ran/deleterealisasi").'",
                    type: "POST",
                    data: { id:id },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })
        });
       

		$(document).ready(function() {
			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listberita\').DataTable();

            $("#fileid").addClass("btn btn-success mb-2");
            $(".mmf_addlink").addClass("float-right");
            
		} );
	');
?>