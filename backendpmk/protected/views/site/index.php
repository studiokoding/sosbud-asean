<style>
    .htDimmed{
        font-weight:bold;
        color:black;
    }
    .colHeader{
        font-weight:bold;
        color:black;
    }
    .border {
       box-shadow: inset 0 -2px 0 red;
    }

    .required{
        color:red;
    }
</style>

<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title">Selamat Datang di Halaman Admin Aplikasi Forkom ASEAN</h4>
            </div>
        </div>
    </div>
</div>
<!-- page-title-box -->

<div class="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary mini-stat position-relative">
                    <div class="card-body">
                        <div class="mini-stat-desc">
                            <h6 class="text-uppercase verti-label text-white-50"></h6>
                            <div class="text-white">
                                <h6 class="text-uppercase mt-0 text-white-50">Rekapitulasi Laporan</h6>
                                <h3 class="mb-3 mt-0"><?php echo $laporan;?></h3>
                                <div class="">
                                    <!-- <span class="badge badge-light text-info"></span> -->
                                    <!-- <span class="ml-2"></span> -->
                                </div>
                            </div>
                            <div class="mini-stat-icon">
                                <i class="mdi mdi-cube-outline display-2"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary mini-stat position-relative">
                    <div class="card-body">
                        <div class="mini-stat-desc">
                            <h6 class="text-uppercase verti-label text-white-50"></h6>
                            <div class="text-white">
                                <h6 class="text-uppercase mt-0 text-white-50">Rekapitulasi Deklarasi</h6>
                                <h3 class="mb-3 mt-0"><?php echo $deklarasi;?></h3>
                                <div class="">
                                    <!-- <span class="badge badge-light text-info"></span> -->
                                    <!-- <span class="ml-2"></span> -->
                                </div>
                            </div>
                            <div class="mini-stat-icon">
                                <i class="mdi mdi-buffer display-2"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary mini-stat position-relative">
                    <div class="card-body">
                        <div class="mini-stat-desc">
                            <h6 class="text-uppercase verti-label text-white-50"></h6>
                            <div class="text-white">
                                <h6 class="text-uppercase mt-0 text-white-50">Rekapitulasi Ran</h6>
                                <h3 class="mb-3 mt-0"><?php echo $ran_percent;?>%</h3>
                                <div class="">
                                      <!-- <span class="badge badge-light text-info"></span> -->
                                    <!-- <span class="ml-2"></span> -->
                                </div>
                            </div>
                            <div class="mini-stat-icon">
                                <i class="mdi mdi-tag-text-outline display-2"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card bg-primary mini-stat position-relative">
                    <div class="card-body">
                        <div class="mini-stat-desc">
                            <h6 class="text-uppercase verti-label text-white-50"></h6>
                            <div class="text-white">
                                <h6 class="text-uppercase mt-0 text-white-50">Rekapitulasi Berita</h6>
                                <h3 class="mb-3 mt-0"><?php echo $berita;?></h3>
                                <div class="">
                                      <!-- <span class="badge badge-light text-info"></span> -->
                                    <!-- <span class="ml-2"></span> -->
                                </div>
                            </div>
                            <div class="mini-stat-icon">
                                <i class="mdi mdi-briefcase-check display-2"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(Yii::app()->user->level==10): ?>
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-primary mini-stat position-relative">
                        <div class="card-body">
                            <div class="mini-stat-desc">
                                <h6 class="text-uppercase verti-label text-white-50"></h6>
                                <div class="text-white">
                                    <h6 class="text-uppercase mt-0 text-white-50">Realisasi Triwulan I</h6>
                                    <h3 class="mb-3 mt-0"><?= $persen_realisasi_tw_1 ?> %</h3>
                                    <div class="">
                                        <!-- <span class="badge badge-light text-info"></span> -->
                                        <!-- <span class="ml-2"></span> -->
                                    </div>
                                </div>
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-percent display-2"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-primary mini-stat position-relative">
                        <div class="card-body">
                            <div class="mini-stat-desc">
                                <h6 class="text-uppercase verti-label text-white-50"></h6>
                                <div class="text-white">
                                    <h6 class="text-uppercase mt-0 text-white-50">Realisasi Triwulan II</h6>
                                    <h3 class="mb-3 mt-0"><?= $persen_realisasi_tw_2 ?> %</h3>
                                    <div class="">
                                        <!-- <span class="badge badge-light text-info"></span> -->
                                        <!-- <span class="ml-2"></span> -->
                                    </div>
                                </div>
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-percent display-2"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-primary mini-stat position-relative">
                        <div class="card-body">
                            <div class="mini-stat-desc">
                                <h6 class="text-uppercase verti-label text-white-50"></h6>
                                <div class="text-white">
                                    <h6 class="text-uppercase mt-0 text-white-50">Realisasi Triwulan III</h6>
                                    <h3 class="mb-3 mt-0"><?= $persen_realisasi_tw_3 ?> %</h3>
                                    <div class="">
                                          <!-- <span class="badge badge-light text-info"></span> -->
                                        <!-- <span class="ml-2"></span> -->
                                    </div>
                                </div>
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-percent display-2"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card bg-primary mini-stat position-relative">
                        <div class="card-body">
                            <div class="mini-stat-desc">
                                <h6 class="text-uppercase verti-label text-white-50"></h6>
                                <div class="text-white">
                                    <h6 class="text-uppercase mt-0 text-white-50">Realisasi Triwulan IV</h6>
                                    <h3 class="mb-3 mt-0"><?= $persen_realisasi_tw_4 ?> %</h3>
                                    <div class="">
                                          <!-- <span class="badge badge-light text-info"></span> -->
                                        <!-- <span class="ml-2"></span> -->
                                    </div>
                                </div>
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-percent display-2"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h1>About</h1>
                        <?= $sektoral->about ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
