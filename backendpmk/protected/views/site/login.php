<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="card">
	<div class="card-body">

		<h3 class="text-center m-0">
			
		</h3>

		<div class="p-3">
			<h4 class="text-muted font-18 m-b-5 text-center">Selamat Datang</h4>
			<p class="text-muted text-center">Backend Forkom ASEAN</p>

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'action'=>Yii::app()->createUrl('site/login'),
				'enableClientValidation'=>false,
				'clientOptions'=>array(
					'validateOnSubmit'=>false,
				),
			)); ?>

				<div class="form-group">
					<?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'username'); ?>
				</div>

				<div class="form-group">
					<?php echo $form->labelEx($model,'password'); ?>
					<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
					<?php echo $form->error($model,'password'); ?>
				</div>

				<div class="form-group row m-t-20">
                	<div class="col-md-12 text-right">
						<?php echo CHtml::submitButton('Log In',array('class'=>'btn btn-primary btn-block w-md waves-effect waves-light')); ?>
                    </div>
                </div>

			<?php $this->endWidget(); ?>
		</div>

	</div>
</div>
