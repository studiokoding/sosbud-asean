<?php
/* @var $this DeklarasiController */
/* @var $model Deklarasi */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'deklarasi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'deklarasi_judul'); ?>
		<?php echo $form->textField($model,'deklarasi_judul',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'deklarasi_judul'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deklarasi_focal'); ?>
		<?php echo $form->textField($model,'deklarasi_focal',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'deklarasi_focal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deklarasi_user_id'); ?>
		<?php echo $form->textField($model,'deklarasi_user_id'); ?>
		<?php echo $form->error($model,'deklarasi_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deklarasi_adddate'); ?>
		<?php echo $form->textField($model,'deklarasi_adddate'); ?>
		<?php echo $form->error($model,'deklarasi_adddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deklarasi_summit_id'); ?>
		<?php echo $form->textField($model,'deklarasi_summit_id'); ?>
		<?php echo $form->error($model,'deklarasi_summit_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->