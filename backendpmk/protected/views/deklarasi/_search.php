<?php
/* @var $this DeklarasiController */
/* @var $model Deklarasi */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'deklarasi_id'); ?>
		<?php echo $form->textField($model,'deklarasi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deklarasi_judul'); ?>
		<?php echo $form->textField($model,'deklarasi_judul',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deklarasi_focal'); ?>
		<?php echo $form->textField($model,'deklarasi_focal',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deklarasi_user_id'); ?>
		<?php echo $form->textField($model,'deklarasi_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deklarasi_adddate'); ?>
		<?php echo $form->textField($model,'deklarasi_adddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deklarasi_summit_id'); ?>
		<?php echo $form->textField($model,'deklarasi_summit_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->