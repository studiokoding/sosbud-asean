<?php
/* @var $this DeklarasiController */
/* @var $model Deklarasi */

$this->breadcrumbs=array(
	'Deklarasis'=>array('index'),
	$model->deklarasi_id=>array('view','id'=>$model->deklarasi_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Deklarasi', 'url'=>array('index')),
	array('label'=>'Create Deklarasi', 'url'=>array('create')),
	array('label'=>'View Deklarasi', 'url'=>array('view', 'id'=>$model->deklarasi_id)),
	array('label'=>'Manage Deklarasi', 'url'=>array('admin')),
);
?>

<h1>Update Deklarasi <?php echo $model->deklarasi_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>