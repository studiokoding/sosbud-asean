<?php
/* @var $this meetingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'meetings',
);

	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');    
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-md-datetimepicker/css/bootstrap-material-datetimepicker.css');
?>
<style>
.hide_{
    display:none;
}

.show_{
    display:block;
}
</style>
<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- <h4 class="page-title">Deklarasi</h4> -->
            </div>
        </div>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="container-fluid">
	<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Daftar Deklarasi</h4>
                        <div id="loading">
                                    <div id="progstat">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                        <div style="width:100%;overflow-x:scroll;">
                        <div class="row">
                            <div class="col-md-2">
                                <button style="float:left;" id="btn-a" type="button" class="btn btn-success waves-effect waves-light">Tambah Deklarasi</button>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                        <br>
                        <table
                            id="listdeklarasi"
                            class="table table-striped table-bordered"
                            style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" style="width: 60%;">Deklarasi</th>
                                    <th scope="col" style="width: 20%;">KTT ASEAN / Pertemuan</th>
                                    <th scope="col" style="width: 10%;">Tanggal Deklarasi</th>
                                    <th scope="col" style="width: 8%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
										$tr="";
										$user="";
										
										foreach($dataProvider as $k=>$row){ 
                                            $dklrsi=Meeting::model()->findByPk($row['deklarasi_summit_id']);
											$button='<div class="button-items">
												<button type="button"  id="v_'.$row['deklarasi_id'].'" class="btn-v btn btn-primary btn-sm btn-block waves-effect waves-light"><i class="far fa-eye"></i> Lihat</button>
												<button type="button"  id="u_'.$row['deklarasi_id'].'" class="btn-u btn btn-warning btn-sm btn-block waves-effect waves-light"><i class="fas fa-pencil-alt"></i> Update</button>
												<button type="button"  id="d_'.$row['deklarasi_id'].'" class="btn-d btn btn-danger btn-sm btn-block waves-effect waves-light"><i class="far fa-trash-alt"></i> Hapus</button>
                                                <a href="'.Yii::app()->createUrl('implementasi/index',array("id_deklarasi"=>$row["deklarasi_id"])).'" class="btn btn-info btn-sm btn-block waves-effect waves-light"><i class="far fa-info"></i> Implementasi</a>
											</div>';

                                            $labeltext = "-";
                                            
                                            if($dklrsi){
                                                if($dklrsi->meeting_title)
                                                {
                                                    $labeltext = $dklrsi->meeting_title;
                                                }
                                                else if($dklrsi->meeting_sektoral_id){
                                                    $labeltext = $dklrsi->meetingSektoral->sektoral_nama .' ke-'.$dklrsi->meeting_urutan;     
                                                }
                                                else if($dklrsi->meeting_sektoralsub_id){
                                                    $labeltext = $dklrsi->meetingSektoralsub->sektoralsub_nama.' ke-'.$dklrsi->meeting_urutansub;
                                                }
                                                else{
                                                    $labeltext = $dklrsi->meeting_ketua.','.$dklrsi->meeting_ketuasub.' di '.$dklrsi->meeting_lokasi;
                                                }    
                                            }
                                            
                                            

											$tr.='<tr id="'.$row['deklarasi_id'].'">
												<td style="width: 2%;">'.($k+1).'</td>
												<td id="judul_'.$row['deklarasi_id'].'" style="width: 60%;">'.$row['deklarasi_judul'].'</td>
												<td style="width: 20%;">'.( ($dklrsi) ? $labeltext : "----").'</td>
												<td style="width: 10%;">'.Yii::app()->dateFormatter->format("dd MMM yyyy",$row['deklarasi_adddate']).'</td>
												<td  style="width: 8%;">'.$button.'</td>
											</tr>';
										}
										echo $tr;
									?>
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade view-deklarasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="deklarasi_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="col-md-12">
						<h4 id="deklarasi_judul" class="mt-0 header-title"></h4>
                        <hr>
						<table class="table table-striped mb-0">
                            <tbody>
                                <tr>
                                	<th scope="row">Tanggal Pertemuan</th>
                                    <td> : </td><td><span id="deklarasi_tgl"></span></td>
                                </tr>
								
							</tbody>
                        </table>
					</div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade form-deklarasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="f_title" class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
			<form id="form-deklarasi" method="post" enctype="multipart/form-data">                        
                <div class="modal-body">
                        <input type="hidden" id="f_id" name="input_id" value="">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">KTT ASEAN / Pertemuan</label>  
                                    <div class="col-sm-8">    
                                        <select name="input_summit" id="f_summit" class="form-control select2">`
                                        <option>:: Pilih Pertemuan ::</option>
                                        <?php
                                            $opt="";
                                            foreach($daftarPertemuan as $r){
                                                $labeltext = "-";
                                                if($r->meeting_title){
                                                    $labeltext = $r->meeting_title;
                                                }
                                                else if($r->meeting_sektoral_id){
                                                    $labeltext = $r->meetingSektoral->sektoral_nama .' ke-'.$r->meeting_urutan;     
                                                }
                                                else if($r->meeting_sektoralsub_id){
                                                    $labeltext = $r->meetingSektoralsub->sektoralsub_nama.' ke-'.$r->meeting_urutansub;
                                                }
                                                else{
                                                    $labeltext = $r->meeting_ketua.','.$r->meeting_ketuasub.' di '.$r->meeting_lokasi;
                                                }
                                                $opt.="<option value='".$r->meeting_id."'>".$labeltext."</option>";
                                            }
                                            echo $opt;
                                        ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
							<div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">
                                        Deklarasi
                                        <br><small class="text-warning">Max *255 karakter</small>
                                    </label>                                
                                    <div class="col-sm-8">
                                        <textarea class="text_wysiwyg" id="f_deklarasi" name="input_deklarasi" class="form-control" rows="4" placeholder="Overview"></textarea>                                        
                                    </div>
                                </div>
                            </div>
							
                        </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-form" type="submit button" class="btn btn-primary waves-effect waves-light">Simpan</button>
                </div>
                </form>
        </div>
    </div>
</div>


<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/bootstrap-md-datetimepicker/js/moment-with-locales.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/bootstrap-md-datetimepicker/js/bootstrap-material-datetimepicker.js', CClientScript::POS_END);
	
	Yii::app()->clientScript->registerScript('initdatatable', '

        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }

		$(".btn-v").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            
            $.ajax({
				url: "'.Yii::app()->createUrl("deklarasi/view").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
					$("#loading").css("display", "block");
					$("#deklarasi_title").html("-");
					$("#deklarasi_judul").html("-");
					$("#deklarasi_tgl").html("-");
				},
				success: function(result) {
					$("#loading").css("display", "none");
                    var res = JSON.parse(result);

					$("#deklarasi_title").html("Deklarasi");
					$("#deklarasi_judul").html(res.data.deklarasi);
					$("#deklarasi_tgl").html(res.data.tgl);
					$(".view-deklarasi").modal("show");
				}
            });
		});

		$(".btn-u").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            $("#deklarasi_title").html("Update Deklarasi");
			
			$.ajax({
				url: "'.Yii::app()->createUrl("deklarasi/view").'",
				type: "POST",
				data: { id:res[1] },
				beforeSend : function(event){
                    $("#f_id").val("");
					$("#f_summit").val("");
					$("#f_deklarasi").summernote("code", "");
                    
					$("#loading").css("display", "block");
				},
				success: function(result) {
					$("#loading").css("display", "none");
					var res = JSON.parse(result);
					//console.log(res);
					$("#f_id").val(res.data.id);
					$("#f_summit").val(res.data.summit);
					$("#f_deklarasi").summernote("pasteHTML", res.data.deklarasi);
                    $(".form-deklarasi").modal("show");  
				}
			});
			
        });
        
        $("#form-deklarasi").submit(function( event ) {
            $("#btn-form").attr("disabled",true);
            event.preventDefault();
         
            var formdata=new FormData(this);   
            console.log(formdata);
    
            $.ajax({
                url: "'.Yii::app()->createUrl("deklarasi/create").'",
                type: "POST",
                data:  formdata,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function(event){
                    $("#loading").css("display", "block");
                },
                success: function(result) {
                    $("#loading").css("display", "none");
                    var res = JSON.parse(result);
                    var type="error";
                    var msg=res.message;
                    if(res.status == 1){
                        type="success";
                    }
                   
                    swal({
                        title: "result",
                        text: msg,
                        type: type,
                        showCancelButton: false,
                        confirmButtonClass: "btn btn-success",
                    }).then(function () {
                        $("#btn-form").attr("disabled",false);
                        $(".form-deklarasi").modal("hide");
                        location.reload();
                    }, function (dismiss) {
                    });                    
                }
            });  
        });

        $(".btn-d").on("click", function(){    
			var id=$(this).attr("id");
			var res=id.split("_");
			var judul="#judul_"+res[1];
			var t=$(judul).text();
            
            swal({
                title: "Yakin menghapus deklarasi ini?",
                text: "deklarasi yang sudah dihapus tidak bisa dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn btn-success",
                cancelButtonClass: "btn btn-danger m-l-10",
                confirmButtonText: "Yakin, Hapus!!"
            }).then(function () {
                $.ajax({
                    url: "'.Yii::app()->createUrl("deklarasi/delete").'",
                    type: "POST",
                    data: { id:res[1] },
                    beforeSend : function(event){
                        $("#loading").css("display", "block");
                    },
                    success: function(result) {
                        $("#loading").css("display", "none");
                        var res = JSON.parse(result);
                        
                        var type="error";
                        var msg=res.message;
                        if(res.status == 1){
                            type="success";
                        }
                    
                        swal({
                            title: "result",
                            text: msg,
                            type: type,
                        }).then(function () {
                            location.reload();
                        }, function (dismiss) {
                        });  
                    }
                });
            })

			
        });

        $("#f_pertemuan").select2({
            placeholder : " :: Pilih Pertemuan :: "
        });

        $("#f_pertemuan").on("select2:select", function (e) { 
            $("#header_pertemuan").html("");
            var f = $("#f_pertemuan option:selected").text();
            $("#header_pertemuan").html(f);
        });


        $("#f_sektoral").on("select2:select", function (e) { 
            var f = $("#f_sektoral").val();
            $("#header_pertemuan").html("");
            if(f!=""){
                if(f==0){
                    $("#f_sektoral_id").val("");
                    $("#header_pertemuan").html("KTT ASEAN");			
                }
                else{
                    $("#f_sektoral_id").val(f);
                    $.ajax({
                        url: "'.Yii::app()->createUrl("meeting/getsektoral").'",
                        type: "POST",
                        data: { id:f },
                        beforeSend : function(event){
                            $("#loading").css("display", "block");
                            $("#pertemuan").removeClass("show_").addClass("hide_");
                        },
                        success: function(result) {
                            $("#loading").css("display", "none");
                            var res = JSON.parse(result);
                            if(res.status == 1){
                                $("#pertemuan").removeClass("hide_").addClass("show_");
                                $("#f_pertemuan").html(res.data);                
                            } else {
                                alert(res.message);
                            }
                        }
                    });
                }
            }
        });

        $("#btn-a").on("click", function(){  
            $("#f_id").val("");
            $("#f_summit").val("");
            $("#f_deklarasi").summernote("code", ""); 
            $("#deklarasi_title").html("Tambah Deklarasi");         
            $(".form-deklarasi").modal("show");
		});

		$(document).ready(function() {
            $("#f_tanggal1").bootstrapMaterialDatePicker({ 
                format : "YYYY-MM-DD", 
                weekStart : 0, 
                time: false ,
            }).on("change", function(e, date) {
                $("#f_tanggal2").bootstrapMaterialDatePicker("setMinDate", date);
            });
            
            $("#f_tanggal2").bootstrapMaterialDatePicker({ 
                format : "YYYY-MM-DD", 
                weekStart : 0, 
                time: false ,
            });

			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});

			var table = $(\'#listdeklarasi\').DataTable({
                "columnDefs": [{targets: [3], "type":"date"}]
            });
			
		} );
	');
?>