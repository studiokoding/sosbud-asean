<?php
/* @var $this DeklarasiController */
/* @var $model Deklarasi */

$this->breadcrumbs=array(
	'Deklarasis'=>array('index'),
	$model->deklarasi_id,
);

$this->menu=array(
	array('label'=>'List Deklarasi', 'url'=>array('index')),
	array('label'=>'Create Deklarasi', 'url'=>array('create')),
	array('label'=>'Update Deklarasi', 'url'=>array('update', 'id'=>$model->deklarasi_id)),
	array('label'=>'Delete Deklarasi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->deklarasi_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Deklarasi', 'url'=>array('admin')),
);
?>

<h1>View Deklarasi #<?php echo $model->deklarasi_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'deklarasi_id',
		'deklarasi_judul',
		'deklarasi_focal',
		'deklarasi_user_id',
		'deklarasi_adddate',
		'deklarasi_summit_id',
	),
)); ?>
