<?php
/* @var $this DeklarasiController */
/* @var $model Deklarasi */

$this->breadcrumbs=array(
	'Deklarasis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Deklarasi', 'url'=>array('index')),
	array('label'=>'Manage Deklarasi', 'url'=>array('admin')),
);
?>

<h1>Create Deklarasi</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>