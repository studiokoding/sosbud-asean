<?php
/* @var $this DeklarasiController */
/* @var $data Deklarasi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('deklarasi_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->deklarasi_id), array('view', 'id'=>$data->deklarasi_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deklarasi_judul')); ?>:</b>
	<?php echo CHtml::encode($data->deklarasi_judul); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deklarasi_focal')); ?>:</b>
	<?php echo CHtml::encode($data->deklarasi_focal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deklarasi_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->deklarasi_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deklarasi_adddate')); ?>:</b>
	<?php echo CHtml::encode($data->deklarasi_adddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deklarasi_summit_id')); ?>:</b>
	<?php echo CHtml::encode($data->deklarasi_summit_id); ?>
	<br />


</div>