<?php
/* @var $this ImplementasiController */
/* @var $model Implementasi */
/* @var $form CActiveForm */
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');  
?>

<div class="form ml-3">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'implementasi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<label class="col-md-2 col-form-label">Keterangan</label>
		<div class="col-md-4">
			<?php echo $form->textArea($model,'implementasi_keterangan', array('class'=>'form-control text_wysiwyg','rows' => 6)); ?>
			<?php echo $form->error($model,'implementasi_keterangan'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-2 col-form-label">Pengentri</label>
		<div class="col-md-4">
			<?php echo $form->textField($model,'implementasi_entri', array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'implementasi_entri'); ?>
		</div>
	</div>

	<div class="row buttons form-group ml-3">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-success waves-effect waves-light')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/bootstrap-md-datetimepicker/js/moment-with-locales.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/bootstrap-md-datetimepicker/js/bootstrap-material-datetimepicker.js', CClientScript::POS_END);
	
	Yii::app()->clientScript->registerScript('initdatatable', '

        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }

		

		$(document).ready(function() {

			$(".text_wysiwyg").summernote({
				toolbar: [
					["font", ["bold", "underline", "clear"]],
					["para", ["ul", "ol", "paragraph"]],
					["view", ["fullscreen", "codeview", "help"]],
				]
			});
			
		} );
	');
?>