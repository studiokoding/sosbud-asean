<?php
/* @var $this ImplementasiController */
/* @var $model Implementasi */

$this->breadcrumbs=array(
	'Implementasis'=>array('index'),
	$model->implementasi_id,
);

$this->menu=array(
	array('label'=>'List Implementasi', 'url'=>array('index')),
	array('label'=>'Create Implementasi', 'url'=>array('create')),
	array('label'=>'Update Implementasi', 'url'=>array('update', 'id'=>$model->implementasi_id)),
	array('label'=>'Delete Implementasi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->implementasi_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Implementasi', 'url'=>array('admin')),
);
?>

<h1>View Implementasi #<?php echo $model->implementasi_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'implementasi_id',
		'implementasi_keterangan',
		'implementasi_sektoral_id',
		'implementasi_user_id',
		'implementasi_adddate',
		'implementasi_deklarasi_id',
		'implementasi_entri',
	),
)); ?>
