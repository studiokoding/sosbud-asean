<?php
/* @var $this ImplementasiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Implementasi',
);


	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/dataTables.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/buttons.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/responsive.bootstrap4.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/datatables/select.dataTables.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/sweet-alert2/sweetalert2.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/select2/css/select2.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/summernote/summernote-bs4.css');    
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-md-datetimepicker/css/bootstrap-material-datetimepicker.css');
?>

<style>
    .htDimmed{
        font-weight:bold;
        color:black;
    }
    .colHeader{
        font-weight:bold;
        color:black;
    }
    .border {
       box-shadow: inset 0 -2px 0 red;
    }

    .required{
        color:red;
    }
</style>

<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title"></h4>
            </div>
        </div>
    </div>
</div>
<div class="page-content-wrapper">
    <div class="container-fluid">
		<div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="mt-0 header-title mb-4">Implementasi</h4>
						<div id="loading">
							<div id="progstat">
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6" style="background:white;border: 4px solid black;"><h1><img src="<?php echo Yii::app()->baseUrl;?>/images/reload.gif"> Loading... </h1></div>
									<div class="col-md-3"></div>
								</div>
							</div>
						</div>
						<div style="width:100% ">
						 	<div class="row mb-5">
								<div class="col-md-2">
									<a href="<?= Yii::app()->createUrl('implementasi/create',array("id_deklarasi"=>$id_deklarasi)) ?>" class="btn btn-success waves-effect waves-light"> Tambah Implementasi</a>
								</div>
								<div class="col-md-10"></div>
							</div>
						</div>
						<table
                            id = "listimplementasi"
                            class = "table table-striped table-bordered"
                            style = "border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 2%;">#</th>
                                    <th scope="col" >Keterangan</th>
                                    <th scope="col" >Pengentri</th>
                                    <th scope="col" style="width: 10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php foreach($model as $key=>$m): ?>
									<tr>
										<td><?= $key+1 ?></td>
										<td><?= $m->implementasi_keterangan ?></td>
										<td><?= $m->implementasi_entri ?></td>
                                        <td>
                                            <a href="<?= Yii::app()->createUrl('implementasi/update',array('id'=>$m->implementasi_id)) ?>" class="btn btn-warning waves-effect waves-light"> Update</a>
                                            <a href="<?= Yii::app()->createUrl('implementasi/delete',array('id'=>$m->implementasi_id,'id_deklarasi'=>$id_deklarasi)) ?>" class="btn btn-danger waves-effect waves-light"> Delete</a>
                                        </td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jquery.dataTables.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.buttons.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/jszip.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/pdfmake.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/vfs_fonts.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.html5.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.print.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/buttons.colVis.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.responsive.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/responsive.bootstrap4.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/datatables/dataTables.select.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/select2/js/select2.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/summernote/summernote-bs4.min.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/bootstrap-md-datetimepicker/js/moment-with-locales.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/public/plugins/bootstrap-md-datetimepicker/js/bootstrap-material-datetimepicker.js', CClientScript::POS_END);
	
	Yii::app()->clientScript->registerScript('initdatatable', '

        function show(z){
            document.getElementById(z).style.display="block";
        }
        function hide(z){		
            document.getElementById(z).style.display="none";	
        }

		$(document).ready(function() {
			var table = $(\'#listimplementasi\').DataTable();
		} );
	');
?>