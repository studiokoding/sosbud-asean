<?php
/* @var $this ImplementasiController */
/* @var $model Implementasi */

$this->breadcrumbs=array(
	'Update Implementasi'
);
?>

<div class="page-title-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- <h4 class="page-title">Deklarasi</h4> -->
            </div>
        </div>
    </div>
</div>
<div class="page-content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h4 class="mt-0 header-title mb-4">Tambah Implementasi</h4>
						<div style="width:100%;overflow-x:scroll;">
                        <br>
						<?php $this->renderPartial('_form', array('model'=>$model)); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>