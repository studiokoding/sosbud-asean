<?php
/* @var $this ImplementasiController */
/* @var $data Implementasi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('implementasi_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->implementasi_id), array('view', 'id'=>$data->implementasi_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('implementasi_keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->implementasi_keterangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('implementasi_sektoral_id')); ?>:</b>
	<?php echo CHtml::encode($data->implementasi_sektoral_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('implementasi_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->implementasi_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('implementasi_adddate')); ?>:</b>
	<?php echo CHtml::encode($data->implementasi_adddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('implementasi_deklarasi_id')); ?>:</b>
	<?php echo CHtml::encode($data->implementasi_deklarasi_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('implementasi_entri')); ?>:</b>
	<?php echo CHtml::encode($data->implementasi_entri); ?>
	<br />


</div>