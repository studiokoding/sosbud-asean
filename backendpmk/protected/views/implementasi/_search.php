<?php
/* @var $this ImplementasiController */
/* @var $model Implementasi */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'implementasi_id'); ?>
		<?php echo $form->textField($model,'implementasi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'implementasi_keterangan'); ?>
		<?php echo $form->textArea($model,'implementasi_keterangan',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'implementasi_sektoral_id'); ?>
		<?php echo $form->textField($model,'implementasi_sektoral_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'implementasi_user_id'); ?>
		<?php echo $form->textField($model,'implementasi_user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'implementasi_adddate'); ?>
		<?php echo $form->textField($model,'implementasi_adddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'implementasi_deklarasi_id'); ?>
		<?php echo $form->textField($model,'implementasi_deklarasi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'implementasi_entri'); ?>
		<?php echo $form->textField($model,'implementasi_entri',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->