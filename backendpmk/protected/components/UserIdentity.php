<?php


class UserIdentity extends CUserIdentity
{
	private $_id;
	private $level_id;
	
	public function authenticate()
	{
		$u= $this->username;
		$p= $this->password;
		
		$user=User::model()->findByAttributes(array('status'=>10),array(
			'condition' => 'username=:username',
			'params' => array(
			  ':username' => $u,
			),
		));
		if($user===null)
		    $this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$user->validatePassword($p))
		    $this->errorCode = self::ERROR_PASSWORD_INVALID;
		else
		{
		    $this->_id = $user->id;
		    $this->username = $user->username;
			$this->setState('level',$user->level);
			$this->setState('username',$user->username);
			$this->setState('sektoralsub',$user->user_sektoralsub_id);
		    $this->errorCode = self::ERROR_NONE;
	  	}
	    return $this->errorCode == self::ERROR_NONE;
	}
	
	public function getId()
        {
            return $this->_id;
        }
}
?>