<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('profile','updateprofile'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionProfile($id){
		$model = $this->loadModel($id);
		$this->render('profile',array(
			'model'=>$model,
		));
	}

    public function actionUpdateprofile()
    {
        $return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest){

            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["input_id"]);
            $input_username = $parser->purify($_POST["input_username"]);
            $input_email = $parser->purify($_POST["input_email"]);

            $error = false;
            $errMsg = "";

            if(strlen($input_username)==0)
            {
                $errMsg .= "Username Tidak Boleh Kosong<br>";
                $error = true;
            }
            else if(strlen($input_email)==0)
            {
                $errMsg .= "Email Tidak Boleh Kosong<br>";
                $error = true;
            }

			//update
            else if(!$error)
            {
                $model = $this->loadModel($id);
                $model->username = $input_username;
                $model->email = $input_email;
                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Profile Berhasil Diupdate.";
                }else{
                    $return['message'] = "Profile Gagal Diupdate";
                }
            }
            else if($error)
            {
                $return['status'] = 2;
                $return['message'] = $errMsg;
            }
        }

        echo json_encode($return);

    }


	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}
