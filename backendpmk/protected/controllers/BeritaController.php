<?php

class BeritaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('utama','vutama','dutama','info','create','createinfo','vinfo','dinfo'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	// ===============================================
	// BERITA UTAMA
	// ===============================================

	public function actionUtama(){
		$level=Yii::app()->user->level;
		
		$dataProvider=new CActiveDataProvider('Berita', array(
			'criteria'=>array(
				'order'=>'t.berita_id DESC',
			)
		));

		if($level == 3){
			$dataProvider=new CActiveDataProvider('Berita', array(
				'criteria'=>array(
					'condition'=>'berita_user_id='.Yii::app()->user->id,
					'order'=>'t.berita_id DESC',
				)
			));
		} 
		$this->render('utama',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionVutama(){
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			$model=$this->loadModel($id);
			if($model){
				$return['data']=array(
					'id'=>$model->berita_id,
					'judul'=>$model->berita_judul,
					'overview'=>$model->berita_overview,
					'foto'=>$model->berita_foto,
					'isi'=>$model->berita_isi,
					'userid'=>$model->berita_user_id,
					'useradd'=>$model->berita_adddate,
				);
				$return['status']=1;
				$return['message']="Berita ditemukan.";
			} else {
				$return['message']="Berita tidak ditemukan.";
			}
		}
		
		echo json_encode($return);
	}

	public function actionCreate()
	{
		$mimecheck=array('image/gif', 'image/jpeg' , 'image/png');
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$user=$parser->purify($_POST['input_user']);
			$judul=$parser->purify($_POST['input_judul']);
			$overview=$parser->purify($_POST['input_overview']);
			$isi=$parser->purify($_POST['input_isi']);
			$filename=preg_replace('/[^A-Za-z0-9 . -]/', '_', $judul);

			$pathFile="";
			//UPDATE
			if(isset($_POST['input_id']) && $_POST['input_id']!=""){
				$id=$parser->purify($_POST['input_id']);

				$model=Berita::model()->findByPk($id);
				if($model){
					$model->berita_judul=$judul;
					$model->berita_overview=$overview;
					$model->berita_isi=$isi;
					$model->berita_user_id=$user;
					$model->berita_adddate=date("Y-m-d H:i:s");
					
					$fotolama=$model->berita_foto;
					// if(strlen(trim(CUploadedFile::getInstance ($model,'berita_foto'))) > 0){
					// 	$foto=CUploadedFile::getInstance($model, 'berita_foto');
					// 	$pathFile=Yii::app()->params['path_berita'].$filename.'.'.$foto->extensionName;
					// 	if($foto->saveAs($pathFile)){
					// 		$model->berita_foto=$filename.'.'.$foto->extensionName;
					// 	}
					// } else {
					// 	$model->berita_foto=$fotolama;
					// }

					if(!empty($_FILES["input_foto"]["name"])){ 
						$mime=$_FILES["input_foto"]["type"];
						$fname=$_FILES["input_foto"]["name"];

						$filetype=".".explode(".",$fname)[1];

						$filename=$filename.$filetype;
						$pathFile=Yii::app()->params['path_berita'].$filename;

						if (in_array($mime, $mimecheck)) {
							if( move_uploaded_file($_FILES["input_foto"]["tmp_name"], $pathFile) ){
								$model->berita_foto=$filename;
							} else {
								$model->berita_foto=$fotolama;
							}
						}
					}
					
					if($model->save()){
						$return['status']=1;
						$return['message']="Berita berhasil diupdate.";
					} else {
						$return['message']="Berita gagal diupdate ".$pathFile." >> ".json_encode($model->getErrors());
					}
				} else {
					$return['message']="Berita yang anda cari tidak ditemukan.";
				}
			} else {
			//CREATE
				$model=new Berita;
				$model->berita_judul=$judul;
				$model->berita_overview=$overview;
				$model->berita_isi=$isi;
				$model->berita_user_id=Yii::app()->user->id;
				$model->berita_adddate=date("Y-m-d H:i:s");

				if(!empty($_FILES["input_foto"]["name"])){ 
					$mime=$_FILES["input_foto"]["type"];
					$fname=$_FILES["input_foto"]["name"];

					$filetype=".".explode(".",$fname)[1];

					$filename=$filename.$filetype;

					if (in_array($mime, $mimecheck)) {
						if( move_uploaded_file($_FILES["input_foto"]["tmp_name"], Yii::app()->params['path_berita'].$filename) ){
							$model->berita_foto=$filename;
						} else {
							$return['message']="Foto tidak boleh kosong";
						}
					}
				} 

				if($model->save()){
					$return['status']=1;
					$return['message']="Berita berhasil ditambahkan.";
				} else {
					$return['message']="Berita gagal ditambahkan.";
				}
			}
		}
		echo json_encode($return);
	}

	public function actionDutama()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			
			$model=$this->loadModel($id)->delete();
			if($model){
				$return['status']=1;
				$return['message']="Berita berhasil dihapus.";
			} else {
				$return['message']="Berita gagal dihapus.";
			}
		}
		
		echo json_encode($return);
	}
	// ===============================================
	// BERITA UTAMA
	// ===============================================
	

	// ===============================================
	// INFO
	// ===============================================

	public function actionInfo(){
		$level=Yii::app()->user->level;
		
		$dataProvider=new CActiveDataProvider('Info', array(
			'criteria'=>array(
				'order'=>'t.info_id DESC',
			)
		));
		
		if($level == 3){
			$dataProvider=new CActiveDataProvider('Info', array(
				'criteria'=>array(
					'condition'=>'info_user_id='.Yii::app()->user->id,
					'order'=>'t.info_id DESC',
				)
			));
		} 

		$this->render('info',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionCreateinfo()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$user=$parser->purify($_POST['input_user']);
			$text=$parser->purify($_POST['input_text']);
			$status=$parser->purify($_POST['input_status']);
			
			//UPDATE
			if(isset($_POST['input_id']) && $_POST['input_id']!=""){
				$id=$parser->purify($_POST['input_id']);

				$model=Info::model()->findByPk($id);
				if($model){
					$model->info_text=$text;
					$model->info_show=$status;
					$model->info_user_id=Yii::app()->user->id;
					$model->info_adddate=date("Y-m-d H:i:s");
					
					if($model->save()){
						$return['status']=1;
						$return['message']="Info berhasil diupdate.";
					} else {
						$return['message']="Info gagal diupdate ".$pathFile." >> ".json_encode($model->getErrors());
					}
				} else {
					$return['message']="Info yang anda cari tidak ditemukan.";
				}
			} else {
			//CREATE
				$model=new Info;
				$model->info_text=$text;
				$model->info_show=$status;
				$model->info_user_id=Yii::app()->user->id;
				$model->info_adddate=date("Y-m-d H:i:s");

				if($model->save()){
					$return['status']=1;
					$return['message']="Info berhasil ditambahkan.";
				} else {
					$return['message']="Info gagal ditambahkan.";
				}
			}
		}
		echo json_encode($return);
	}

	public function actionVinfo(){
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			$model=$this->loadInfo($id);
			if($model){
				$return['data']=array(
					'id'=>$model->info_id,
					'text'=>$model->info_text,
					'status'=>$model->info_show,
					// 'status'=>( ($model->info_show==1) ? "Tampil" : "Tidak Tampil" ),
					'userid'=>$model->info_user_id,
					'useradd'=>$model->info_adddate,
				);
				$return['status']=1;
				$return['message']="Info ditemukan.";
			} else {
				$return['message']="Info tidak ditemukan.";
			}
		}
		
		echo json_encode($return);
	}

	public function actionDinfo()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			
			$model=$this->loadInfo($id)->delete();
			if($model){
				$return['status']=1;
				$return['message']="Info berhasil dihapus.";
			} else {
				$return['message']="Info gagal dihapus.";
			}
		}
		
		echo json_encode($return);
	}

	// ===============================================
	// INFO
	// ===============================================


	public function loadModel($id)
	{
		$model=Berita::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadInfo($id)
	{
		$model=Info::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}
