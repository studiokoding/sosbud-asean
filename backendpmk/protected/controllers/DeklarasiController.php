<?php

class DeklarasiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			
			$model=$this->loadModel($id);
			if($model){

				$return['data']=array(
					'id'=>$model->deklarasi_id,
					'deklarasi'=>$model->deklarasi_judul,
					'user'=>$model->deklarasi_user_id,
					'tgl'=>$model->deklarasi_adddate,
					'summit'=>$model->deklarasi_summit_id,
				);
				$return['status']=1;
				$return['message']="Deklarasi ditemukan.";
			} else {
				$return['message']="Deklarasi tidak ditemukan.";
			}
		}
		
		echo json_encode($return);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);


		if(Yii::app()->request->isPostRequest && isset($_POST['input_summit']) && isset($_POST['input_deklarasi']) && strlen($_POST['input_deklarasi'])!=0){

			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['input_id']);
			$id_meeting=$parser->purify($_POST['input_summit']);
			$deklarasi=$parser->purify($_POST['input_deklarasi']);
		
			// CREATE
			if($id==""){
				$model=new Deklarasi;
				$model->deklarasi_judul=$deklarasi;
				$model->deklarasi_user_id=Yii::app()->user->id;
				$model->deklarasi_adddate=date('Y-m-d H:i:s');
				$model->deklarasi_summit_id=$id_meeting;

				if($model->save()){
					$return['status']=1;
					$return['message']="Deklarasi berhasil ditambahkan.";
				} else {
					$return['message']="Deklarasi gagal ditambahkan.";
				}
			} else { //UPDATE
				$model=$this->loadModel($id);
				$model->deklarasi_judul=$deklarasi;
				$model->deklarasi_user_id=Yii::app()->user->id;
				$model->deklarasi_adddate=date('Y-m-d H:i:s');
				$model->deklarasi_summit_id=$id_meeting;
				if($model->save()){
					$return['status']=1;
					$return['message']="Deklarasi berhasil diupdate.";
				} else {
					$return['message']="Deklarasi gagal diupdate.";
				}
			}
				
		}
		else{
			$return['message'] = 'Pastikan Semua Isian Terisi';
		}
		echo json_encode($return);
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Deklarasi']))
		{
			$model->attributes=$_POST['Deklarasi'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->deklarasi_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			try{
				$model=$this->loadModel($id)->delete();
				if($model){
					$return['status']=1;
					$return['message']="Deklarasi berhasil dihapus.";
				} else {
					$return['message']="Anda Harus Menghapus Semua Implementasi Pada Deklarasi Ini";
				}
			}catch(\Exception $e)
			{
				$return['message']="Anda Harus Menghapus Semua Implementasi Pada Deklarasi Ini";
			}
			
		}
		
		echo json_encode($return);
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$level=Yii::app()->user->level;		
		$dataProvider=Deklarasi::model()->findAll(array('order'=>'deklarasi_id DESC'));
		if($level == 3){
			$dataProvider=Deklarasi::model()->findAllByAttributes(array('deklarasi_user_id'=>Yii::app()->user->id),array('order'=>'deklarasi_id DESC'));
		} 
		
		$daftarPertemuan=Meeting::model()->findAll(array('order'=>'meeting_id DESC'));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'daftarPertemuan'=>$daftarPertemuan
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Deklarasi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Deklarasi']))
			$model->attributes=$_GET['Deklarasi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Deklarasi the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Deklarasi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Deklarasi $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='deklarasi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
