<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->isGuest){
			$this->redirect(array('site/login'));
		} else{
			$user = User::model()->findByPk(Yii::app()->user->id);
			$sektoral = $user->userSektoralsub->sektoralsubSektoral;

			if(Yii::app()->user->level==10){
				$laporan = Meeting::model()->count("YEAR(meeting_tgl)=:year", array("year" =>date('Y')));
				$deklarasi = Deklarasi::model()->count("YEAR(deklarasi_adddate)=:year", array("year" =>date('Y')));

				$query_sum_realisasi = "SELECT SUM(realisasi) AS realisasi FROM ran_realisasi WHERE tahun=:tahun";

				$sum_realisasi = Yii::app()->db->createCommand($query_sum_realisasi)->bindValue('tahun',date('Y'))->queryAll()[0]['realisasi'];

				$query_sum_target = "SELECT SUM(target) AS target FROM ran_target WHERE tahun=:tahun";

				$sum_target = Yii::app()->db->createCommand($query_sum_target)->bindValue('tahun',date('Y'))->queryAll()[0]['target'];

				$ran_count = ($sum_realisasi/$sum_target)*100;
				$ran_percent = number_format($ran_count, 2, '.', '');

				$realisasi_tw_1 = "SELECT SUM(realisasi) AS realisasi FROM ran_realisasi WHERE tahun=:tahun AND triwulan=1";
				$realisasi_tw_2 = "SELECT SUM(realisasi) AS realisasi FROM ran_realisasi WHERE tahun=:tahun AND triwulan=2";
				$realisasi_tw_3 = "SELECT SUM(realisasi) AS realisasi FROM ran_realisasi WHERE tahun=:tahun AND triwulan=3";
				$realisasi_tw_4 = "SELECT SUM(realisasi) AS realisasi FROM ran_realisasi WHERE tahun=:tahun AND triwulan=4";

				$sum_realisasi_tw_1 = Yii::app()->db->createCommand($realisasi_tw_1)
										->bindValue('tahun',date('Y'))
										->queryAll()[0]['realisasi'];
				$sum_realisasi_tw_2 = Yii::app()->db->createCommand($realisasi_tw_2)
										->bindValue('tahun',date('Y'))
										->queryAll()[0]['realisasi'];
				$sum_realisasi_tw_3 = Yii::app()->db->createCommand($realisasi_tw_3)
										->bindValue('tahun',date('Y'))
										->queryAll()[0]['realisasi'];
				$sum_realisasi_tw_4 = Yii::app()->db->createCommand($realisasi_tw_4)
										->bindValue('tahun',date('Y'))
										->queryAll()[0]['realisasi'];

				$persen_realisasi_tw_1 =  number_format(($sum_realisasi_tw_1/$sum_target)*100, 2, '.', '');
				$persen_realisasi_tw_2 =  number_format(($sum_realisasi_tw_2/$sum_target)*100, 2, '.', '');
				$persen_realisasi_tw_3 =  number_format(($sum_realisasi_tw_3/$sum_target)*100, 2, '.', '');
				$persen_realisasi_tw_4 =  number_format(($sum_realisasi_tw_4/$sum_target)*100, 2, '.', '');


				$berita=Berita::model()->count("YEAR(berita_adddate)=:year", array("year" =>date('Y')));
			} else {
				$laporan = Meeting::model()->count("YEAR(meeting_tgl)=:year AND meeting_user_id=:user", array("year" =>date('Y'),"user"=>Yii::app()->user->id));
				$deklarasi = Deklarasi::model()->count("YEAR(deklarasi_adddate)=:year AND deklarasi_user_id=:user", array("year" =>date('Y'),"user"=>Yii::app()->user->id));

				$query_sum_realisasi = "SELECT SUM(r.realisasi) AS realisasi 
										FROM ran_realisasi r
										LEFT JOIN m_kegiatan k ON r.kegiatan = k.id
										WHERE r.tahun=:tahun
										AND k.pj_sektoral = :sektoral
										";

				$sum_realisasi = Yii::app()->db->createCommand($query_sum_realisasi)
									->bindValue('tahun',date('Y'))
									->bindValue('sektoral',$sektoral->sektoral_id)
									->queryAll()[0]['realisasi'];


				$query_sum_target = "SELECT SUM(t.target) AS target 
									FROM ran_target t
									LEFT JOIN m_kegiatan k ON t.kegiatan = k.id
									WHERE t.tahun=:tahun
									AND k.pj_sektoral = :sektoral";

				$sum_target = Yii::app()->db->createCommand($query_sum_target)
								->bindValue('tahun',date('Y'))
								->bindValue('sektoral',$sektoral->sektoral_id)
								->queryAll()[0]['target'];

				$ran_count = ($sum_realisasi/$sum_target)*100;

				$ran_percent = number_format($ran_count, 2, '.', '');

				$realisasi_tw_1 = "SELECT SUM(r.realisasi) AS realisasi FROM ran_realisasi r LEFT JOIN m_kegiatan k ON r.kegiatan = k.id WHERE r.tahun=:tahun AND r.triwulan=1 AND k.pj_sektoral = :sektoral";
				$realisasi_tw_2 = "SELECT SUM(r.realisasi) AS realisasi FROM ran_realisasi r LEFT JOIN m_kegiatan k ON r.kegiatan = k.id WHERE r.tahun=:tahun AND r.triwulan=2 AND k.pj_sektoral = :sektoral";
				$realisasi_tw_3 = "SELECT SUM(r.realisasi) AS realisasi FROM ran_realisasi r LEFT JOIN m_kegiatan k ON r.kegiatan = k.id WHERE r.tahun=:tahun AND r.triwulan=3 AND k.pj_sektoral = :sektoral";
				$realisasi_tw_4 = "SELECT SUM(r.realisasi) AS realisasi FROM ran_realisasi r LEFT JOIN m_kegiatan k ON r.kegiatan = k.id WHERE r.tahun=:tahun AND r.triwulan=4 AND k.pj_sektoral = :sektoral";

				$sum_realisasi_tw_1 = Yii::app()->db->createCommand($realisasi_tw_1)
										->bindValue('tahun',date('Y'))
										->bindValue('sektoral',$sektoral->sektoral_id)
										->queryAll()[0]['realisasi'];
				$sum_realisasi_tw_2 = Yii::app()->db->createCommand($realisasi_tw_2)
										->bindValue('tahun',date('Y'))
										->bindValue('sektoral',$sektoral->sektoral_id)
										->queryAll()[0]['realisasi'];
				$sum_realisasi_tw_3 = Yii::app()->db->createCommand($realisasi_tw_3)
										->bindValue('tahun',date('Y'))
										->bindValue('sektoral',$sektoral->sektoral_id)
										->queryAll()[0]['realisasi'];
				$sum_realisasi_tw_4 = Yii::app()->db->createCommand($realisasi_tw_4)
										->bindValue('tahun',date('Y'))
										->bindValue('sektoral',$sektoral->sektoral_id)
										->queryAll()[0]['realisasi'];

				$persen_realisasi_tw_1 =  number_format(($sum_realisasi_tw_1/$sum_target)*100, 2, '.', '');
				$persen_realisasi_tw_2 =  number_format(($sum_realisasi_tw_2/$sum_target)*100, 2, '.', '');
				$persen_realisasi_tw_3 =  number_format(($sum_realisasi_tw_3/$sum_target)*100, 2, '.', '');
				$persen_realisasi_tw_4 =  number_format(($sum_realisasi_tw_4/$sum_target)*100, 2, '.', '');

				$berita=Berita::model()->count("YEAR(berita_adddate)=:year AND berita_user_id=:user", array("year" =>date('Y'),"user"=>Yii::app()->user->id));
			}

			
			
			$this->render('index',array(
				'laporan'=>$laporan,
				'deklarasi'=>$deklarasi,
				'ran_percent'=>$ran_percent,
				'berita'=>$berita,
				'sektoral'=>$sektoral,
				'persen_realisasi_tw_1' => $persen_realisasi_tw_1,
				'persen_realisasi_tw_2' => $persen_realisasi_tw_2,
				'persen_realisasi_tw_3' => $persen_realisasi_tw_3,
				'persen_realisasi_tw_4' => $persen_realisasi_tw_4,
			));
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout="login";
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}