<?php

class LaporanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('statistik'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionStatistik(){
        
        $sql_sektoralsub = "SELECT sektoralsub_id,sektoralsub_singkatan FROM sektoralsub;";
        $sektoralsub = Yii::app()->db->createCommand($sql_sektoralsub)->queryAll();

        $datalaporan=array();

        foreach($sektoralsub as $s){
            $datalaporan[$s['sektoralsub_singkatan']][date('Y')]=0;
        }

        $sql = "SELECT CASE WHEN ( base_rekap.sektoral_sub_id IS NOT NULL ) THEN ( SELECT base_rekap.sektoral_sub_id ) ELSE (SELECT 0) END AS 'id', COUNT(*) AS jumlah ,sektoralsub.sektoralsub_singkatan as nama FROM base_rekap RIGHT JOIN sektoralsub ON base_rekap.sektoral_sub_id = sektoralsub.sektoralsub_id WHERE sektoral_sub_id IS NOT NULL and tahun =:tahun GROUP BY sektoral_sub_id";
        $data = Yii::app()->db->createCommand($sql)->bindValue('tahun',date('Y'))->queryAll();
        if($data){
            foreach($data as $d){
                $datalaporan[$d['nama']][date('Y')]=$d['jumlah'];
            }
        }

		$this->render('statistik',array(
			'datalaporan'=>$datalaporan,
		));
	}

	public function loadModel($id)
	{
		$model=Laporan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}
