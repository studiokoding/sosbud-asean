<?php

class ImplementasiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id_deklarasi)
	{
		$model=new Implementasi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Implementasi']))
		{
			//cari sektoral id user
			$user = User::model()->findByPk(Yii::app()->user->id); 
			$user_sektoral_id = $user->userSektoralsub->sektoralsubSektoral->sektoral_id;

			$model->attributes=$_POST['Implementasi'];
			$model->implementasi_sektoral_id = $user_sektoral_id;
			$model->implementasi_user_id = Yii::app()->user->id;
			$model->implementasi_adddate = date("Y-m-d H:i:s");
			$model->implementasi_deklarasi_id = $id_deklarasi;
			if($model->save())
				$this->redirect(array('index','id_deklarasi'=>$id_deklarasi));
		}

		$this->render('create',array(
			'model'=>$model,
			'id_deklarasi' => $id_deklarasi,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Implementasi']))
		{
			$model->attributes=$_POST['Implementasi'];
			if($model->save())
				$this->redirect(array('index','id_deklarasi'=>$model->implementasi_deklarasi_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id, $id_deklarasi)
	{
		$this->loadModel($id)->delete();
		$this->redirect(array('index','id_deklarasi'=>$id_deklarasi));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($id_deklarasi)
	{

        $model = Implementasi::model()->findAll(array("condition"=>"implementasi_deklarasi_id =  $id_deklarasi "));

		$this->render('index',array(
			'model'=>$model,
			'id_deklarasi' => $id_deklarasi,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Implementasi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Implementasi']))
			$model->attributes=$_GET['Implementasi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Implementasi the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Implementasi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Implementasi $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='implementasi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
