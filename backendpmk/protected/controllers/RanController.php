<?php

class RanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index',
								'view',
								'inputtarget',
								'createtarget',
								'viewtarget',
								'deletetarget',
								'inputrealisasi',
								'createrealisasi',
								'viewrealisasi',
								'deletedatadukung',
								'deleterealisasi'
								),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Controller View Data Realisasi
	 * 22 Nov 2020
	 * dv
	 */
	public function actionViewRealisasi()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);
		if(Yii::app()->request->isPostRequest){
			$parser = new CHtmlPurifier();
			$id = $parser->purify($_POST['id']);

			$model = RanRealisasi::model()->findByPk($id);

			$datadukung = [];

			foreach($model->ranDatadukungs as $dd){
				$temp_dd["id"] = $dd->id;
				$temp_dd["filename"] = $dd->filename;
				$temp_dd["file"] = $dd->file;
				array_push($datadukung, $temp_dd);
			}

			if($model){
                $return['data'] = array(
                    'id' => $id,
                    'tahun' => $model->tahun,
                    'triwulan' => $model->triwulan,
                    'kegiatan' => $model->kegiatan,
					'target' => $model->target,
					'realisasi' => $model->realisasi,
					'datadukung' => $datadukung,
                );
                $return["status"] = 1;
                $return["message"] = "Realisasi Ditemukan";
            }else{
                $return["message"] = "Realisasi tidak ditemukan";
            }
		}

		echo json_encode($return);
	}

	/**
	 * Controller Halaman Input Realisasi
	 * 20 Nov 2020
	 * dv
	 */
	public function actionInputRealisasi()
	{
		Yii::import('ext.multimodelform.MultiModelForm');
		$modelFileDukung = new RanDatadukung;
		$validatedFiles = array();
		
		$modelTarget=array();
		/*if(Yii::app()->user->level==10){
			$modelTarget = RanTarget::model()->findAllByAttributes(array("tahun"=>date('Y')),array('order'=>'kegiatan DESC'));
		} else {
			$sektoralsub=Yii::app()->user->sektoralsub;
			$sektoral=Sektoralsub::model()->findByPk($sektoralsub)->sektoralsub_sektoral_id;
			$modelKegiatan = MKegiatan::model()->findAllByAttributes(array("pj_sektoral"=>$sektoral,"pj_sektoralsub"=>$sektoralsub));
			$idKegiatan=array();
			foreach($modelKegiatan as $r){
				array_push($idKegiatan,$r->id);
			}
			
			$criteria=new CDbCriteria(); 
			$criteria->addCondition("tahun=".date('Y')); 
			$criteria->addInCondition('kegiatan',$idKegiatan);
			$criteria->order='kegiatan DESC';
			$modelTarget = RanTarget::model()->findAll($criteria);
		}*/
		
		$modelTarget = RanTarget::model()->findAllByAttributes(array("tahun"=>date('Y')),array('order'=>'kegiatan DESC'));
		$sektoralsub=Yii::app()->user->sektoralsub;
		$sektoral=Sektoralsub::model()->findByPk($sektoralsub)->sektoralsub_sektoral_id;

		$this->render('realisasi',array(
			'modelTarget' => $modelTarget,
			'sektoralsub' => $sektoralsub,
			'sektoral' => $sektoral,
			'modelFileDukung' => $modelFileDukung,
			'validatedFiles' => $validatedFiles,
		));
	}

	public function actionCreateRealisasi()
	{
		if(isset($_POST)){
			$input_triwulan = $_POST['input_triwulan'];
			$input_realisasi = $_POST['input_realisasi'];
			$input_tahun = $_POST['input_tahun'];
			$input_kegiatan = $_POST['input_kegiatan'];
			$input_target = $_POST['input_id_target'];
			$id_realisasi = $_POST['input_id_realisasi'];

			$modelRealisasi = new RanRealisasi;

			if($id_realisasi!="")
			{
				$modelRealisasi = RanRealisasi::model()->findByPk($id_realisasi);
			}
		

			$modelRealisasi->target = $input_target;
			$modelRealisasi->tahun = $input_tahun;
			$modelRealisasi->triwulan = $input_triwulan;
			$modelRealisasi->kegiatan = $input_kegiatan;
			$modelRealisasi->realisasi = $input_realisasi;

			if($modelRealisasi->save())
			{
				if($_FILES['RanDatadukung']){
					try{
						$total_file = count($_FILES['RanDatadukung']['name']['file']);
						for ($i = 0; $i < $total_file; $i++) {
							$tmpFilePath = $_FILES['RanDatadukung']['tmp_name']['file'][$i];
							$file_dukung = $_FILES['RanDatadukung']['name']['file'][$i];
							
							$exp = explode('.', $file_dukung);
							$file_dukung_ext = end($exp);

							$modelDataDukung = new RanDatadukung;
							$modelDataDukung->id_realisasi = $modelRealisasi->id;
							$modelDataDukung->file = str_replace('/', '_', 'dukung_['.$i .''. date('YmdHis', time()) . '].' . $file_dukung_ext);
							$modelDataDukung->filename = $file_dukung;
							$modelDataDukung->save();
							if ($tmpFilePath != "") {
								$newFilePath = Yii::app()->params['path_datadukung'] . $modelDataDukung->file;
								move_uploaded_file($tmpFilePath, $newFilePath);
							}
						}
					}
					catch(\Exception $e)
					{
						print_r($e);exit();
					}	
				}
			}
		}
		$this->redirect(array('inputrealisasi'));
	}
	/**
	 * Controller Halaman Input Target
	 * 19 Nov 2020
	 * dv
	 */
	public function actionInputTarget()
	{
		$modelKegiatan=array();
		/*if(Yii::app()->user->level==10){
			$modelKegiatan = MKegiatan::model()->findAll();
		} else {
			$sektoralsub=Yii::app()->user->sektoralsub;
			$sektoral=Sektoralsub::model()->findByPk($sektoralsub)->sektoralsub_sektoral_id;
			$modelKegiatan = MKegiatan::model()->findAllByAttributes(array("pj_sektoral"=>$sektoral,"pj_sektoralsub"=>$sektoralsub));
		}*/
		$sektoralsub=Yii::app()->user->sektoralsub;
		$sektoral=Sektoralsub::model()->findByPk($sektoralsub)->sektoralsub_sektoral_id;
		$modelKegiatan = MKegiatan::model()->findAll();

		$this->render('target',array(
			'modelKegiatan' => $modelKegiatan,
			'sektoralsub' => $sektoralsub,
			'sektoral' => $sektoral,
		));
	}

	public function actionCreateTarget()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest){
			$parser = new CHtmlPurifier();
			$input_id_kegiatan = $parser->purify($_POST["input_id_kegiatan"]);
			$input_id_target = $parser->purify($_POST["input_id_target"]);
			$input_tahun = $parser->purify($_POST["input_tahun"]);
			$input_target = $parser->purify($_POST["input_target"]);
			$input_satuan = $parser->purify($_POST["input_satuan"]);

			if(strlen($input_id_target)==0 && $input_id_target=="")
			{
				$model = new RanTarget;
				$model->kegiatan = $input_id_kegiatan;
				$model->tahun = $input_tahun;
				$model->target = $input_target;
				$model->satuan = $input_satuan;
				if($model->save()){
					$return['status'] = 1;
					$return['message'] = "Target Rencana Aksi Nasional Berhasil Ditambahkan.";
				}else{
					$return = "Target Gagal Ditambahkan";
				}
			}else{
				$model = RanTarget::model()->findByPk($input_id_target);
				$model->kegiatan = $input_id_kegiatan;
				$model->tahun = $input_tahun;
				$model->target = $input_target;
				$model->satuan = $input_satuan;
				if($model->save()){
					$return['status'] = 1;
					$return['message'] = "Target Rencana Aksi Nasional Berhasil Diupdate.";
				}else{
					$return = "Target Gagal Diupdate";
				}
			}
		}	

		echo json_encode($return);
	}

	public function actionViewTarget()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest ){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST['id']);

            $model = RanTarget::model()->findByPk($id);

            if($model){
                $return['data'] = array(
                    'id' => $id,
                    'tahun' => $model->tahun,
                    'id_kegiatan' => $model->kegiatan,
                    'target' => $model->target,
					'satuan' => $model->satuan,
					'kegiatan' => $model->kegiatan0->kegiatan,
                );
                $return["status"] = 1;
                $return["message"] = "Master Ditemukan";
            }else{
                $return["message"] = "Master tidak ditemukan";
            }
        }

        echo json_encode($return);
	}

	public function actionDeleteTarget()
	{
		$parser = new CHtmlPurifier();
		$id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["msg"] = "Gagal Menghapus";
        try{
            RanTarget::model()->findByPk($id)->delete();
            $return["status"] = 1;
            $return["msg"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {

        }	
        echo json_encode($return);
	}

	public function actionDeleteRealisasi()
	{
		$parser = new CHtmlPurifier();
		$id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["msg"] = "Gagal Menghapus";
        try{
            $realisasi = RanRealisasi::model()->findByPk($id);

			foreach($realisasi->ranDatadukungs as $dd)
			{
				RanDatadukung::model()->findByPk($dd->id)->delete();
			}

			$realisasi->delete();

            $return["status"] = 1;
            $return["msg"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {
			$return["data"] = $e;
        }	
        echo json_encode($return);
	}

	public function actionDeleteDataDukung()
	{
		$parser = new CHtmlPurifier();
		$idDukung = $parser->purify($_POST['idDukung']);
		$idRealisasi = $parser->purify($_POST['idRealisasi']);
		
		$return["status"] = "error";
        $return["msg"] = "Gagal Menghapus";
        try{
            RanDatadukung::model()->findByPk($idDukung)->delete();
            $return["status"] = 1;
            $return["msg"] = "Berhasil Menghapus";
			$return["idRealisasi"] = $idRealisasi;
        }
        catch(\Exception $e)
        {

        }	
        echo json_encode($return);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MKegiatan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MKegiatan']))
		{
			$model->attributes=$_POST['MKegiatan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MKegiatan']))
		{
			$model->attributes=$_POST['MKegiatan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MKegiatan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MKegiatan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MKegiatan']))
			$model->attributes=$_GET['MKegiatan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MKegiatan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MKegiatan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MKegiatan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mkegiatan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
