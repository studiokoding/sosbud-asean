<?php

class MasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index',
									'view',
									'ran',
									'createran',
									'viewran', 
									'deleteran',
									'kl',
									'createkl',
									'viewkl',
									'deletekl',
									'sektoral',
									'createsektoral',
									'viewsektoral',
									'deletesektoral',
									'sektoralsub',
									'createsektoralsub',
									'viewsektoralsub',
									'deletesektoralsub',
									'link',
									'createlink',
									'viewlink',
									'deletelink',
									'config',
									'createconfig',
									'viewconfig',
									'deleteconfig',
                                    'user',
                                    'createuser',
                                    'viewuser',
                                    'deleteuser',
                                    'changepassword'
									),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    /**
	 * 27 November 2020
	 * User
	 * dv
	 */
    public function actionUser()
	{
		$model = User::model()->findAll();
        $modelUserLevel = Userlevel::model()->findAll();
        $modelSektoralsub = Sektoralsub::model()->findAll();

		$this->render('user', array(
			'model' => $model,
            'modelUserLevel' => $modelUserLevel,
            'modelSektoralsub' => $modelSektoralsub
		));
	}

    public function actionCreateUser()
    {
        $return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest){

            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["input_id"]);
            $input_username = $parser->purify($_POST["input_username"]);
            $input_password = $parser->purify($_POST["input_password"]);
            $input_repeat_password = $parser->purify($_POST["input_repeat_password"]);
			$input_email = $parser->purify($_POST["input_email"]);
			$input_sektoralsub = $parser->purify($_POST["input_sektoralsub"]);
            $input_level = $parser->purify($_POST["input_level"]);

            $repeat = false;
            if($input_repeat_password===$input_password)
            {
                $repeat = true;
                $return['message'] = "Password dan Ulangi Password Tidak Sama";
            }

            //create
            if(strlen($id)==0 && $id=="" && $repeat)
            {
                $model = new User;
                $model->username = $input_username;
                $model->email = $input_email;
				$model->created_at = strtotime(date("Y-m-d H:i:s"));
                $model->updated_at = strtotime(date("Y-m-d H:i:s"));
                $model->user_sektoralsub_id = $input_sektoralsub;
                $model->level = $input_level;

                $hash = $model->generateHash();
                $model->password_hash = $hash;

                $auth_key = $model->hashPassword($input_password, $hash);
                $model->auth_key = $auth_key;

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master User Berhasil Ditambahkan.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
			//update
            else if($repeat)
            {
                $model = User::model()->findByPk($id);
				$model->username = $input_username;
                $model->email = $input_email;
                $model->updated_at = strtotime(date("Y-m-d H:i:s"));
                $model->user_sektoralsub_id = $input_sektoralsub;
                $model->level = $input_level;
                
                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Pengaturan Berhasil Diupdate.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
        }

        echo json_encode($return);
    }

    public function actionViewUser()
    {
        $return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest ){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST['id']);

            $model = User::model()->findByPk($id);

            if($model){
                $return['data'] = array(
                    'id' => $id,
                    'username' => $model->username,
					'email' => $model->email,
					'status' => $model->status,
                    'created_at' => $model->created_at,
                    'created_at_text' => date("Y-m-d H:i:s",$model->created_at),
                    'updated_at' => $model->updated_at,
                    'updated_at_text' => date("Y-m-d H:i:s",$model->updated_at),
                    'user_sektoralsub_id' => $model->user_sektoralsub_id,
                    'sektoralsub_text' => $model->userSektoralsub->sektoralsub_nama,
                    'level_text' => $model->level0->userlevel,
                    'level' => $model->level,
                );
                $return["status"] = 1;
                $return["message"] = "Master Ditemukan";
            }else{
                $return["message"] = "Master tidak ditemukan";
            }
        }

        echo json_encode($return);
    }

    public function actionDeleteUser()
	{
	    $parser = new CHtmlPurifier();
        $id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["message"] = "Gagal Menghapus";
        try{
            User::model()->findByPk($id)->delete();
            $return["status"] = 1;
            $return["message"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {

        }	
        echo json_encode($return);
	}

    public function actionChangePassword()
    {
        $return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["input_id"]);

            $model = User::model()->findByPk($id);
            $auth_key = $model->auth_key;
            $password_hash = $model->password_hash;
            
            $old_pass = $parser->purify($_POST["input_old_pass"]);
            $new_pass = $parser->purify($_POST["input_new_pass"]);
            $repeat_new_pass = $parser->purify($_POST["input_repeat_new_pass"]);

            $errMsg = "";
            $error = false;
            //check old pass
            $user = new User;
            if($auth_key !== $user->hashPassword($old_pass,$password_hash)){
                $errMsg.="Password Lama Salah<br>";
                $error = true;
            }

            if(strlen($old_pass)==0 || strlen($new_pass)==0 || strlen($repeat_new_pass)==0)
            {
                $errMsg.="Semua Field Harus Terisi<br>";
                $error = true;
            }

            if($new_pass!==$repeat_new_pass)
            {
                $errMsg.="Password Baru dan Ulangi Password Baru Tidak Sama!<br>";
                $error = true;
            }

            if(!$error)
            {
                $hash = $model->generateHash();
                $model->password_hash = $hash;

                $auth_key = $model->hashPassword($new_pass, $hash);
                $model->auth_key = $auth_key;

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Password Berhasil Diupdate.";
                }else{
                    $return['message'] = "Password Gagal Diupdate";
                }
            }else{
                $return['message'] = $errMsg;
            }
        }

        echo json_encode($return);
    }

	/**
	 * 27 November 2020
	 * Config
	 * dv
	 */
	public function actionConfig()
	{
		$model = Config::model()->findAll();

		$this->render('config', array(
			'model' => $model,
		));
	}

	public function actionCreateConfig()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest){

            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["input_id"]);
            $input_alias = $parser->purify($_POST["input_alias"]);
			$input_judul = $parser->purify($_POST["input_judul"]);
			$input_isi = $parser->purify($_POST["input_isi"]);

            //create
            if(strlen($id)==0 && $id=="")
            {
                $model = new Config;
                $model->alias = $input_alias;
                $model->judul = $input_judul;
				$model->isi = $input_isi;

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Pengaturan Berhasil Ditambahkan.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
			//update
            else 
            {
                $model = Config::model()->findByPk($id);
				$model->alias = $input_alias;
                $model->judul = $input_judul;
				$model->isi = $input_isi;
                
                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Pengaturan Berhasil Diupdate.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
        }

        echo json_encode($return);
	}

	public function actionViewConfig()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest ){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST['id']);

            $model = Config::model()->findByPk($id);

            if($model){
                $return['data'] = array(
                    'config_id' => $id,
                    'config_alias' => $model->alias,
					'config_judul' => $model->judul,
					'config_isi' => $model->isi,
                );
                $return["status"] = 1;
                $return["message"] = "Master Ditemukan";
            }else{
                $return["message"] = "Master tidak ditemukan";
            }
        }

        echo json_encode($return);
	}

	public function actionDeleteConfig()
	{
	$parser = new CHtmlPurifier();
        $id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["message"] = "Gagal Menghapus";
        try{
            Config::model()->findByPk($id)->delete();
            $return["status"] = 1;
            $return["message"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {

        }	
        echo json_encode($return);
	}

	/**
	 * 27 November 2020
	 * Link
	 * dv
	 */
	public function actionLink()
	{
		$model = Link::model()->findAll();

		$this->render('link', array(
			'model' => $model,
		));
	}

	public function actionCreateLink()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest){

            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["input_id"]);
            $input_nama = $parser->purify($_POST["input_nama"]);
			$input_href = $parser->purify($_POST["input_href"]);
			$input_opt = $parser->purify($_POST["input_opt"]);
			$input_show = $parser->purify($_POST["input_show"]);

            //create
            if(strlen($id)==0 && $id=="")
            {
                $model = new Link;
                $model->link_nama = $input_nama;
                $model->link_href = $input_href;
				$model->link_opt = $input_opt;
				$model->link_show = $input_show;

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Link Berhasil Ditambahkan.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
			//update
            else 
            {
                $model = Link::model()->findByPk($id);
				$model->link_nama = $input_nama;
                $model->link_href = $input_href;
				$model->link_opt = $input_opt;
				$model->link_show = $input_show;
                
                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Link Berhasil Diupdate.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
        }

        echo json_encode($return);
	}

	public function actionViewLink()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest ){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST['id']);

            $model = Link::model()->findByPk($id);

            if($model){
                $return['data'] = array(
                    'link_id' => $id,
                    'link_nama' => $model->link_nama,
					'link_href' => $model->link_href,
					'link_opt' => $model->link_opt,
					'link_show' => $model->link_show,
                );
                $return["status"] = 1;
                $return["message"] = "Master Ditemukan";
            }else{
                $return["message"] = "Master tidak ditemukan";
            }
        }

        echo json_encode($return);
	}

	public function actionDeleteLink()
	{
	$parser = new CHtmlPurifier();
        $id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["message"] = "Gagal Menghapus";
        try{
            Link::model()->findByPk($id)->delete();
            $return["status"] = 1;
            $return["message"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {

        }	
        echo json_encode($return);
	}

	/**
	 * 26 November 2020
	 * Badan Sektoral1/sektoralsub
	 */
	public function actionSektoralsub()
	{
		$model = Sektoralsub::model()->findAll();

		$modelSektoral = Sektoral::model()->findAll();

		$this->render('sektoralsub', array(
			'model' => $model,
			'modelSektoral' => $modelSektoral
		));
	}

	public function actionCreateSektoralsub()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest){

            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["input_id_sektoralsub"]);
            $input_nama = $parser->purify($_POST["input_nama"]);
			$input_singkatan = $parser->purify($_POST["input_singkatan"]);
			$sektoral = $parser->purify($_POST["input_sektoral"]);

            //create
            if(strlen($id)==0 && $id=="")
            {
                $model = new Sektoralsub;
                $model->sektoralsub_nama = $input_nama;
                $model->sektoralsub_singkatan = $input_singkatan;
				$model->sektoralsub_sektoral_id = $sektoral;
				$model->sektoralsub_user_id = Yii::app()->user->id;
				$model->sektoralsub_adddate = date("Y-m-d H:i:s");

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Badan Sektoral I Berhasil Ditambahkan.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
			//update
            else 
            {
                $model = Sektoralsub::model()->findByPk($id);
				$model->sektoralsub_nama = $input_nama;
                $model->sektoralsub_singkatan = $input_singkatan;
				$model->sektoralsub_sektoral_id = $sektoral;
                
                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Badan Sektoral I Berhasil Diupdate.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
        }

        echo json_encode($return);
	}

	public function actionViewSektoralsub()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest ){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST['id']);

            $model = Sektoralsub::model()->findByPk($id);

            if($model){
                $return['data'] = array(
                    'sektoralsub_id' => $id,
                    'sektoralsub_nama' => $model->sektoralsub_nama,
					'sektoralsub_singkatan' => $model->sektoralsub_singkatan,
					'sektoralsub_sektoral_id' => $model->sektoralsub_sektoral_id,
					'sektoral_nama' => $model->sektoralsubSektoral["sektoral_nama"],
                );
                $return["status"] = 1;
                $return["message"] = "Master Ditemukan";
            }else{
                $return["message"] = "Master tidak ditemukan";
            }
        }

        echo json_encode($return);
	}

	public function actionDeleteSektoralsub()
	{
	$parser = new CHtmlPurifier();
        $id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["message"] = "Gagal Menghapus";
        try{
            Sektoralsub::model()->findByPk($id)->delete();
            $return["status"] = 1;
            $return["message"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {

        }	
        echo json_encode($return);
	}

	/**
	 * 25 November 2020
	 * Badan Sektoral/Sektoral
	 * dv
	 */
	public function actionSektoral()
	{
		$model = Sektoral::model()->findAll(
					/* array(
                        'condition'=>'sektoral_nama!=""'
                    ) */);

		$modelKl = Kl::model()->findAll();

		$this->render('sektoral', array(
			'model' => $model,
			'modelKl' => $modelKl
		));
	}

	public function actionCreateSektoral()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest){

            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["input_id_sektoral"]);
            $input_nama = $parser->purify($_POST["input_nama"]);
			$input_singkatan = $parser->purify($_POST["input_singkatan"]);
			$kl = $parser->purify($_POST["input_kl"]);
			$overview = $parser->purify($_POST["input_overview"]);
            $about = $parser->purify($_POST['input_about']);

            //create
            if(strlen($id)==0 && $id=="")
            {
                $model = new Sektoral;
                $model->sektoral_nama = $input_nama;
                $model->sektoral_singkatan = $input_singkatan;
				$model->sektoral_kl_id = $kl;
				$model->sektoral_overview = $overview;
                $model->about = $about;
				$model->sektoral_user_id = Yii::app()->user->id;
				$model->sektoral_adddate = date("Y-m-d H:i:s");

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Badan Sektoral Berhasil Ditambahkan.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
			//update
            else 
            {
                $model = Sektoral::model()->findByPk($id);
				$model->sektoral_nama = $input_nama;
                $model->sektoral_singkatan = $input_singkatan;
				$model->sektoral_kl_id = $kl;
				$model->sektoral_overview = $overview;
                $model->about = $about;
                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Badan Sektoral Berhasil Diupdate.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
        }

        echo json_encode($return);
	}

	public function actionViewSektoral()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest ){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST['id']);

            $model = Sektoral::model()->findByPk($id);

            $sektoral_kl_id = '-';
            $sektoral_kl_nama = '-';
            if($model->sektoral_kl_id!=NULL)
            {
                $sektoral_kl_id = $model->sektoral_kl_id;
                $sektoral_kl_nama = $model->sektoralKl['kl_nama'];
            }

            if($model){
                $return['data'] = array(
                    'sektoral_id' => $id,
                    'sektoral_nama' => $model->sektoral_nama,
					'sektoral_singkatan' => $model->sektoral_singkatan,
					'sektoral_kl_id' => $sektoral_kl_id,
					'kl_nama' => $sektoral_kl_nama,
					'sektoral_overview' => $model->sektoral_overview,
                    'about' => $model->about,
                );
                $return["status"] = 1;
                $return["message"] = "Master Ditemukan";
            }else{
                $return["message"] = "Master tidak ditemukan";
            }
        }

        echo json_encode($return);
	}

	public function actionDeleteSektoral()
	{
	$parser = new CHtmlPurifier();
        $id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["message"] = "Gagal Menghapus";
        try{
            Sektoral::model()->findByPk($id)->delete();
            $return["status"] = 1;
            $return["message"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {

        }	
        echo json_encode($return);
	}

	/**24 November 2020
	 * KL
	 * dv
	 */
	public function actionKl()
	{
		$model = Kl::model()->findAll();
        
		$this->render('kl', array(
			'model' => $model,
		));
	}

	public function actionCreateKl()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);
        $error = false;
        $errMsg = "";

        if(Yii::app()->request->isPostRequest){

            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["input_id_kl"]);
            $input_nama = $parser->purify($_POST["input_nama"]);
            $nama_file_db = NULL;
            if(isset($_FILES['input_logo']) && $_FILES['input_logo']['name']!='')
            {
                $files = $_FILES['input_logo'];
                $file_name = $files['name'];
                $file_size =$files['size'];
                $file_tmp =$files['tmp_name'];
                $file_type=$files['type'];

                $exp = explode('.', $file_name);
                $file_ext = end($exp);

                $extensions= array("jpeg","jpg","png");
      
                if(in_array($file_ext,$extensions)=== false){
                    $errMsg .="extension not allowed, please choose a JPEG or PNG file.";
                    $error = true;
                }
      
                if($file_size > 2097152) {
                    $errMsg .='File size must be excately 2 MB';
                    $error = true;
                }
                $nama_file_db = date("YmdHis").'.'.$file_ext;
                $logoPath = Yii::app()->basePath . '/../../web/logo/' . $nama_file_db;
                if(empty($errors)==true  ) {
                     move_uploaded_file($file_tmp, $logoPath);
                }else{
                    print_r($errors);
                }
            }
            
            if(strlen($input_nama)==0)
            {
                $errMsg .= "Field Nama Tidak Boleh Kosong<br>";
                $error = true;
            }
            else if(strlen($input_nama)<=3)
            {
                $errMsg .= "Nama Kurang dari 3 karakter<br>";
                $error = true;
            }

            //create
            if(strlen($id)==0 && $id=="" && !$error)
            {
                
                $model = new Kl;
                $model->kl_id = $id;
                $model->kl_nama = $input_nama;
                $model->logo = $nama_file_db;

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Kementrian/Lembaga Berhasil Ditambahkan.";
                }else{
                    $return['message'] = "Master Gagal Ditambahkan";
                }
            }
			//update
            else if(!$error)
            {
                $model = Kl::model()->findByPk($id);
                $model->kl_nama = $input_nama;
                $model->logo = $nama_file_db;
                
                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Kementrian/Lembaga Berhasil Diupdate.";
                }else{
                    $return['message'] = "Update Gagal";
                }
            }
            else if($error)
            {
                $return['status'] = 2;
                $return['message'] = $errMsg;
            }
        }

        echo json_encode($return);
	}

	public function actionViewKl()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest ){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST['id']);

            $model = Kl::model()->findByPk($id);

            if($model){
                $return['data'] = array(
                    'kl_id' => $id,
                    'kl_nama' => $model->kl_nama,
                    'logo' => $model->logo,
                );
                $return["status"] = 1;
                $return["message"] = "Master Ditemukan";
            }else{
                $return["message"] = "Master tidak ditemukan";
            }
        }

        echo json_encode($return);
	}

	public function actionDeleteKl()
	{
	$parser = new CHtmlPurifier();
        $id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["message"] = "Gagal Menghapus";
        try{
            Kl::model()->findByPk($id)->delete();
            $return["status"] = 1;
            $return["message"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {

        }	
        echo json_encode($return);
	}

    /**
     * 19 November 2020 
     * Rencana Aksi Nasional
     * dv
     */
    public function actionRan()
    {
        $model = MKegiatan::model()->findAll();
        $sektoralSub = Sektoralsub::model()->findAll(array('order'=>'sektoralsub_id ASC'));

        $this->render('ran', array(
            'model' => $model,
            'sektoralSub' => $sektoralSub
        ));
    }
    /**
     * 19 November 2020
     * Create Master Ran
     * dv
     */
    public function actionCreateRan()
    {
        $return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest){

            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST["f_id"]);
            $input_iku = $parser->purify($_POST["input_iku"]);
            $input_kegiatan = $parser->purify($_POST["input_kegiatan"]);
            $input_sektoralsub = $parser->purify($_POST["input_sektoralsub"]);

            //find sektoral
            $sektoralSub = sektoralSub::model()->findByPk($input_sektoralsub);
            $sektoralId = $sektoralSub->sektoralsubSektoral->sektoral_id;
            $kl = $sektoralSub->sektoralsubSektoral->sektoral_kl_id;

            //create
            if(strlen($id)==0 && $id=="")
            {
                $model = new MKegiatan;
                $model->iku = $input_iku;
                $model->kegiatan = $input_kegiatan;
                $model->pj_sektoralsub = $input_sektoralsub;
                $model->pj_sektoral = $sektoralId;
                $model->pj_kl = $kl;

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Rencana Aksi Nasional Berhasil Ditambahkan.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
            else //update
            {
                $model = MKegiatan::model()->findByPk($id);
                $model->iku = $input_iku;
                $model->kegiatan = $input_kegiatan;
                $model->pj_sektoralsub = $input_sektoralsub;
                $model->pj_sektoral = $sektoralId;
                $model->pj_kl = $kl;

                if($model->save()){
                    $return['status'] = 1;
                    $return['message'] = "Master Rencana Aksi Nasional Berhasil Diupdate.";
                }else{
                    $return = "Master Gagal Ditambahkan";
                }
            }
        }

        echo json_encode($return);
    }

    /**
     * 19 November 2020
     * View Master Ran
     * dv
     */
    public function actionViewRan()
    {
        $return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

        if(Yii::app()->request->isPostRequest ){
            $parser = new CHtmlPurifier();
            $id = $parser->purify($_POST['id']);

            $model = MKegiatan::model()->findByPk($id);

            if($model){
                $return['data'] = array(
                    'id' => $id,
                    'iku' => $model->iku,
                    'kegiatan' => $model->kegiatan,
                    'id_sektoralsub' => $model->pj_sektoralsub,
                    'pj_sektoralsub' => $model->pjSektoralsub->sektoralsub_nama." (<b>".$model->pjSektoralsub->sektoralsub_singkatan."<b>)",
                    'pj_sektoral' => $model->pjSektoral->sektoral_nama." (<b>".$model->pjSektoral->sektoral_singkatan."<b>)",
                    'pj_kl' => $model->pjKl->kl_nama
                );
                $return["status"] = 1;
                $return["message"] = "Master Ditemukan";
            }else{
                $return["message"] = "Master tidak ditemukan";
            }
        }

        echo json_encode($return);
    }

    public function actionDeleteRan()
    {
        $parser = new CHtmlPurifier();
        $id = $parser->purify($_POST['id']);
        $return["status"] = "error";
        $return["msg"] = "Gagal Menghapus";
        try{
            MKegiatan::model()->findByPk($id)->delete();
            $return["status"] = 1;
            $return["msg"] = "Berhasil Menghapus";
        }
        catch(\Exception $e)
        {
            $return["msg"] = "Data RAN digunakan di target/realisasi";
        }	
        echo json_encode($return);
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MKegiatan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MKegiatan']))
		{
			$model->attributes=$_POST['MKegiatan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MKegiatan']))
		{
			$model->attributes=$_POST['MKegiatan'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MKegiatan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MKegiatan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MKegiatan']))
			$model->attributes=$_GET['MKegiatan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MKegiatan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MKegiatan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MKegiatan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mkegiatan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
