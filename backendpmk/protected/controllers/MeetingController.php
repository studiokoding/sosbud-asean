<?php

class MeetingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getsektoral','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			
			$model=$this->loadModel($id);
			if($model){

				$opt="<option>:: Pilih Pertemuan ::</option>";
				
				$sektoral_nama="";
				$sektoralsub_nama="";
				$m=Daftarsektoral::model()->findAllByAttributes(array('sektoral_id'=>$model->meeting_sektoral_id));
				if($m){
					foreach($m as $r){
						$sektoral_nama=$r->sektoral_singkatan;
						$sektoralsub_nama=$r->sektoralsub_singkatan;
						if($r->sektoralsub_nama!=null){
							$opt.="<option value='".$r->sektoralsub_id."_0'>".$r->sektoralsub_singkatan."</option>";
						}

						if($r->sektoral_nama!=null){
							$opt.="<option value='0_".$r->sektoral_id."'>".$r->sektoral_singkatan."</option>";
						}

						if($r->sektoralsub_nama!=null && $r->sektoral_nama!=null){
							$opt.="<option value='".$r->sektoralsub_id."_".$r->sektoral_id."'>".$r->sektoralsub_singkatan." dan ".$r->sektoral_singkatan."</option>";
						}  
					}
				}

				$filelain1="";
				$filelain2="";
				$filelain3="";
				$filelain4="";
				$filelain5="";
				if($model->meeting_flain1 != NULL){
					$filelain1="<a target='_blank' href='".Yii::app()->params['path_meeting_download'].$model->meeting_flain1."'>".$model->meeting_flaint1."</a><br>";
				}
				if($model->meeting_flain2 != NULL){
					$filelain2="<a target='_blank' href='".Yii::app()->params['path_meeting_download'].$model->meeting_flain2."'>".$model->meeting_flaint2."</a><br>";
				}
				if($model->meeting_flain3 != NULL){
					$filelain3="<a target='_blank' href='".Yii::app()->params['path_meeting_download'].$model->meeting_flain3."'>".$model->meeting_flaint3."</a><br>";
				}
				if($model->meeting_flain4 != NULL){
					$filelain4="<a target='_blank' href='".Yii::app()->params['path_meeting_download'].$model->meeting_flain4."'>".$model->meeting_flaint4."</a><br>";
				}
				if($model->meeting_flain5 != NULL){
					$filelain5="<a target='_blank' href='".Yii::app()->params['path_meeting_download'].$model->meeting_flain5."'>".$model->meeting_flaint5."</a><br>";
				}
					
				$return['data']=array(
					'id'=>$model->meeting_id,
					'pertemuan'=>$model->meeting_title,
					'sektoralid'=>$model->meeting_sektoral_id,
					'sektoralnama'=>$sektoral_nama,
					'sektoralsubid'=>$model->meeting_sektoralsub_id,
					'sektoralsubnama'=>$sektoralsub_nama,
					'ketua'=>$model->meeting_ketua,
					'pertemuanke'=>$model->meeting_urutan,
					'ketuasub'=>$model->meeting_ketuasub,
					'pertemuansubke'=>$model->meeting_urutansub,
					'lokasikota'=>$model->meeting_lokasi,
					'lokasinegara'=>$model->meeting_negara,
					'lokasi'=>$model->meeting_lokasi.', '.$model->meeting_negara,
					'bahasan'=>$model->meeting_bahasan,
					'perihal'=>$model->meeting_bperihal,
					'pendahuluan'=>$model->meeting_bpendahuluan,
					'isi'=>$model->meeting_bisi,
					'tindaklanjut'=>$model->meeting_btindaklanjut,
					'filehasil'=>"<a target='_blank' href='".Yii::app()->baseUrl.Yii::app()->params['path_meeting_download'].$model->meeting_fhasil."'>Download File Hasil</a>",
					'filelaporan'=>"<a target='_blank' href='".Yii::app()->baseUrl.Yii::app()->params['path_meeting_download'].$model->meeting_fbrafaks."'>Download File Laporan</a>",
					'filelain1'=>$filelain1,
					'filelain2'=>$filelain2,
					'filelain3'=>$filelain3,
					'filelain4'=>$filelain4,
					'filelain5'=>$filelain5,
					'file1'=>$model->meeting_flaint1,
					'file2'=>$model->meeting_flaint2,
					'file3'=>$model->meeting_flaint3,
					'file4'=>$model->meeting_flaint4,
					'file5'=>$model->meeting_flaint5,
					'entry'=>$model->meeting_entri,
					'start'=>$model->meeting_tgl,
					'end'=>$model->meeting_tglb,
					'opt'=>$opt,
					'tgl'=>Yii::app()->dateFormatter->format("dd MMM yyyy",$model->meeting_tgl)." s/d ".Yii::app()->dateFormatter->format("dd MMM yyyy",$model->meeting_tglb),
				);
				$return['status']=1;
				$return['message']="Meeting ditemukan.";
			} else {
				$return['message']="Meeting tidak ditemukan.";
			}
		}
		
		echo json_encode($return);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$mimecheckFile=array('application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document');
		$mimecheckFoto=array('image/gif', 'image/jpeg' , 'image/png');
		
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){

			$parser=new CHtmlPurifier();
			$id=$parser->purify($_POST['input_meeting_id']); 
			$idSektoral=$parser->purify($_POST['input_sektoral_id']);
			$idSubSektoral=$parser->purify($_POST['input_subsektoral_id']);
			
			$sUrutan=$parser->purify($_POST['input_s_pertemuanke']);
			$subUrutan=$parser->purify($_POST['input_sub_pertemuanke']);
			$namaSektoral=$parser->purify($_POST['input_sektoral_nama']);
			$namaSubSektoral=$parser->purify($_POST['input_subsektoral_nama']);

			$title=null;
			if($idSektoral==0 && $idSubSektoral==null){
				$title="KTT ASEAN ke ".$sUrutan;
			} else if($sUrutan!=null && $subUrutan==null){
				$title=$namaSektoral." ke ".$sUrutan;
			} else if($sUrutan==null && $subUrutan!=null){
				$title=$namaSubSektoral." ke ".$subUrutan;
			} else if($sUrutan!=null && $subUrutan!=null){
				$title=$namaSektoral." ke ".$sUrutan." dan ".$namaSubSektoral." ke ".$subUrutan;
			}
			$file=preg_replace("/[^a-zA-Z0-9]/", "_",$title);
			if($id=="")
			{
				$model=new Meeting;	
			}else{
				$model = $this->loadModel($id);
			}


			$model->meeting_sektoral_id=$idSektoral;
			$model->meeting_title=$title;
			$model->meeting_urutan=$sUrutan;
			$model->meeting_ketua=$parser->purify($_POST['input_s_ketua']);
			$model->meeting_sektoralsub_id=$idSubSektoral;

			$model->meeting_urutansub=$subUrutan;
			$model->meeting_ketuasub=$parser->purify($_POST['input_sub_ketua']);
			$model->meeting_lokasi=$parser->purify($_POST['input_kota']);
			$model->meeting_negara=$parser->purify($_POST['input_negara']);
			
			// === FILE BRAFAKS === //
			if(!empty($_FILES["input_flaporan"]["name"])){ 
				$mime=$_FILES["input_flaporan"]["type"];
				$fname=$_FILES["input_flaporan"]["name"];

				$filetype=".".explode(".",$fname)[1];

				$filename=$file.'_brafaks_'.date("Ymd_His").$filetype;

				if (in_array($mime, $mimecheckFile)) {
					$newFilePath = Yii::app()->basePath . '/../../assets/meeting/' . $filename;
					if( move_uploaded_file($_FILES["input_flaporan"]["tmp_name"], $newFilePath) ){
						$model->meeting_fbrafaks=$filename;
					} 
				}
			} 
			// === FILE BRAFAKS === //

			// === FILE HASIL === //
			if(!empty($_FILES["input_fhasil"]["name"])){ 
				$mime=$_FILES["input_fhasil"]["type"];
				$fname=$_FILES["input_fhasil"]["name"];

				$filetype=".".explode(".",$fname)[1];

				$filename=$file.'_hasil_'.date("Ymd_His").$filetype;

				if (in_array($mime, $mimecheckFile)) {
					$newFilePath = Yii::app()->basePath . '/../../assets/meeting/' . $filename;
					if( move_uploaded_file($_FILES["input_fhasil"]["tmp_name"], $newFilePath) ){
						$model->meeting_fhasil=$filename;
					}
				}
			} 
			// === FILE HASIL === //
			
			$model->meeting_bperihal=$parser->purify($_POST['input_perihal']);
			$model->meeting_bpendahuluan=$parser->purify($_POST['input_pendahuluan']);
			$model->meeting_bisi=$parser->purify($_POST['input_isi']);
			$model->meeting_btindaklanjut=$parser->purify($_POST['input_tindaklanjut']);
			// === FOTO KEGIATAN === //
			if(!empty($_FILES["input_foto"]["name"])){ 
				$mime=$_FILES["input_foto"]["type"];
				$fname=$_FILES["input_foto"]["name"];

				$filetype=".".explode(".",$fname)[1];

				$filename=$file.'_foto_'.date("Ymd_His").$filetype;

				if (in_array($mime, $mimecheckFoto)) {
					$newFilePath = Yii::app()->basePath . '/../../assets/meeting/' . $filename;
					if( move_uploaded_file($_FILES["input_foto"]["tmp_name"], $newFilePath) ){
						$model->meeting_foto=$filename;
					}
				}
			} 
			// === FOTO KEGIATAN === //

			$model->meeting_flaint1=$parser->purify($_POST['input_nft1']);
			// === FILE LAIN 1 === //
			if(!empty($_FILES["input_ft1"]["name"])){ 
				$mime=$_FILES["input_ft1"]["type"];
				$fname=$_FILES["input_ft1"]["name"];

				$filetype=".".explode(".",$fname)[1];

				$filename=$file.'_lain1_'.date("Ymd_His").$filetype;

				if (in_array($mime, $mimecheckFile)) {
					$newFilePath = Yii::app()->basePath . '/../../assets/meeting/' . $filename;
					if( move_uploaded_file($_FILES["input_ft1"]["tmp_name"], $newFilePath) ){
						$model->meeting_flain1=$filename;
					} 
				}
			} 
			// === FILE LAIN 1 === //
			
			$model->meeting_flaint2=$parser->purify($_POST['input_nft2']);
			// === FILE LAIN 2 === //
			if(!empty($_FILES["input_ft2"]["name"])){ 
				$mime=$_FILES["input_ft2"]["type"];
				$fname=$_FILES["input_ft2"]["name"];

				$filetype=".".explode(".",$fname)[1];

				$filename=$file.'_lain2_'.date("Ymd_His").$filetype;

				if (in_array($mime, $mimecheckFile)) {
					$newFilePath = Yii::app()->basePath . '/../../assets/meeting/' . $filename;
					if( move_uploaded_file($_FILES["input_ft2"]["tmp_name"], $newFilePath) ){
						$model->meeting_flain2=$filename;
					} 
				}
			} 
			// === FILE LAIN 2 === //

			$model->meeting_flaint3=$parser->purify($_POST['input_nft3']);
			// === FILE LAIN 3 === //
			if(!empty($_FILES["input_ft3"]["name"])){ 
				$mime=$_FILES["input_ft3"]["type"];
				$fname=$_FILES["input_ft3"]["name"];

				$filetype=".".explode(".",$fname)[1];

				$filename=$file.'_lain3_'.date("Ymd_His").$filetype;

				if (in_array($mime, $mimecheckFile)) {
					$newFilePath = Yii::app()->basePath . '/../../assets/meeting/' . $filename;
					if( move_uploaded_file($_FILES["input_ft3"]["tmp_name"], $newFilePath) ){
						$model->meeting_flain3=$filename;
					} 
				}
			} 
			// === FILE LAIN 3 === //

			$model->meeting_flaint4=$parser->purify($_POST['input_nft4']);
			// === FILE LAIN 4 === //
			if(!empty($_FILES["input_ft4"]["name"])){ 
				$mime=$_FILES["input_ft4"]["type"];
				$fname=$_FILES["input_ft4"]["name"];

				$filetype=".".explode(".",$fname)[1];

				$filename=$file.'_lain4_'.date("Ymd_His").$filetype;

				if (in_array($mime, $mimecheckFile)) {
					$newFilePath = Yii::app()->basePath . '/../../assets/meeting/' . $filename;
					if( move_uploaded_file($_FILES["input_ft4"]["tmp_name"], $newFilePath) ){
						$model->meeting_flain4=$filename;
					} 
				}
			} 
			// === FILE LAIN 4 === //

			$model->meeting_flaint5=$parser->purify($_POST['input_nft5']);
			// === FILE LAIN 5 === //
			if(!empty($_FILES["input_ft5"]["name"])){ 
				$mime=$_FILES["input_ft5"]["type"];
				$fname=$_FILES["input_ft5"]["name"];

				$filetype=".".explode(".",$fname)[1];

				$filename=$file.'_lain5_'.date("Ymd_His").$filetype;

				if (in_array($mime, $mimecheckFile)) {
					$newFilePath = Yii::app()->basePath . '/../../assets/meeting/' . $filename;
					if( move_uploaded_file($_FILES["input_ft5"]["tmp_name"], $newFilePath) ){
						$model->meeting_flain5=$filename;
					} 
				}
			} 
			// === FILE LAIN 5 === //

			$model->meeting_user_id=Yii::app()->user->id;
			$model->meeting_tgl=$parser->purify($_POST['input_tanggal1']);
			$model->meeting_tglb=$parser->purify($_POST['input_tanggal2']);
			$model->meeting_adddate=date("Y-m-d H:i:s");
			$model->meeting_entri=$parser->purify($_POST['input_namaentry']);
			if($model->save()){
				$return['status']=1;
				$return['message']="Pertemuan berhasil ditambahkan.";
			} else {
				$return['message']="Pertemuan gagal ditambahkan.";
			}
		}
		
		echo json_encode($return);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Meeting']))
		{
			$model->attributes=$_POST['Meeting'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->meeting_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			
			$model=$this->loadModel($id)->delete();
			if($model){
				$return['status']=1;
				$return['message']="Pertemuan berhasil dihapus.";
			} else {
				$return['message']="Pertemuan gagal dihapus.";
			}
		}
		
		echo json_encode($return);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$level=Yii::app()->user->level;		
		$dataProvider=Meeting::model()->findAll(array('order'=>'meeting_id DESC'));
		if($level == 3){
			$dataProvider=Meeting::model()->findAllByAttributes(array('meeting_user_id'=>Yii::app()->user->id),array('order'=>'meeting_id DESC'));
		} 

		$listSektoral=Daftarsektoral::model()->findAll();
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'daftarSektoral'=>$listSektoral
		));
	}

	public function actionGetsektoral(){
		$return=array(
			'status'=>0,
			'message'=>'Anda tidak diizinkan untuk melakukan akses.',
			'data'=>array(),
		);

		if(Yii::app()->request->isPostRequest ){
			$parser=new CHtmlPurifier(); 
			$id=$parser->purify($_POST['id']);
			
			$model=Daftarsektoral::model()->findAllByAttributes(array('sektoralsub_id'=>$id));
			if($model){
				$opt="<option>:: Pilih Pertemuan ::</option>";
				foreach($model as $r){
					// $opt.="<option value='".$r->sektoralsub_id."'>".$r->sektoralsub_singkatan."</option>";

					if($r->sektoralsub_nama!=null){
                    	$opt.="<option value='".$r->sektoralsub_id."_0'>".$r->sektoralsub_singkatan."</option>";
                    }

                    if($r->sektoral_nama!=null){
                    	$opt.="<option value='0_".$r->sektoral_id."'>".$r->sektoral_singkatan."</option>";
                    }

                    if($r->sektoralsub_nama!=null && $r->sektoral_nama!=null){
                    	$opt.="<option value='".$r->sektoralsub_id."_".$r->sektoral_id."'>".$r->sektoralsub_singkatan." dan ".$r->sektoral_singkatan."</option>";
                    }  
				}
				$return['data']=$opt;
				$return['status']=1;
				$return['message']="Pertemuan ditemukan.";
			} else {
				$return['message']="Pertemuan tidak ditemukan.";
			}
		}
		
		echo json_encode($return);
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Meeting the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Meeting::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Meeting $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='meeting-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
