<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "berita".
 *
 * @property int $berita_id
 * @property string|null $berita_judul
 * @property string|null $berita_foto
 * @property string|null $berita_overview
 * @property string|null $berita_isi
 * @property int|null $berita_user_id
 * @property string|null $berita_adddate
 */
class Berita extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'berita';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['berita_judul', 'berita_overview', 'berita_isi'], 'string'],
            [['berita_user_id'], 'integer'],
            [['berita_adddate'], 'safe'],
            [['berita_foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'berita_id' => 'Berita ID',
            'berita_judul' => 'Berita Judul',
            'berita_foto' => 'Berita Foto',
            'berita_overview' => 'Berita Overview',
            'berita_isi' => 'Berita Isi',
            'berita_user_id' => 'Berita User ID',
            'berita_adddate' => 'Berita Adddate',
        ];
    }
}
