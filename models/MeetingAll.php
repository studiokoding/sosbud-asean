<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meeting_all_name".
 *
 * @property int $all_id
 * @property string|null $title
 * @property string|null $tindaklanjut
 * @property string|null $tanggal
 * @property string|null $foto
 * @property string|null $lokasi
 * @property string|null $ketua1
 * @property string|null $ketua2
 * @property int $meeting_id
 * @property int|null $meeting_sektoral_id
 * @property string|null $meeting_title
 * @property int|null $meeting_urutan
 * @property string|null $meeting_ketua
 * @property int|null $meeting_sektoralsub_id
 * @property int|null $meeting_urutansub
 * @property string|null $meeting_ketuasub
 * @property string|null $meeting_lokasi
 * @property string|null $meeting_negara
 * @property string|null $meeting_bahasan
 * @property string|null $meeting_hasil
 * @property string|null $meeting_bperihal
 * @property string|null $meeting_bpendahuluan
 * @property string|null $meeting_bisi
 * @property string|null $meeting_btindaklanjut
 * @property string|null $meeting_tgl
 * @property string|null $meeting_tglb
 * @property string $meeting_entri
 */
class MeetingAll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meeting_all_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['all_id', 'meeting_id', 'meeting_sektoral_id', 'meeting_urutan', 'meeting_sektoralsub_id', 'meeting_urutansub'], 'integer'],
            [['tindaklanjut', 'meeting_bahasan', 'meeting_hasil', 'meeting_bperihal', 'meeting_bpendahuluan', 'meeting_bisi', 'meeting_btindaklanjut'], 'string'],
            [['tanggal', 'meeting_tgl', 'meeting_tglb'], 'safe'],
            [['title'], 'string', 'max' => 545],
            [['foto', 'lokasi', 'ketua1', 'ketua2', 'meeting_title', 'meeting_ketua', 'meeting_ketuasub', 'meeting_lokasi', 'meeting_negara', 'meeting_entri'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'all_id' => 'All ID',
            'title' => 'Title',
            'tindaklanjut' => 'Tindaklanjut',
            'tanggal' => 'Tanggal',
            'foto' => 'Foto',
            'lokasi' => 'Lokasi',
            'ketua1' => 'Ketua1',
            'ketua2' => 'Ketua2',
            'meeting_id' => 'Meeting ID',
            'meeting_sektoral_id' => 'Meeting Sektoral ID',
            'meeting_title' => 'Meeting Title',
            'meeting_urutan' => 'Meeting Urutan',
            'meeting_ketua' => 'Meeting Ketua',
            'meeting_sektoralsub_id' => 'Meeting Sektoralsub ID',
            'meeting_urutansub' => 'Meeting Urutansub',
            'meeting_ketuasub' => 'Meeting Ketuasub',
            'meeting_lokasi' => 'Meeting Lokasi',
            'meeting_negara' => 'Meeting Negara',
            'meeting_bahasan' => 'Meeting Bahasan',
            'meeting_hasil' => 'Meeting Hasil',
            'meeting_bperihal' => 'Meeting Bperihal',
            'meeting_bpendahuluan' => 'Meeting Bpendahuluan',
            'meeting_bisi' => 'Meeting Bisi',
            'meeting_btindaklanjut' => 'Meeting Btindaklanjut',
            'meeting_tgl' => 'Meeting Tgl',
            'meeting_tglb' => 'Meeting Tglb',
            'meeting_entri' => 'Meeting Entri',
        ];
    }
}
