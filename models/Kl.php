<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kl".
 *
 * @property int $kl_id
 * @property string|null $kl_nama
 * @property string|null $logo
 *
 * @property MKegiatan[] $mKegiatans
 * @property Sektoral[] $sektorals
 */
class Kl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kl';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kl_nama'], 'string', 'max' => 100],
            [['logo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kl_id' => 'Kl ID',
            'kl_nama' => 'Kl Nama',
            'logo' => 'Logo',
        ];
    }

    /**
     * Gets query for [[MKegiatans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMKegiatans()
    {
        return $this->hasMany(MKegiatan::className(), ['pj_kl' => 'kl_id']);
    }

    /**
     * Gets query for [[Sektorals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSektorals()
    {
        return $this->hasMany(Sektoral::className(), ['sektoral_kl_id' => 'kl_id']);
    }
}
