<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property int $id
 * @property string $alias
 * @property string $judul
 * @property string $isi
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'judul', 'isi'], 'required'],
            [['isi'], 'string'],
            [['alias'], 'string', 'max' => 50],
            [['judul'], 'string', 'max' => 160],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'judul' => 'Judul',
            'isi' => 'Isi',
        ];
    }
}
