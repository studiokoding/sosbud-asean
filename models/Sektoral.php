<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sektoral".
 *
 * @property int $sektoral_id
 * @property string|null $sektoral_nama
 * @property string|null $sektoral_singkatan
 * @property int|null $sektoral_kl_id
 * @property string|null $sektoral_overview
 * @property string|null $about
 * @property int|null $sektoral_user_id
 * @property string|null $sektoral_adddate
 *
 * @property Implementasi[] $implementasis
 * @property MKegiatan[] $mKegiatans
 * @property Meeting[] $meetings
 * @property Kl $sektoralKl
 * @property User $sektoralUser
 * @property Sektoralsub[] $sektoralsubs
 */
class Sektoral extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sektoral';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sektoral_kl_id', 'sektoral_user_id'], 'integer'],
            [['sektoral_overview', 'about'], 'string'],
            [['sektoral_adddate'], 'safe'],
            [['sektoral_nama', 'sektoral_singkatan'], 'string', 'max' => 255],
            [['sektoral_kl_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kl::className(), 'targetAttribute' => ['sektoral_kl_id' => 'kl_id']],
            [['sektoral_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['sektoral_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sektoral_id' => 'Sektoral ID',
            'sektoral_nama' => 'Sektoral Nama',
            'sektoral_singkatan' => 'Sektoral Singkatan',
            'sektoral_kl_id' => 'Sektoral Kl ID',
            'sektoral_overview' => 'Sektoral Overview',
            'about' => 'About',
            'sektoral_user_id' => 'Sektoral User ID',
            'sektoral_adddate' => 'Sektoral Adddate',
        ];
    }

    /**
     * Gets query for [[Implementasis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImplementasis()
    {
        return $this->hasMany(Implementasi::className(), ['implementasi_sektoral_id' => 'sektoral_id']);
    }

    /**
     * Gets query for [[MKegiatans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMKegiatans()
    {
        return $this->hasMany(MKegiatan::className(), ['pj_sektoral' => 'sektoral_id']);
    }

    /**
     * Gets query for [[Meetings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMeetings()
    {
        return $this->hasMany(Meeting::className(), ['meeting_sektoral_id' => 'sektoral_id']);
    }

    /**
     * Gets query for [[SektoralKl]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSektoralKl()
    {
        return $this->hasOne(Kl::className(), ['kl_id' => 'sektoral_kl_id']);
    }

    /**
     * Gets query for [[SektoralUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSektoralUser()
    {
        return $this->hasOne(User::className(), ['id' => 'sektoral_user_id']);
    }

    /**
     * Gets query for [[Sektoralsubs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSektoralsubs()
    {
        return $this->hasMany(Sektoralsub::className(), ['sektoralsub_sektoral_id' => 'sektoral_id']);
    }
}
